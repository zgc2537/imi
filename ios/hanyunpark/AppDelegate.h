/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import <UIKit/UIKit.h>
#import "RCTGetuiModule.h"
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
#import <UserNotifications/UserNotifications.h>
#endif
#define kGtAppId @"tD0li2zKN66VnucrZ9bvb1"
#define kGtAppKey @"oxQ0wg9jvE9SVxsXEtzLn2"
#define kGtAppSecret @"8wNZry30Zp57ZKUVfRmE31"

#import <React/RCTBridgeModule.h>
#import "React/RCTEventDispatcher.h"
#import "React/RCTBridge.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate,GeTuiSdkDelegate, RCTBridgeModule>

@property (nonatomic, strong) UIWindow *window;

@end
