Author:
  zhou.Bob (zhouguocheng)

Arguments:
  /usr/bin/nodejs /usr/local/bin/yarn add react-native --exact

PATH:
  config environment ANDROID_HOME JAVA_HOME

React

Yarn version:
  0.23.4

Node version:
  5.12.0

Platform:
  linux x64

npm version:
  3.8.6

npm manifest:
  {"name":"imi","version":"0.0.1","private":true,"scripts":{"start":"node node_modules/react-native/local-cli/cli.js start"}}

yarn manifest:
  No manifest

Lockfile:
  No lockfile

Request:
  https://registry.npm.taobao.org/
  (/usr/local/lib/node_modules/yarn/node_modules/request/request.js:852:19)

Gradle:
  gradle-2.14.1-all

  download by: http://www.androiddevtools.cn/
  and put the zip file into the path: ~/.gradle/wrapper/dists/gradle-2.14.1-all/8bnwg5hd3w55iofp58khbp6yv/

node_modules version:
  react@16.0.0-alpha.6
  react-native@0.44.3
  react-native-wechat@1.9.2


Install Step:
  (1) $ npm install  //'$' means this COMMAND will run in Ubuntu Terminal window.
  (2) COMPARE folder1 './node_modules/react-native-baidu-map' WITH folder2 './backup/react-native-baidu-map',
      and MERGE difference source codes of folder2 TO foler1.
  (3) Connect a Mobile Phone or simulator To PC by pluging into the USB port.
  (4) $ react-native run-android  OR  #react-native run-ios
  (5) $ react-native start

Q1:

  ERROR  A non-recoverable condition has triggered.  Watchman needs your help!
  The triggering condition was at timestamp=1463647475: inotify-add-watch(.../node_modules/iconv-lite/encodings/tables)
  -> The user limit on the total number of inotify watches was reached; increase the fs.inotify.max_user_watches sysctl
  All requests will continue to fail with this message until you resolve
  the underlying problem.  You will find more information on fixing this at
  https://facebook.github.io/watchman/docs/troubleshooting.html#poison-inotify-add-watch

A1:

  The following terminal COMMANDs will be help you =>
    $ echo 256 | sudo tee -a /proc/sys/fs/inotify/max_user_instances
    $ echo 32768 | sudo tee -a /proc/sys/fs/inotify/max_queued_events
    $ echo 65536 | sudo tee -a /proc/sys/fs/inotify/max_user_watches
    $ watchman shutdown-server

  It also be referenced =>
    ++ https://facebook.github.io/watchman/docs/install.html#system-specific-preparation ++
