/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import { AppRegistry } from 'react-native';
import Setup from './app/Setup'

AppRegistry.registerComponent('imi', () => Setup);


/*
import React, { Component, PropTypes } from 'react';
import { Navigator, Text, TouchableHighlight,
AppRegistry, View } from 'react-native';

import MyScene from './MyScene';//引入刚刚建好的文件。

export default class MyProject extends Component {

render() {
return (
<Navigator
initialRoute={{ title: 'My Initial Scene', index: 0 }}
renderScene={(route, navigator) =>
<MyScene
title={route.title}
// Function to call when a new scene should be displayed
onForward={ () => { //定义点击向前的函数内容
const nextIndex = route.index + 1;
navigator.push({
title: 'Scene ' + nextIndex,
index: nextIndex,
});
}}
// Function to call to go back to the previous scene
onBack={() => {//定义返回的函数
if (route.index > 0) {
navigator.pop();
}
}}
/>
}
/>
)
}
}
AppRegistry.registerComponent('hanyunpark', () => MyProject);//
*/
