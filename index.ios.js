/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import { AppRegistry } from 'react-native';
import Setup from './app/Setup';

AppRegistry.registerComponent('imi', () => Setup);
