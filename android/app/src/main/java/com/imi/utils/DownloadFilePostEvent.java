package com.imi.utils;

import com.imi.event.AbstractEvent;

/**
 * Created by Hanyun-05 on 2017/6/22.
 */

public class DownloadFilePostEvent extends AbstractEvent {
    private boolean mIsSucess = false;

    public DownloadFilePostEvent() {
        super("DownloadFilePostEvent");
    }

    public void setResult(boolean success) {
        mIsSucess = success;
    }

    public boolean getResult() {
        return mIsSucess;
    }
}
