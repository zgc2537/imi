package com.imi.utils;

import com.imi.event.AbstractEvent;

/**
 * Created by Hanyun-05 on 2017/6/22.
 */

public class DownloadFileProgressEvent extends AbstractEvent {
    private int mProgress = 0;

    public DownloadFileProgressEvent() {
        super("DownloadFileProcessEvent");
    }

    public void setProcess(int process) {
        mProgress = process;
    }

    public int getProcess() {
        return mProgress;
    }
}
