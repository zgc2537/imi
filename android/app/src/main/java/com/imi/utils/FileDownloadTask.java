package com.imi.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.widget.Toast;

import com.imi.event.EventListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import android.util.Log;

/**
 * Created on 2017/6/22.
 */

public class FileDownloadTask extends AsyncTask<String, Integer, String> {

    private Context mContext;
    private PowerManager.WakeLock mWakeLock;
    private int mDownloadProgress = 0;
    private EventListener<DownloadFilePostEvent> mDownloadPostEventListener;
    private EventListener<DownloadFileProgressEvent> mDownloadProgressEventListener;
    private DownloadFilePostEvent mPostEvent = new DownloadFilePostEvent();
    private DownloadFileProgressEvent mProgressEvent = new DownloadFileProgressEvent();

    public FileDownloadTask(Context context, EventListener<DownloadFilePostEvent> postEventListener,
        EventListener<DownloadFileProgressEvent> progressEventListener) {
        this.mContext = context;
        mDownloadPostEventListener = postEventListener;
        mDownloadProgressEventListener = progressEventListener;
    }

    @Override
    protected String doInBackground(String... params) {
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        File file;
        try {
            URL url = new URL(params[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "网络或服务器错误，" + connection.getResponseCode();
            }
            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = connection.getContentLength();
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                file = new File(params[1], params[2]);

                if (!file.exists()) {
                    // 判断父文件夹是否存在
                    if (!file.getParentFile().exists()) {
                        file.getParentFile().mkdirs();
                    }
                } else {
                    file.delete();
                }

            } else {
                return "sd卡未挂载";
            }
            input = connection.getInputStream();
            output = new FileOutputStream(file);
            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                if (isCancelled()) {
                    input.close();
                    return null;
                }
                total += count;
                Log.i("FileDownloadTask", "fileLen: " + fileLength + ", total:" + total);
                // publishing the progress....
                if (fileLength > 0) // only if total length is known
                    publishProgress(((new BigDecimal(total * 100)).divide(new BigDecimal(fileLength), 2, BigDecimal.ROUND_HALF_DOWN)).intValue());
                output.write(data, 0, count);

            }
        } catch (Exception e) {
            return e.toString();
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // take CPU lock to prevent CPU from going off if the user
        // presses the power button during download
        PowerManager pm = (PowerManager) mContext
                .getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                getClass().getName());
        mWakeLock.acquire();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
        int delta = progress[0] - mDownloadProgress;
        if(delta >= 20 || progress[0] == 100) {
            mDownloadProgress = progress[0];
            mProgressEvent.setProcess(progress[0]);
            mDownloadProgressEventListener.onEvent(mProgressEvent);
        }
    }

    @Override
    protected void onPostExecute(String result) {
        mWakeLock.release();
        if (result != null) {
            Toast.makeText(mContext, result, Toast.LENGTH_LONG).show();
            mPostEvent.setResult(false);
            mDownloadPostEventListener.onEvent(mPostEvent);
        } else {
            mPostEvent.setResult(true);
            mDownloadPostEventListener.onEvent(mPostEvent);
        }
    }
}
