package com.imi.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.widget.Toast;

import com.imi.MainApplication;
import com.imi.R;
import com.imi.event.EventListener;

import java.io.File;

/**
 * Created on 2017/4/1.
 */

public class Utils {
    public static final String UPDATE_APK_FILE_SAVE_PATH = Environment.getExternalStorageDirectory() + "/wx/";
    public static final String UPDATE_BUNDLE_ZIP_FILE_NAME = "bundle.zip";
    public static final String UPDATE_BUNDLE_FILE_NAME = "index.android.bundle";

    public static void apkUpdate(final Context context, String url) {
        String[] urlSplited = url.split("/");
        final String apkName = urlSplited[urlSplited.length - 1];
        final Context fContext = context;
        new FileDownloadTask(context, new EventListener<DownloadFilePostEvent>() {
            @Override
            public void onEvent(DownloadFilePostEvent var1) {
                if(var1.getResult()) {
                    if(deleteBundleFile(context)) {
                        MainApplication.getDeviceOutCallPackage().getDeviceForOuterCall().nativeCallRn("update-event", true);
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(new File(UPDATE_APK_FILE_SAVE_PATH, apkName)),
                                "application/vnd.android.package-archive");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        fContext.startActivity(intent);
                    } else {
                        MainApplication.getDeviceOutCallPackage().getDeviceForOuterCall().nativeCallRn("update-event", false);
                    }
                } else {
                    MainApplication.getDeviceOutCallPackage().getDeviceForOuterCall().nativeCallRn("update-event", var1.getResult());
                }

            }
        }, new EventListener<DownloadFileProgressEvent>() {
            @Override
            public void onEvent(DownloadFileProgressEvent var1) {
                MainApplication.getDeviceOutCallPackage().getDeviceForOuterCall().nativeCallRn("update-progress", var1.getProcess());
            }
        }).execute(url, UPDATE_APK_FILE_SAVE_PATH, apkName);
    }

    public static void hotUpdate(Context context, String url) {
        new FileDownloadTask(context, new EventListener<DownloadFilePostEvent>() {
            @Override
            public void onEvent(DownloadFilePostEvent var1) {
                MainApplication.getDeviceOutCallPackage().getDeviceForOuterCall().nativeCallRn("update-event", var1.getResult());
            }
        }, new EventListener<DownloadFileProgressEvent>() {
            @Override
            public void onEvent(DownloadFileProgressEvent var1) {
                MainApplication.getDeviceOutCallPackage().getDeviceForOuterCall().nativeCallRn("update-progress", var1.getProcess());
            }
        }).execute(url, context.getFilesDir().getAbsolutePath(), UPDATE_BUNDLE_ZIP_FILE_NAME);
    }

    public static void hotUpdateUnzipFile(Context context) {
        final Context fContext = context;
        new UnZipTask(context, new EventListener<UnZipFilePostEvent>() {
            @Override
            public void onEvent(UnZipFilePostEvent var1) {
                if(var1.getResult()) {
                    new File(fContext.getFilesDir().getAbsolutePath() + File.separator + UPDATE_BUNDLE_ZIP_FILE_NAME).delete();
                    restartApp(fContext);
                }
                MainApplication.getDeviceOutCallPackage().getDeviceForOuterCall().nativeCallRn("unzip-event", var1.getResult());
            }
        }).execute(context.getFilesDir().getAbsolutePath() + File.separator + UPDATE_BUNDLE_ZIP_FILE_NAME, context.getFilesDir().getAbsolutePath());
    }

    public static void restartApp(Context context) {
        Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(context.getPackageName());
        PendingIntent restartIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager mgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, restartIntent); // 1秒钟后重启应用
        System.exit(0);
    }

    public static boolean getWifiEnabled(Context context) {
        WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if(manager != null) {
            if(manager.getWifiState() != WifiManager.WIFI_STATE_ENABLED) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    //it is important. if the old bundle exists after the new apk was installed,
    // and the apk updated the rn part, the new rn part will not work
    private static boolean deleteBundleFile(Context context) {
        File bundleFile = new File(context.getFilesDir().getAbsolutePath() + File.separator + Utils.UPDATE_BUNDLE_FILE_NAME);
        if(bundleFile.exists()) {
            if(bundleFile.delete()) {
                return true;
            } else {
                Toast.makeText(context, R.string.bundle_delete_fail, Toast.LENGTH_LONG).show();
            }
        }
        return true;
    }

}
