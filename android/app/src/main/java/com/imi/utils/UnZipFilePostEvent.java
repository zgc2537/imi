package com.imi.utils;

import com.imi.event.AbstractEvent;


/**
 * Created on 2017/6/22.
 */

public class UnZipFilePostEvent extends AbstractEvent {
    private boolean mIsSucess = false;

    public UnZipFilePostEvent() {
        super("UnZipFilePostEvent");
    }

    public void setResult(boolean success) {
        mIsSucess = success;
    }

    public boolean getResult() {
        return mIsSucess;
    }
}
