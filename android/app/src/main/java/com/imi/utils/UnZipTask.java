package com.imi.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.widget.Toast;

import com.imi.event.EventListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created on 2017/6/23.
 */

public class UnZipTask extends AsyncTask<String, Integer, String> {
    private Context mContext;
    private PowerManager.WakeLock mWakeLock;
    private EventListener<UnZipFilePostEvent> mUnZipPostEventListener;
    private UnZipFilePostEvent mPostEvent = new UnZipFilePostEvent();


    public UnZipTask(Context context, EventListener<UnZipFilePostEvent> postEventListener) {
        this.mContext = context;
        mUnZipPostEventListener = postEventListener;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            ZipInputStream inZip = new ZipInputStream(new FileInputStream(params[0]));
            ZipEntry zipEntry;
            String szName;
            try {
                while((zipEntry = inZip.getNextEntry()) != null) {
                    szName = zipEntry.getName();
                    if(zipEntry.isDirectory()) {
                        szName = szName.substring(0,szName.length()-1);
                        File folder = new File(params[1] + File.separator + szName);
                        folder.mkdirs();
                    }else{

                        File file1 = new File(params[1] + File.separator + szName);
                        //if(!file1.createNewFile()) {
                        //    return "解压错误";
                        //}
                        FileOutputStream fos = new FileOutputStream(file1);
                        int len;
                        byte[] buffer = new byte[1024];

                        while((len = inZip.read(buffer)) != -1) {
                            fos.write(buffer, 0 , len);
                            fos.flush();
                        }
                        fos.close();
                    }
                }
            } catch (IOException e) {
                return "解压错误";
            }
            inZip.close();
        } catch (FileNotFoundException e) {
            return "解压错误";
        } catch (IOException e) {
            return "解压错误";
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // take CPU lock to prevent CPU from going off if the user
        // presses the power button during download
        PowerManager pm = (PowerManager) mContext
                .getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                getClass().getName());
        mWakeLock.acquire();
    }

    @Override
    protected void onPostExecute(String result) {
        mWakeLock.release();
        if (result != null) {
            Toast.makeText(mContext, result, Toast.LENGTH_LONG).show();
            mPostEvent.setResult(false);
            mUnZipPostEventListener.onEvent(mPostEvent);
        } else {
            mPostEvent.setResult(true);
            mUnZipPostEventListener.onEvent(mPostEvent);
        }
    }

}
