package com.imi;

import android.app.Application;
import android.support.annotation.Nullable;

import com.facebook.react.ReactApplication;
import com.vinzscam.reactnativefileviewer.RNFileViewerPackage;
import org.reactnative.camera.RNCameraPackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.filepicker.FilePickerPackage;
import com.imi.outercall.DeviceForOuterCallPackage;
import com.imi.utils.Utils;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private static final DeviceForOuterCallPackage mDeviceOutCallPackage = new DeviceForOuterCallPackage();
  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

    @Nullable
    @Override
    protected String getJSBundleFile() {
      String jsBundleFile = getFilesDir().getAbsolutePath() + File.separator + Utils.UPDATE_BUNDLE_FILE_NAME;
      File file = new File(jsBundleFile);
      android.util.Log.i("MainApplication", file != null && file.exists() ? "true" : "false");
      return file != null && file.exists() ? jsBundleFile : null;
    }

    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNFileViewerPackage(),
            new RNCameraPackage(),
            new SplashScreenReactPackage(),
              mDeviceOutCallPackage,
              new RNFetchBlobPackage(),
              new FilePickerPackage()
      );
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }

  public static DeviceForOuterCallPackage getDeviceOutCallPackage() {
    return mDeviceOutCallPackage;
  }
}
