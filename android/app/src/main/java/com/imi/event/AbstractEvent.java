package com.imi.event;


/**
 * Created on 2017/4/10.
 */

public abstract class AbstractEvent implements Event {
    private String eventName;
    private long timestamp;

    public AbstractEvent(String var1) {
        this.eventName = var1;
        this.timestamp = System.currentTimeMillis();
    }

    public String getEventName() {
        return this.eventName;
    }

    public long timestamp() {
        return this.timestamp;
    }
}
