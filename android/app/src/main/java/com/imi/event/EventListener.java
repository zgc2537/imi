package com.imi.event;

/**
 * Created on 2017/4/10.
 */

public interface EventListener<T extends Event> {
    void onEvent(T var1);
}
