package com.imi.event;

/**
 * Created on 2017/4/10.
 */

public interface Event {
    String getEventName();

    long timestamp();
}
