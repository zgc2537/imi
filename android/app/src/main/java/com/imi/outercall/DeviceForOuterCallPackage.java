package com.imi.outercall;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

import com.facebook.react.ReactPackage;
import com.facebook.react.uimanager.ViewManager;
import com.facebook.react.bridge.JavaScriptModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.NativeModule;

/**
 * Created on 2017/4/17.
 */

public class DeviceForOuterCallPackage implements ReactPackage{
    private DeviceForOuterCall mDeviceForOuterCall;

    public List<Class<? extends JavaScriptModule>> createJSModules() {
        return Collections.emptyList();
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }

    @Override
    public List<NativeModule> createNativeModules(
            ReactApplicationContext reactContext) {
        List<NativeModule> modules = new ArrayList<>();
        mDeviceForOuterCall = new DeviceForOuterCall(reactContext);
        modules.add(mDeviceForOuterCall);
        return modules;
    }

    public DeviceForOuterCall getDeviceForOuterCall() {
        return mDeviceForOuterCall;
    }


}
