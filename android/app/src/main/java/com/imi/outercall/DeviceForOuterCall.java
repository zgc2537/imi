package com.imi.outercall;


import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.imi.utils.Utils;


/**
 * Created on 2017/4/17.
 */

public class DeviceForOuterCall extends ReactContextBaseJavaModule {

    private ReactApplicationContext mContext;

    public DeviceForOuterCall(ReactApplicationContext context) {
        super(context);
        mContext = context;
    }

    @Override
    public String getName() {
        return "Device";
    }

    public void nativeCallRn(String eventName, Object msg) {
        mContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName,msg);
    }

    @ReactMethod
    public void hotUpdate(String url) {
        Utils.hotUpdate(mContext, url);
    }

    @ReactMethod
    public void apkUpdate(String url) {
        Utils.apkUpdate(mContext, url);
    }

    @ReactMethod
    public void hotUpdateUnZip() {
        Utils.hotUpdateUnzipFile(mContext);
    }

    @ReactMethod
    public void wifiEnabled(Promise promise) {
        promise.resolve(Utils.getWifiEnabled(mContext));
    }
}
