/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import LoginScreen from '../pages/Login';
import * as creators from '../actions/user';

class LoginContainer extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props){
    super(props);
  }

  render() {
    return <LoginScreen {...this.props}/> ;
  }
}

function select(store){
    return {
        isLoggedIn: store.userInfo.isLoggedIn,
        user: store.userInfo.user,
        loginError: store.userInfo.loginError,
        loginStatus: store.userInfo.loginStatus,
    }
}

function actions(dispatch) {
  const actions = bindActionCreators(creators, dispatch);
  return {
    actions
  };
}

export default connect(select,actions)(LoginContainer);
