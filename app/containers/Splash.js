/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */

import React from 'react';
import SplashScreen from '../pages/Splash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userCreator from '../actions/user';

class SplashContainer extends React.Component {
  render() {
    return <SplashScreen {...this.props}/> ;
  }
}

function select(store){
  return{
    isLoggedIn : store.userInfo.isLoggedIn
  }
}

function actions(dispatch) {
  const actions = bindActionCreators({...userCreator}, dispatch);
  return {
    actions
  };
}

export default connect(select,actions)(SplashContainer);
