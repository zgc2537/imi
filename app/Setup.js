/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */

import React, { Component } from 'react';
import { Platform } from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './store/configure';
import App from './ParkApp';
import SplashScreen from 'react-native-splash-screen';
import * as CONST from './utils/constants';

let store = configureStore();

export default class Setup extends Component{
  constructor(props){
    super(props);
  }

  state = {
    isStoreConfiguring : true,
    nativeActivityLodingDone :false,
    store : configureStore(() => {
      this.setState({isStoreConfiguring : false});
      if(this.state.nativeActivityLodingDone) {
        SplashScreen.hide();//两次调用，没有问题
      }
  })
};

  componentDidMount(){
    if (!__DEV__) {
      // eslint-disable-line no-undef
      [
        'assert',
        'clear',
        'count',
        'debug',
        'dir',
        'dirxml',
        'error',
        'exception',
        'group',
        'groupCollapsed',
        'groupEnd',
        'info',
        'log',
        'profile',
        'profileEnd',
        'table',
        'time',
        'timeEnd',
        'timeStamp',
        'trace',
        'warn',
      ].forEach(methodName => {
        console[methodName] = () => {
          /* noop */
        };
      });
    }
    this.state.nativeActivityLodingDone = true;
    if(!this.state.isStoreConfiguring) {
      SplashScreen.hide();
    }
  }

  render(){
    if (!__DEV__) {
      global.console = {
        info: () => {},
        log: () => {},
        warn: () => {},
        debug: () => {},
        error: () => {},
      };
    }
    if(this.state.isStoreConfiguring){
       return null;
    }

    return (
      // <Provider store={store}>
      <Provider store={this.state.store}>
          <App />
      </Provider>
    )
  }
}
