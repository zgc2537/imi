/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */
import React from 'react';
import { DeviceEventEmitter,Image,TouchableOpacity,View,StyleSheet } from 'react-native';
import { StackNavigator, TabNavigator, NavigationActions } from 'react-navigation';

import * as CONST from './utils/constants';

import SplashContainer from './containers/Splash';
import LoginContainer from './containers/Login';
import LoginScreen from './pages/Login';
import Home from './pages/home';
import Support from './pages/Support';
import Upgrades from './pages/Upgrades';
import ManualSearchInput from './pages/ManualSearchInput';
import Asset from './pages/Asset';
import AssetInfo from './pages/AssetInfo';
import OpportunityDetail from './pages/OpportunityDetail';
import SelectDatabase from './pages/SelectDatabase';
import MyUpgrade from './pages/MyUpgrade';
import CaseStudies from './pages/CaseStudies';
import FindUpgrade from './pages/FindUpgrade';
import Documents from './pages/Documents';
import Contacts from './pages/Contacts';
import Filter from './pages/Filter';
import ContactDetail from './pages/ContactDetail';
import ServiceReport from './pages/ServiceReport';
import ReportDetail from './pages/ReportDetail';
import AddServiceReport from './pages/AddServiceReport';
import ServiceReportDetail from './pages/ServiceReportDetail';
import SpareParts from './pages/SpareParts';
import OrderHistory from './pages/OrderHistory';
import ValveSearch from './pages/ValveSearch';
import ValveSearchDetail from './pages/ValveSearchDetail';
import ProductNumber from './pages/ProductNumber';
import ScanTag from './pages/ScanTag';
import TakePic from './pages/TakePic';
import AssetCheck from './pages/AssetCheck';
import ValveDoctorRequest from './pages/ValveDoctorRequest';
import ValveDoctorRequestSubmitted from './pages/ValveDoctorRequestSubmitted';
import Chat from './pages/Chat';
import ImageSubmitted from './pages/ImageSubmitted';
import Utilities from './pages/Utilities';
import ValueInput from './pages/ValueInput';
import UpgradePackageDetail from './pages/UpgradePackageDetail';

const ParkApp = StackNavigator(
    {
      Splash: {
        screen: SplashContainer
      },
      Login: {
        screen: LoginContainer,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      Home: {
        screen: Home
      },
      Support: {
        screen: Support,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      Upgrades: {
        screen: Upgrades,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      ManualSearchInput: {
        screen: ManualSearchInput,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      Asset: {
        screen: Asset,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      AssetInfo: {
        screen: AssetInfo,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      OpportunityDetail: {
        screen: OpportunityDetail,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      SelectDatabase: {
        screen: SelectDatabase,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      MyUpgrade: {
        screen: MyUpgrade,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      CaseStudies: {
        screen: CaseStudies,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      FindUpgrade: {
        screen: FindUpgrade,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      Documents: {
        screen: Documents,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      Contacts: {
        screen: Contacts,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      Filter: {
        screen: Filter,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      ContactDetail: {
        screen: ContactDetail,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      ServiceReport: {
        screen: ServiceReport,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      ReportDetail: {
        screen: ReportDetail,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      AddServiceReport: {
        screen: AddServiceReport,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      ServiceReportDetail: {
        screen: ServiceReportDetail,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      SpareParts: {
        screen: SpareParts,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      OrderHistory: {
        screen: OrderHistory,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      ValveSearch: {
        screen: ValveSearch,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      ValveSearchDetail: {
        screen: ValveSearchDetail,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      ProductNumber: {
        screen: ProductNumber,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      ScanTag: {
        screen: ScanTag,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      TakePic: {
        screen: TakePic,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      AssetCheck: {
        screen: AssetCheck,
        navigationOptions: ({navigation}) => StackOptions({navigation})
      },
      ValveDoctorRequest: {
        screen: ValveDoctorRequest,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      ValveDoctorRequestSubmitted: {
        screen: ValveDoctorRequestSubmitted,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      Chat: {
        screen: Chat,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      ImageSubmitted: {
        screen: ImageSubmitted,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      Utilities: {
        screen: Utilities,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      ValueInput: {
        screen: ValueInput,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      },
      UpgradePackageDetail: {
        screen: UpgradePackageDetail,
        navigationOptions: ({navigation}) => StackOptions({navigation}),
      }
    },
    {
        initialRouteName: 'Splash',
        headerMode: 'screen',
        //navigationOptions: ({navigation}) => StackOptions({navigation})
        navigationOptions: {
          //headerTitle: '瀚沄停车',
          headerStyle: {
                paddingTop : 24,
                height : CONST.ACTION_BAR_HEIGHT,
                backgroundColor : '#fff',
                elevation : 0,
                borderBottomWidth : 1,
                borderColor: '#dde1e2'
            },
          headerTitleStyle: {
                color : '#000',
                fontSize : 18,
                alignSelf : 'center'
            },
          headerTintColor : '#fff'
        }
    }
);

const StackOptions = ({navigation}) =>{
  let {state,goBack} = navigation;

  const headerLeft = (
    <TouchableOpacity style={{height:48,justifyContent:'center',paddingLeft:16,paddingRight:16}} onPress={()=>goBack()}>
        <Image source={require('../app/img/ic_title_pre.png')} style={{width:20,height:20}} />
    </TouchableOpacity>
  );

  const headerRight = (
    <TouchableOpacity style={{height:48,justifyContent:'center',paddingLeft:16,paddingRight: 8, paddingTop: 6}}>
        <Image source={require('../app/img/logo.png')} style={{width: 60, height: 40}} />
    </TouchableOpacity>
  );
  return { headerLeft , headerRight };
};


const defaultGetStateForAction = ParkApp.router.getStateForAction;
ParkApp.router.getStateForAction = (action, state) => {
  let routeName = null;
  if (action.type === NavigationActions.BACK && state && state.index > 0 && state.routes){
    if(state.routes[state.index].routeName == CONST.NAV_PAY_MAIN){
      for(let i = state.index; i >= 0; i--){
        if(state.routes[i].key == action.key && i == state.index){//goback
          break;
        }else if(state.routes[i].key == action.key && i == state.index - 2){
          routeName = state.routes[state.index - 2].routeName;
          DeviceEventEmitter.emit(CONST.VENDOR_EVENT_STACK_ROUTE_CHANGE, state.index - 2, routeName);
          return {
            ...state,
            routes: state.routes.slice(0, state.index - 1),
            index : state.index - 2,
          };
        }
      }
    }
    routeName = state.routes[state.index - 1].routeName;
    //send back event to vendor components.
    DeviceEventEmitter.emit(CONST.VENDOR_EVENT_STACK_ROUTE_CHANGE, state.index-1, routeName);
  }
  return defaultGetStateForAction(action, state);
};

export default ParkApp;
