/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */

import * as CONST from '../utils/constants';
import { Alert, Platform, ActionSheetIOS } from 'react-native';
import ToastUtil from '../utils/ToastUtil';

// react-native bundle --entry-file index.android.js --bundle-output ./bundle/index.android.bundle --platform android --assets-dest ./bundle --dev false
export const onlyAlert = (title, content, click)=> {
  if(title && content){
    if(Platform.OS === 'ios'){
      Alert.alert(
        title,
        content,
        [
          {text: '确定', onPress: click},
        ],
        { cancelable: false }
      )
    } else {
      ToastUtil.showShort(content);
    }
    if(click) {
      click();
    }
  }else{
    return;
  }
};

export const judgeHasUnread = (arr) => {
  for(var value of arr){
    if(!value.isRead){
      return true
    }
  }
  return false;
};

export const copyArrObj = (arr) => {
  let res = [];
  if(arr){
    for(let i = 0 ; i < arr.length ; i++){
      res[i] = JSON.parse(JSON.stringify(arr[i]));
    }
  }
  return res;
};

export const carnoRegExp1 = /^[\u4e00-\u9fa5]{1}[a-zA-Z]{1}[a-zA-Z_0-9]{4}[a-zA-Z_0-9_\u4e00-\u9fa5]$/; //普通车牌
export const carnoRegExp2 = /^[\u4e00-\u9fa5]{1}[a-zA-Z]{1}[a-zA-Z_0-9]{5}[a-zA-Z_0-9_\u4e00-\u9fa5]$/; //能源车牌

//删除左右两端的空格
export const trim = (str) => {
  return str.replace(/(^\s*)|(\s*$)/g,"");
};

//删除左边的空格
export const ltrim = (str) => {
  return str.replace(/(^\s*)/g,"");
};

//删除右边的空格
export const rtrim = (str) => {
  return str.replace(/(\s*$)/g,"");
};

//删除全部空格
export const alltrim = (str) => {
  return str.replace(/\ +/g,"");
}

//删除回车
export const etrim = (str) => {
  return str.replace(/[\r\n]/g,"");
}

//检查当前手机号是否正确
export const checkValidPhoneNo = (value,validator) => {
    let isValid = false;
    let COUNTRY_CODES = ['BR', 'CN', 'CZ', 'DK', 'ES', 'FR', 'GB', 'MA', 'PK', 'RO', 'RU', 'SK', 'TH', 'US', 'VE'];

    if(validator == null || typeof validator.country !== 'string') return isValid;

    let country = validator.country;
    let isCodeOk = false;

    COUNTRY_CODES.map((COUNTRY_CODE,i) => {
      if(country.toUpperCase() === COUNTRY_CODE)
        isCodeOk = true;
    });

    if(!isCodeOk) return false;

    switch (country.toUpperCase()) {
        case 'BR':
            // Test: http://regexr.com/399m1
            value   = trim(value);
            isValid = (/^(([\d]{4}[-.\s]{1}[\d]{2,3}[-.\s]{1}[\d]{2}[-.\s]{1}[\d]{2})|([\d]{4}[-.\s]{1}[\d]{3}[-.\s]{1}[\d]{4})|((\(?\+?[0-9]{2}\)?\s?)?(\(?\d{2}\)?\s?)?\d{4,5}[-.\s]?\d{4}))$/).test(value);
            break;

        case 'CN':
            // http://regexr.com/39dq4
            value   = trim(value);
            isValid = (/^((00|\+)?(86(?:-| )))?((\d{11})|(\d{3}[- ]{1}\d{4}[- ]{1}\d{4})|((\d{2,4}[- ]){1}(\d{7,8}|(\d{3,4}[- ]{1}\d{4}))([- ]{1}\d{1,4})?))$/).test(value);
            break;

        case 'CZ':
            // Test: http://regexr.com/39hhl
            isValid = /^(((00)([- ]?)|\+)(420)([- ]?))?((\d{3})([- ]?)){2}(\d{3})$/.test(value);
            break;

        case 'DK':
            // Mathing DK phone numbers with country code in 1 of 3 formats and an
            // 8 digit phone number not starting with a 0 or 1. Can have 1 space
            // between each character except inside the country code.
            // Test: http://regex101.com/r/sS8fO4/1
            value   = trim(value);
            isValid = (/^(\+45|0045|\(45\))?\s?[2-9](\s?\d){7}$/).test(value);
            break;

        case 'ES':
            // http://regex101.com/r/rB9mA9/1
            value   = trim(value);
            isValid = (/^(?:(?:(?:\+|00)34\D?))?(?:9|6)(?:\d\D?){8}$/).test(value);
            break;

        case 'FR':
            // http://regexr.com/39a2p
            value   = trim(value);
            isValid = (/^(?:(?:(?:\+|00)33[ ]?(?:\(0\)[ ]?)?)|0){1}[1-9]{1}([ .-]?)(?:\d{2}\1?){3}\d{2}$/).test(value);
            break;

        case 'GB':
                // http://aa-asterisk.org.uk/index.php/Regular_Expressions_for_Validating_and_Formatting_GB_Telephone_Numbers#Match_GB_telephone_number_in_any_format
                // Test: http://regexr.com/38uhv
                value  = trim(value);
                isValid = (/^\(?(?:(?:0(?:0|11)\)?[\s-]?\(?|\+)44\)?[\s-]?\(?(?:0\)?[\s-]?\(?)?|0)(?:\d{2}\)?[\s-]?\d{4}[\s-]?\d{4}|\d{3}\)?[\s-]?\d{3}[\s-]?\d{3,4}|\d{4}\)?[\s-]?(?:\d{5}|\d{3}[\s-]?\d{3})|\d{5}\)?[\s-]?\d{4,5}|8(?:00[\s-]?11[\s-]?11|45[\s-]?46[\s-]?4\d))(?:(?:[\s-]?(?:x|ext\.?\s?|\#)\d+)?)$/).test(value);
            break;

        case 'MA':
            // http://en.wikipedia.org/wiki/Telephone_numbers_in_Morocco
            // Test: http://regexr.com/399n8
            value   = trim(value);
            isValid = (/^(?:(?:(?:\+|00)212[\s]?(?:[\s]?\(0\)[\s]?)?)|0){1}(?:5[\s.-]?[2-3]|6[\s.-]?[13-9]){1}[0-9]{1}(?:[\s.-]?\d{2}){3}$/).test(value);
            break;

        case 'PK':
            // http://regex101.com/r/yH8aV9/2
            value   = trim(value);
            isValid = (/^0?3[0-9]{2}[0-9]{7}$/).test(value);
            break;

        case 'RO':
            // All mobile network and land line
            // http://regexr.com/39fv1
            isValid = (/^(\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/g).test(value);
            break;

        case 'RU':
            // http://regex101.com/r/gW7yT5/5
            isValid = (/^((8|\+7|007)[\-\.\/ ]?)?([\(\/\.]?\d{3}[\)\/\.]?[\-\.\/ ]?)?[\d\-\.\/ ]{7,10}$/g).test(value);
            break;

        case 'SK':
            // Test: http://regexr.com/39hhl
            isValid = /^(((00)([- ]?)|\+)(420)([- ]?))?((\d{3})([- ]?)){2}(\d{3})$/.test(value);
            break;

        case 'TH':
            // http://regex101.com/r/vM5mZ4/2
            isValid = (/^0\(?([6|8-9]{2})*-([0-9]{3})*-([0-9]{4})$/).test(value);
            break;

        case 'VE':
            // http://regex101.com/r/eM2yY0/6
            value   = trim(value);
            isValid = (/^0(?:2(?:12|4[0-9]|5[1-9]|6[0-9]|7[0-8]|8[1-35-8]|9[1-5]|3[45789])|4(?:1[246]|2[46]))\d{7}$/).test(value);
            break;

        case 'US':
        /* falls through */
        default:
            // Make sure US phone numbers have 10 digits
            // May start with 1, +1, or 1-; should discard
            // Area code may be delimited with (), & sections may be delimited with . or -
            // Test: http://regexr.com/38mqi
            value   = value.replace(/\D/g, '');
            isValid = (/^(?:(1\-?)|(\+1 ?))?\(?(\d{3})[\)\-\.]?(\d{3})[\-\.]?(\d{4})$/).test(value) && (value.length === 10);
            break;
    }
    return isValid;
};

export const getParkSpendTimeStr = (minutes) => {
  let str = "",
      days = 0,
      hours = 0;
  if(!minutes){
    return "0分钟";
  }
  if((days = Math.floor(minutes/1440)) > 0){
    str += days + "天";
  }
  minutes = minutes%1440;
  if((hours = Math.floor(minutes/60)) > 0){
    str += hours + "小时";
  }
  minutes = minutes%60;
  if(minutes > 0){
    str += minutes + "分钟";
  }
  return str;
};

//加法
export function FloatAdd(arg1, arg2) {
    var r1,
        r2,
        m;
    try {
        r1 = arg1.toString().split(".")[1].length
    } catch (e) {
        r1 = 0
    }
    try {
        r2 = arg2.toString().split(".")[1].length
    } catch (e) {
        r2 = 0
    }
    m = Math.pow(10, Math.max(r1, r2));
    return (Number((arg1 * m).toFixed(0)) + Number((arg2 * m).toFixed(0))) / m;
}

//减法
export function FloatSub(arg1, arg2) {
    var r1,
        r2,
        m,
        n;
    try {
        r1 = arg1.toString().split(".")[1].length
    } catch (e) {
        r1 = 0
    }
    try {
        r2 = arg2.toString().split(".")[1].length
    } catch (e) {
        r2 = 0
    }
    m = Math.pow(10, Math.max(r1, r2));
    //动态控制精度长度
    n = (r1 >= r2) ? r1 : r2;
    return ((Number((arg1 * m).toFixed(0)) - Number((arg2 * m).toFixed(0))) / m).toFixed(n);
}

export function getErrStr(e){
    let str = 'Service error'

    if(!e || ((!e.errmsg && !e.errcode) && (typeof(e.message) != 'string'))){
        str = 'Service error'
        return str
    }

    if(e.errcode) {
        str = CONST.ERR_MSG_INFO[e.errcode].errmsg
        return str
    }

    if(e.msg) {
        str = e.msg
        return str
    }

    return str
    // const TEXT_MSG_ERR_INFO = {
    //     "Network request failed":{ errmsg:"Network request failed",  desc:"无网络或者其他网络问题",},
    // };
    // const WECHAT_MSG_ERR_INFO = {
    //     "0"  : { errmsg:"", desc:"用户同意" },
    //     "-2" : { errmsg:"", desc:"用户取消" },
    //     "-4" : { errmsg:"", desc:"用户拒绝授权" },
    // };
    // let strErr;

    // //check wechat error.
    // if(e && e.name == 'WechatError'){
    //    strErr = e.code
    //             ? ( !WECHAT_MSG_ERR_INFO[e.code]
    //                 ? e.code
    //                 : WECHAT_MSG_ERR_INFO[e.code].desc
    //               )
    //             : '微信请求异常';
    //    return strErr;
    // }

    // if(!e || ((!e.errmsg && !e.errcode) && (typeof(e.message) != 'string'))){
    //    strErr = '请求出现异常';
    // }else if(e.errcode || e.errmsg){
    //    strErr = e.errcode
    //             ? ( !CONST.ERR_MSG_INFO[e.errcode]
    //                 ? e.errcode
    //                 : CONST.ERR_MSG_INFO[e.errcode].desc
    //               )
    //             : e.errmsg;
    // }else{
    //    let item = TEXT_MSG_ERR_INFO[e.message];
    //    if(item)
    //        strErr = item.desc;
    //    else
    //        strErr = e.message;
    // }
    // return strErr;
}

export const toHourMinute = (minutes) => {
  var str = "";
  if (Math.floor(minutes / 60) > 0) {
    str += Math.floor(minutes / 60).toFixed(0) + "小时";
    if (minutes % 60 > 0) {
      str += (minutes % 60).toFixed(0) + "分";
    }
  } else {
    str += (minutes % 60).toFixed(0) + "分钟";
  }
  return str;
}

export const dateFormat = (date, fmt) => { //author: meizz
    var o = {
        "M+": date.getMonth() + 1, //月份
        "d+": date.getDate(), //日
        "h+": date.getHours(), //小时
        "m+": date.getMinutes(), //分
        "s+": date.getSeconds(), //秒
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度
        "S": date.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

export const showActionSheetWithIosTakePhoto = (actionSheets)=> {
  var options = [];
  for(var value of actionSheets.BUTTONS){
    options.push(value.title)
  }
  ActionSheetIOS.showActionSheetWithOptions({
     options: options,
     cancelButtonIndex: actionSheets.CANCEL_INDEX,
   },function(index){
     actionSheets.BUTTONS[index].click();
   })
}

export function getGetParams() {
  return {method : 'GET'};
}

export function getPostParams(params) {
  return {
    method : 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body : JSON.stringify(params)
  };
}

export function getFormDataPostParams(params) {
  return {
    method : 'POST',
    headers: {
      'Accept': 'application/json',
      // 'Content-Type':'multipart/form-data'
    },
    body : params
  };
}

export function getPatchParams(params) {
  return {
    method : 'PATCH',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body : JSON.stringify(params)
  };
}

export function stringReplace(origin, ...args) {
    var args = args;
    return origin.replace(/\{(\d+)\}/g,
        function(m, i) {
            return args[i];
        });
}

export function sFetch(url, opt, scb, ecb, requiredResult = true) {
  fetch(url, opt).then((response)=>{
    if(response.ok) {
      if(requiredResult) {
        response.json().then((json)=>{
          if(scb) {
            scb(json, response);
          }
        });
      } else {
        if(scb) {
          scb(null, response);
        }
      }
    } else {
      if(response.status == 400) {
        response.json().then((json)=>{
          if(ecb) {
            ecb(json);
          }
        });
      } else if(response.status == 401) {
        ToastUtil.showShort("Login is invalid, please exit the app to log in again");
        if(ecb) {
          ecb({errcode : -1});
        }
      } else {
        if(ecb) {
          ecb({errcode : -1});
        }
      }
  }}).catch((e) => {
    if(ecb) {
      ecb({errcode : -1});
    }
  });
}

export function compareVersion(ov, nv){
  if(ov && nv && ov != "" && nv != "") {
    let ova = ov.split(".", 3);
    let nva = nv.split(".", 3);
    let o_v_a_symbol = ova[0], n_v_a_symbol = nva[0];
    if(o_v_a_symbol != "" && n_v_a_symbol != "" && o_v_a_symbol < n_v_a_symbol) {
      return CONST.VERSION_UPDATE_RANK.APK_UPDATE;
    }
    var o_v_h_symbol = ova[1], n_v_h_symbol = nva[1];
    if(o_v_h_symbol != "" && n_v_h_symbol != "" && o_v_h_symbol < n_v_h_symbol) {
      return CONST.VERSION_UPDATE_RANK.HOT_UPDATE;
    }
    var o_v_i_symbol = ova[2], n_v_i_symbol = nva[2];
    if(o_v_i_symbol != "" && n_v_i_symbol != "" && o_v_i_symbol < n_v_i_symbol) {
      return CONST.VERSION_UPDATE_RANK.INCREMENTAL_UPDATE;
    }
	}
  return CONST.VERSION_UPDATE_RANK.NO_UPDATE;
}

export function getFileSize(limit) {
  if (!limit) {
    return '--';
  }
  let size = '';
  if (limit < 0.1 * 1024) {
    size = limit.toFixed(2) + 'B';
  } else if (limit < 0.1 * 1024 * 1024) {
    size = (limit / 1024).toFixed(2) + 'KB';
  } else if (limit < 0.1 * 1024 * 1024 * 1024) {
    size = (limit / (1024 * 1024)).toFixed(2) + 'MB';
  } else {
    size = (limit / (1024 * 1024 * 1024)).toFixed(2) + 'GB';
  }

  let sizeStr = size + '';
  let index = sizeStr.indexOf('.');
  let dou = sizeStr.substr(index + 1, 2);
  if (dou == '00') {
    return sizeStr.substring(0, index) + sizeStr.substr(index + 3, 2);
  }
  return size;
}

export function encode(str) {
  let code = str.replace(/\+/g, "%2B");//"+"转义 
  code = code.replace(/\s+/g, "%20");//空格
  code = code.replace(/\&/g, "%26");//"&" 
  code = code.replace(/\#/g, "%23");//"#"
  return code
}