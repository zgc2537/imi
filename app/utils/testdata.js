/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */


// user data
export const testUser1 = {
  balance: "0.00",
  headimgurl: "http://wx.qlogo.cn/mmopen/I1OPdTuWhE9vrdZ9vD5K3xldOdibVXcGCPw9vseQdQIlskQibJPQvziczPpOYbmxiaYynGAGpLfWID9trUcrH86Bb83xhOIg8EOP/0",
  nickname: "kenn",
  phone: null,
  plate_no: "粤FQK883",
  status: "OK",
  wallet_id: 25,
  customerprofile_id: 26,
};

// for skip user
export const skipUser = {
  balance: "0.00",
  headimgurl: null,
  nickname: null,
  phone: null,
  plate_no: null,
  status: null,
  wallet_id: 0,
};

export const testRechargeList = [
  {
    id:1,
    amount: 50,
    cashback: 5,
  },
  {
    id:1,
    amount: 100,
    cashback: 10,
  },
  {
    id:1,
    amount: 200,
    cashback: 20,
  },
  {
    id:1,
    amount: 300,
    cashback: 30,
  },
];

export const testMarkerList1 = [
  {
      "chargerulecombine": {
          "chargerules": [
              {
                  "charge_by_count": false,
                  "charge_end": "23:00:00",
                  "charge_fee_by_count": "20.00",
                  "charge_start": "09:00:00",
                  "chargerulecombine": [
                      "/api/v1/chargerulecombine/25/"
                  ],
                  "deposit_stage1": "20.00",
                  "deposit_stage2": "50.00",
                  "free_duration": 30,
                  "id": 18,
                  "leave_duration": 10,
                  "max_fee_per_count_large": "40.00",
                  "max_fee_per_count_small": "20.00",
                  "max_fee_per_day_large": "40.00",
                  "max_fee_per_day_small": "20.00",
                  "name": "",
                  "pay_monthly": false,
                  "resource_uri": "/api/v1/chargerule/18/",
                  "stagechargerules": [
                      {
                          "chargerule": [
                              "/api/v1/chargerule/18/"
                          ],
                          "id": 15,
                          "resource_uri": "/api/v1/stagechargerule/15/",
                          "stage_charge_large_fee": "20.00",
                          "stage_charge_small_fee": "10.00",
                          "stage_charge_unit": 30,
                          "stage_duration": -1
                      },
                      {
                          "chargerule": [
                              "/api/v1/chargerule/18/"
                          ],
                          "id": 7,
                          "resource_uri": "/api/v1/stagechargerule/7/",
                          "stage_charge_large_fee": "10.00",
                          "stage_charge_small_fee": "5.00",
                          "stage_charge_unit": 30,
                          "stage_duration": 60
                      },
                      {
                          "chargerule": [
                              "/api/v1/chargerule/18/"
                          ],
                          "id": 12,
                          "resource_uri": "/api/v1/stagechargerule/12/",
                          "stage_charge_large_fee": "20.00",
                          "stage_charge_small_fee": "10.00",
                          "stage_charge_unit": 30,
                          "stage_duration": 120
                      }
                  ]
              },
              {
                  "charge_by_count": true,
                  "charge_end": "07:00:00",
                  "charge_fee_by_count": "50.00",
                  "charge_start": "23:00:00",
                  "chargerulecombine": [
                      "/api/v1/chargerulecombine/25/"
                  ],
                  "deposit_stage1": "30.00",
                  "deposit_stage2": "60.00",
                  "free_duration": 0,
                  "id": 19,
                  "leave_duration": 15,
                  "max_fee_per_count_large": "60.00",
                  "max_fee_per_count_small": "30.00",
                  "max_fee_per_day_large": "60.00",
                  "max_fee_per_day_small": "30.00",
                  "name": "",
                  "pay_monthly": false,
                  "resource_uri": "/api/v1/chargerule/19/",
                  "stagechargerules": [
                      {
                          "chargerule": [
                              "/api/v1/chargerule/19/"
                          ],
                          "id": 9,
                          "resource_uri": "/api/v1/stagechargerule/9/",
                          "stage_charge_large_fee": "0.00",
                          "stage_charge_small_fee": "0.00",
                          "stage_charge_unit": 30,
                          "stage_duration": 60
                      }
                  ]
              }
          ],
          "id": 25,
          "name": "碧云公馆商务楼停车场收费规则",
          "remark": null,
          "resource_uri": "/api/v1/chargerulecombine/25/"
      },
      "contact_name": "",
      "contact_phone": "",
      "id": 3,
      "lat": 31.259061,
      "lng": 121.607947,
      "name": "碧云公馆商务楼停车场",
      "pot_num": 110,
      "resource_uri": "/api/v1/parkinglot/3/",
      "road": {
          "contact_name": "",
          "contact_phone": "",
          "id": 5,
          "name": "浙桥路",
          "region": {
              "city": {
                  "code": "310100",
                  "contact_name": "测",
                  "contact_phone": "",
                  "id": 2,
                  "name": "上海市市辖区",
                  "province": {
                      "code": "310000",
                      "contact_name": "周",
                      "contact_phone": null,
                      "id": 12,
                      "name": "上海市",
                      "resource_uri": "/api/v1/province/12/"
                  },
                  "resource_uri": "/api/v1/city/2/"
              },
              "code": "310115",
              "contact_name": "",
              "contact_phone": "",
              "id": 5,
              "name": "浦东新区",
              "resource_uri": "/api/v1/region/5/"
          },
          "resource_uri": "/api/v1/road/5/"
      }
  },
  {
      "chargerulecombine": null,
      "contact_name": "",
      "contact_phone": "",
      "id": 5,
      "lat": 31.151744,
      "lng": 121.311563,
      "name": "亭知路路面",
      "pot_num": 20,
      "resource_uri": "/api/v1/parkinglot/5/",
      "road": {
          "contact_name": "",
          "contact_phone": "",
          "id": 4,
          "name": "亭知路",
          "region": {
              "city": {
                  "code": "310100",
                  "contact_name": "测",
                  "contact_phone": "",
                  "id": 2,
                  "name": "上海市市辖区",
                  "province": {
                      "code": "310000",
                      "contact_name": "周",
                      "contact_phone": null,
                      "id": 12,
                      "name": "上海市",
                      "resource_uri": "/api/v1/province/12/"
                  },
                  "resource_uri": "/api/v1/city/2/"
              },
              "code": "310117",
              "contact_name": "虎",
              "contact_phone": "13900000000",
              "id": 2,
              "name": "松江区",
              "resource_uri": "/api/v1/region/2/"
          },
          "resource_uri": "/api/v1/road/4/"
      }
  },
  {
      "chargerulecombine": {
          "chargerules": [
              {
                  "charge_by_count": false,
                  "charge_end": "00:00:00",
                  "charge_fee_by_count": "0.00",
                  "charge_start": "00:00:00",
                  "chargerulecombine": [
                      "/api/v1/chargerulecombine/28/"
                  ],
                  "deposit_stage1": "20.00",
                  "deposit_stage2": "40.00",
                  "free_duration": 15,
                  "id": 22,
                  "leave_duration": 15,
                  "max_fee_per_count_large": "40.00",
                  "max_fee_per_count_small": "20.00",
                  "max_fee_per_day_large": "40.00",
                  "max_fee_per_day_small": "20.00",
                  "name": "",
                  "pay_monthly": false,
                  "resource_uri": "/api/v1/chargerule/22/",
                  "stagechargerules": [
                      {
                          "chargerule": [
                              "/api/v1/chargerule/22/"
                          ],
                          "id": 14,
                          "resource_uri": "/api/v1/stagechargerule/14/",
                          "stage_charge_large_fee": "20.00",
                          "stage_charge_small_fee": "10.00",
                          "stage_charge_unit": 30,
                          "stage_duration": -1
                      }
                  ]
              }
          ],
          "id": 28,
          "name": "兰州市公共道路收费标准",
          "remark": null,
          "resource_uri": "/api/v1/chargerulecombine/28/"
      },
      "contact_name": "",
      "contact_phone": "",
      "id": 6,
      "lat": 36.067851,
      "lng": 103.879216,
      "name": "雁滩路西段路面",
      "pot_num": 10,
      "resource_uri": "/api/v1/parkinglot/6/",
      "road": {
          "contact_name": "",
          "contact_phone": "",
          "id": 7,
          "name": "雁滩路",
          "region": {
              "city": {
                  "code": "620100",
                  "contact_name": "陈",
                  "contact_phone": "12222222222",
                  "id": 1,
                  "name": "兰州市",
                  "province": {
                      "code": "620000",
                      "contact_name": "1212121",
                      "contact_phone": "13900000000",
                      "id": 18,
                      "name": "甘肃省",
                      "resource_uri": "/api/v1/province/18/"
                  },
                  "resource_uri": "/api/v1/city/1/"
              },
              "code": "620102",
              "contact_name": "吴",
              "contact_phone": "13819191919",
              "id": 1,
              "name": "城关区",
              "resource_uri": "/api/v1/region/1/"
          },
          "resource_uri": "/api/v1/road/7/"
      }
  },
];

export const testBillList1 = [
  {
      "all_amount": null,
      "charge_operator": null,
      "charged_amount": "0.00",
      "create_type": 0,
      "deposit_amount": "0.00",
      "end_park_time": 0,
      "escape_duration": -1,
      "id": 25,
      "leave_time": null,
      "order_no": "PO100000026",
      "order_time": "2017-06-01T08:40:32.338191+00:00",
      "owner": null,
      "park": {
          "adjoin_park": "",
          "devices": [
              {
                  "client_id": "",
                  "description": "CARDECTOR",
                  "device_id": "D0000020",
                  "device_type": 0,
                  "err_status": 0,
                  "id": 20,
                  "manufacture": "费舍泰格",
                  "model": "XX001",
                  "park": "/api/v1/park/20/",
                  "resource_uri": "/api/v1/device/20/",
                  "status": 1
              }
          ],
          "id": 20,
          "park_no": "A020",
          "resource_uri": "/api/v1/park/20/"
      },
      "park_end": null,
      "park_start": "2017-06-01T08:40:32.334979+00:00",
      "parkcar_paytype": 0,
      "parkcar_pics": [],
      "parkcar_platecolor": 0,
      "parkcar_plateno": null,
      "parkcar_platetype": 1,
      "parkcar_type": 0,
      "parkinglot": {
          "contact_name": "",
          "contact_phone": "",
          "id": 3,
          "lat": 31.259061,
          "lng": 121.607947,
          "name": "碧云公馆商务楼停车场",
          "pot_num": 110,
          "resource_uri": "/api/v1/parkinglot/3/",
          "road": {
              "contact_name": "",
              "contact_phone": "",
              "id": 5,
              "name": "浙桥路",
              "region": {
                  "city": {
                      "code": "310100",
                      "contact_name": "测",
                      "contact_phone": "",
                      "id": 2,
                      "name": "上海市市辖区",
                      "province": {
                          "code": "310000",
                          "contact_name": "周",
                          "contact_phone": null,
                          "id": 12,
                          "name": "上海市",
                          "resource_uri": "/api/v1/province/12/"
                      },
                      "resource_uri": "/api/v1/city/2/"
                  },
                  "code": "310115",
                  "contact_name": "",
                  "contact_phone": "",
                  "id": 5,
                  "name": "浦东新区",
                  "resource_uri": "/api/v1/region/5/"
              },
              "resource_uri": "/api/v1/road/5/"
          }
      },
      "pay_amount": null,
      "payment": null,
      "record_operator": null,
      "record_time": null,
      "resource_uri": "/api/v1/parkorder/25/",
      "status": 0,
      "unexpect_leave_reason": null
  },
  {
      "all_amount": "20.00",
      "charge_operator": {
          "bind_posdevices": [],
          "id": 6,
          "last_login": null,
          "name": "王建国1",
          "phone": "13900011114",
          "resource_uri": "/api/v1/staffprofile/6/",
          "stafftype": 1,
          "user": {
              "groups": [
                  "/api/v1/group/1/"
              ],
              "id": 13,
              "is_active": true,
              "resource_uri": "/api/v1/user/13/",
              "username": "ST3101000013",
              "wxuser": null
          }
      },
      "charged_amount": "20.00",
      "create_type": 1,
      "deposit_amount": "20.00",
      "end_park_time": 774,
      "escape_duration": 0,
      "id": 5,
      "leave_time": null,
      "order_no": "PO100000006",
      "order_time": "2017-05-31T02:59:41.380320+00:00",
      "owner": null,
      "park": {
          "adjoin_park": "A001",
          "devices": [],
          "id": 2,
          "park_no": "A002",
          "resource_uri": "/api/v1/park/2/"
      },
      "park_end": "2017-05-31T15:54:09.523226+00:00",
      "park_start": "2017-05-31T02:59:41.378068+00:00",
      "parkcar_paytype": 0,
      "parkcar_pics": [],
      "parkcar_platecolor": 1,
      "parkcar_plateno": "粤FQK883",
      "parkcar_platetype": 1,
      "parkcar_type": 0,
      "parkinglot": {
          "contact_name": "",
          "contact_phone": "",
          "id": 3,
          "lat": 31.259061,
          "lng": 121.607947,
          "name": "碧云公馆商务楼停车场",
          "pot_num": 110,
          "resource_uri": "/api/v1/parkinglot/3/",
          "road": {
              "contact_name": "",
              "contact_phone": "",
              "id": 5,
              "name": "浙桥路",
              "region": {
                  "city": {
                      "code": "310100",
                      "contact_name": "测",
                      "contact_phone": "",
                      "id": 2,
                      "name": "上海市市辖区",
                      "province": {
                          "code": "310000",
                          "contact_name": "周",
                          "contact_phone": null,
                          "id": 12,
                          "name": "上海市",
                          "resource_uri": "/api/v1/province/12/"
                      },
                      "resource_uri": "/api/v1/city/2/"
                  },
                  "code": "310115",
                  "contact_name": "",
                  "contact_phone": "",
                  "id": 5,
                  "name": "浦东新区",
                  "resource_uri": "/api/v1/region/5/"
              },
              "resource_uri": "/api/v1/road/5/"
          }
      },
      "pay_amount": "20.00",
      "payment": {
          "attach": "费舍泰格",
          "body": "停车费",
          "id": 39,
          "out_trade_no": "MO100000039",
          "pay_time": "2017-05-31T15:54:09.549902+00:00",
          "pay_type": 2,
          "resource_uri": "/api/v1/paymentorder/39/",
          "status": 2,
          "total_fee": "20.00"
      },
      "record_operator": {
          "bind_posdevices": [],
          "id": 15,
          "last_login": null,
          "name": "测试12",
          "phone": "13900000000",
          "resource_uri": "/api/v1/staffprofile/15/",
          "stafftype": 3,
          "user": {
              "groups": [
                  "/api/v1/group/5/"
              ],
              "id": 22,
              "is_active": true,
              "resource_uri": "/api/v1/user/22/",
              "username": "MG3101000022",
              "wxuser": null
          }
      },
      "record_time": "2017-05-31T02:59:41.378084+00:00",
      "resource_uri": "/api/v1/parkorder/5/",
      "status": 3,
      "unexpect_leave_reason": null
  },
  {
      "all_amount": null,
      "charge_operator": null,
      "charged_amount": "0.00",
      "create_type": 0,
      "deposit_amount": "0.00",
      "end_park_time": 0,
      "escape_duration": -1,
      "id": 52,
      "leave_time": null,
      "order_no": "PO100000053",
      "order_time": "2017-06-01T11:16:51.783499+00:00",
      "owner": null,
      "park": {
          "adjoin_park": "",
          "devices": [
              {
                  "client_id": "",
                  "description": "CARDECTOR",
                  "device_id": "D0000020",
                  "device_type": 0,
                  "err_status": 0,
                  "id": 20,
                  "manufacture": "费舍泰格",
                  "model": "XX001",
                  "park": "/api/v1/park/20/",
                  "resource_uri": "/api/v1/device/20/",
                  "status": 1
              }
          ],
          "id": 20,
          "park_no": "A020",
          "resource_uri": "/api/v1/park/20/"
      },
      "park_end": null,
      "park_start": "2017-06-01T11:16:51.779388+00:00",
      "parkcar_paytype": 0,
      "parkcar_pics": [],
      "parkcar_platecolor": 0,
      "parkcar_plateno": null,
      "parkcar_platetype": 1,
      "parkcar_type": 0,
      "parkinglot": {
          "contact_name": "",
          "contact_phone": "",
          "id": 3,
          "lat": 31.259061,
          "lng": 121.607947,
          "name": "碧云公馆商务楼停车场",
          "pot_num": 110,
          "resource_uri": "/api/v1/parkinglot/3/",
          "road": {
              "contact_name": "",
              "contact_phone": "",
              "id": 5,
              "name": "浙桥路",
              "region": {
                  "city": {
                      "code": "310100",
                      "contact_name": "测",
                      "contact_phone": "",
                      "id": 2,
                      "name": "上海市市辖区",
                      "province": {
                          "code": "310000",
                          "contact_name": "周",
                          "contact_phone": null,
                          "id": 12,
                          "name": "上海市",
                          "resource_uri": "/api/v1/province/12/"
                      },
                      "resource_uri": "/api/v1/city/2/"
                  },
                  "code": "310115",
                  "contact_name": "",
                  "contact_phone": "",
                  "id": 5,
                  "name": "浦东新区",
                  "resource_uri": "/api/v1/region/5/"
              },
              "resource_uri": "/api/v1/road/5/"
          }
      },
      "pay_amount": null,
      "payment": null,
      "record_operator": null,
      "record_time": null,
      "resource_uri": "/api/v1/parkorder/52/",
      "status": 0,
      "unexpect_leave_reason": null
  },
];

export const testBillList2 = [
  {
      "all_amount": "35.00",
      "charge_operator": {
          "bind_posdevices": [],
          "id": 6,
          "last_login": null,
          "name": "王建国1",
          "phone": "13900011114",
          "resource_uri": "/api/v1/staffprofile/6/",
          "stafftype": 1,
          "user": {
              "groups": [
                  "/api/v1/group/1/"
              ],
              "id": 13,
              "is_active": true,
              "resource_uri": "/api/v1/user/13/",
              "username": "ST3101000013",
              "wxuser": null
          }
      },
      "charged_amount": "35.00",
      "create_type": 1,
      "deposit_amount": "5.00",
      "end_park_time": 2277,
      "escape_duration": 0,
      "id": 4,
      "leave_time": null,
      "order_no": "PO100000005",
      "order_time": "2017-05-26T13:08:37.220583+00:00",
      "owner": null,
      "park": {
          "adjoin_park": "",
          "devices": [
              {
                  "client_id": "",
                  "description": "CARDECTOR",
                  "device_id": "D0000004",
                  "device_type": 0,
                  "err_status": 0,
                  "id": 4,
                  "manufacture": "费舍泰格",
                  "model": "XX001",
                  "park": "/api/v1/park/4/",
                  "resource_uri": "/api/v1/device/4/",
                  "status": 0
              }
          ],
          "id": 4,
          "park_no": "A004",
          "resource_uri": "/api/v1/park/4/"
      },
      "park_end": "2017-05-31T16:02:55.813956+00:00",
      "park_start": "2017-05-26T13:08:37.217875+00:00",
      "parkcar_paytype": 0,
      "parkcar_pics": [],
      "parkcar_platecolor": 0,
      "parkcar_plateno": "沪M12345",
      "parkcar_platetype": 1,
      "parkcar_type": 0,
      "parkinglot": {
          "contact_name": "",
          "contact_phone": "",
          "id": 3,
          "lat": 31.259061,
          "lng": 121.607947,
          "name": "碧云公馆商务楼停车场",
          "pot_num": 110,
          "resource_uri": "/api/v1/parkinglot/3/",
          "road": {
              "contact_name": "",
              "contact_phone": "",
              "id": 5,
              "name": "浙桥路",
              "region": {
                  "city": {
                      "code": "310100",
                      "contact_name": "测",
                      "contact_phone": "",
                      "id": 2,
                      "name": "上海市市辖区",
                      "province": {
                          "code": "310000",
                          "contact_name": "周",
                          "contact_phone": null,
                          "id": 12,
                          "name": "上海市",
                          "resource_uri": "/api/v1/province/12/"
                      },
                      "resource_uri": "/api/v1/city/2/"
                  },
                  "code": "310115",
                  "contact_name": "",
                  "contact_phone": "",
                  "id": 5,
                  "name": "浦东新区",
                  "resource_uri": "/api/v1/region/5/"
              },
              "resource_uri": "/api/v1/road/5/"
          }
      },
      "pay_amount": "35.00",
      "payment": {
          "attach": "费舍泰格",
          "body": "停车费",
          "id": 41,
          "out_trade_no": "MO100000041",
          "pay_time": "2017-05-31T16:02:55.829753+00:00",
          "pay_type": 2,
          "resource_uri": "/api/v1/paymentorder/41/",
          "status": 2,
          "total_fee": "35.00"
      },
      "record_operator": null,
      "record_time": "2017-05-26T13:08:37.217912+00:00",
      "resource_uri": "/api/v1/parkorder/4/",
      "status": 3,
      "unexpect_leave_reason": null
  },
  {
      "all_amount": "40.00",
      "charge_operator": {
          "bind_posdevices": [],
          "id": 15,
          "last_login": null,
          "name": "测试12",
          "phone": "13900000000",
          "resource_uri": "/api/v1/staffprofile/15/",
          "stafftype": 3,
          "user": {
              "groups": [
                  "/api/v1/group/5/"
              ],
              "id": 22,
              "is_active": true,
              "resource_uri": "/api/v1/user/22/",
              "username": "MG3101000022",
              "wxuser": null
          }
      },
      "charged_amount": "40.00",
      "create_type": 0,
      "deposit_amount": "0.00",
      "end_park_time": 3243,
      "escape_duration": 184,
      "id": 2,
      "leave_time": null,
      "order_no": "",
      "order_time": "2017-05-25T03:17:24.224718+00:00",
      "owner": null,
      "park": {
          "adjoin_park": "",
          "devices": [
              {
                  "client_id": "",
                  "description": "CARDECTOR",
                  "device_id": "D0000003",
                  "device_type": 0,
                  "err_status": 0,
                  "id": 3,
                  "manufacture": "费舍泰格",
                  "model": "XX001",
                  "park": "/api/v1/park/3/",
                  "resource_uri": "/api/v1/device/3/",
                  "status": 0
              }
          ],
          "id": 3,
          "park_no": "A003",
          "resource_uri": "/api/v1/park/3/"
      },
      "park_end": "2017-05-27T10:48:44.551476+00:00",
      "park_start": "2017-05-25T03:17:24.221643+00:00",
      "parkcar_paytype": 0,
      "parkcar_pics": [],
      "parkcar_platecolor": 0,
      "parkcar_plateno": "沪M12346",
      "parkcar_platetype": 1,
      "parkcar_type": 0,
      "parkinglot": {
          "contact_name": "",
          "contact_phone": "",
          "id": 3,
          "lat": 31.259061,
          "lng": 121.607947,
          "name": "碧云公馆商务楼停车场",
          "pot_num": 110,
          "resource_uri": "/api/v1/parkinglot/3/",
          "road": {
              "contact_name": "",
              "contact_phone": "",
              "id": 5,
              "name": "浙桥路",
              "region": {
                  "city": {
                      "code": "310100",
                      "contact_name": "测",
                      "contact_phone": "",
                      "id": 2,
                      "name": "上海市市辖区",
                      "province": {
                          "code": "310000",
                          "contact_name": "周",
                          "contact_phone": null,
                          "id": 12,
                          "name": "上海市",
                          "resource_uri": "/api/v1/province/12/"
                      },
                      "resource_uri": "/api/v1/city/2/"
                  },
                  "code": "310115",
                  "contact_name": "",
                  "contact_phone": "",
                  "id": 5,
                  "name": "浦东新区",
                  "resource_uri": "/api/v1/region/5/"
              },
              "resource_uri": "/api/v1/road/5/"
          }
      },
      "pay_amount": "40.00",
      "payment": {
          "attach": "费舍泰格",
          "body": "停车费",
          "id": 33,
          "out_trade_no": "MO100000033",
          "pay_time": "2017-05-27T13:53:07.061762+00:00",
          "pay_type": 0,
          "resource_uri": "/api/v1/paymentorder/33/",
          "status": 2,
          "total_fee": "40.00"
      },
      "record_operator": {
          "bind_posdevices": [],
          "id": 15,
          "last_login": null,
          "name": "测试12",
          "phone": "13900000000",
          "resource_uri": "/api/v1/staffprofile/15/",
          "stafftype": 3,
          "user": {
              "groups": [
                  "/api/v1/group/5/"
              ],
              "id": 22,
              "is_active": true,
              "resource_uri": "/api/v1/user/22/",
              "username": "MG3101000022",
              "wxuser": null
          }
      },
      "record_time": "2017-05-26T15:08:42.470881+00:00",
      "resource_uri": "/api/v1/parkorder/2/",
      "status": 3,
      "unexpect_leave_reason": null
  },
];
