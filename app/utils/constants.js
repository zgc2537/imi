/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */

const env_config =
  {
     host_name: "http://valveexpertise.com",
     update_host: "https://park-update.oss-cn-shanghai.aliyuncs.com/SmartParkUser/",
     wx_app_id: "wx4b58a16543615040",
  };

export const ACTION_BAR_HEIGHT = 72;

export const VERSION_CODE = 1;
export const VERSION_NAME = "1.0.0";

export const HOST_NAME = env_config.host_name;
export const UPDATE_HOST_NAME = env_config.update_host;
export const API_VERSION = "api/v1";

export const REQ_URL_LOGIN = `${HOST_NAME}/usermgr/user_login/`;
export const REQ_URL_CONTACT = `${HOST_NAME}/api/v1/contact/?limit=0`;
export const REQ_URL_LOGOUT = `${HOST_NAME}/usermgr/login_out`;

export const REQ_URL_ASSET = `${HOST_NAME}/api/v1/asset/`;
export const REQ_URL_ASSET_FILE = `${HOST_NAME}/api/v1/assetfile/?asset__id={0}`;
export const REQ_URL_ASSET_ORDER = `${HOST_NAME}/api/v1/assetorder/?asset__id={0}`;
export const REQ_URL_PRODUCT = `${HOST_NAME}/api/v1/product/?asset__id={0}`;
export const REQ_URL_SUCCESS_CASE = `${HOST_NAME}/api/v1/successcase/?order_by=id`;
export const REQ_URL_OPPORTUNITY = `${HOST_NAME}/api/v1/opportunity/?order_by=-id`;
export const REQ_URL_OPPORTUNITY_CREATE = `${HOST_NAME}/api/v1/opportunity/`;
export const REQ_URL_VALUES = `${HOST_NAME}/api/v1/completevalues/?order_by=-id`;
export const REQ_URL_REPORT = `${HOST_NAME}/api/v1/servicereport/`;
export const REQ_URL_UPLOAD_FILE = `${HOST_NAME}/api/v1/servicereport/upload_file/`;
export const REQ_URL_GET_DATA = `${HOST_NAME}/get_data/`;
export const REQ_URL_BURIED = `${HOST_NAME}/api/v1/clickcount/`


//自定义事件
export const VENDOR_EVENT_STACK_ROUTE_CHANGE  = 'STACK_ROUTE_CHANGE' ;

export const STORAGE_CARNO_HISTORY = 'recentCarnos';
export const CARNO_HISTORY_MAX = 3;

export const SMS_CODE_TIMEOUT = 60; //s.

export const CARNO_TEXT_INPUT_MAX = 9;
export const CARNO_INPUT_PLACEHOLDER = '请输入车牌号，例如苏B123GT';
export const CARNO_INPUT_PLACEHOLDER2 = '请输入车牌号进行查询';
export const CARNO_INPUT_PLACEHOLDER3 = '请输入目标地';
export const CARNO_INPUT_TIP_ERRSTR = '请输入正确的车牌号，例如: 苏B123GT';
export const CARINFO_HAS_NOT_FOUND = '没有找到您的汽车信息， T.T';
export const CARNO_UNPAID_RECORD_HAS_NOT_FOUND = '没有找到您汽车的账单';
export const CARNO_BINDED_NOT_FOUND = '您还没有绑定车牌号呢';
export const PHONE_NO_TEXT_INPUT_MAX = 13;
export const NEW_PHONE = '新手机号:';
export const OLD_PHONE = '原手机号:';
export const PHONE_NO_INPUT_TIP_ERRSTR = '请输入正确的手机号，例如:13900000000 或者 0791-83897371';
export const SMS_CODE_INPUT_TIP_ERRSTR = '验证码有误';
export const SMS_CODE_SEND_OK_TIP = '验证码成功发送至您的手机上，请查收';
export const SMS_CODE_SEND_NG_TIP = '验证码发送失败，请重试';
export const PAY_MAIN_BALANCE_TIP_ERRSTR = '余额不足，请充值';
export const HAS_NO_LOGGIN_ERRSTR = '请先登录';
export const WALLET_NO_INFO = '没有找到您的钱包信息呢';
export const SEARCH_WALLET_RECORD_NO_INFO = "没有找到您的钱包记录呢";
export const SEARCH_MY_POINT_NO_INFO = "没有找到您的积分记录呢";
export const SERVER_ERROR = "网络或服务器错误";
export const ALERT_TITLE_OK = '成功';
export const ALERT_TITLE_TIP = '提示';
export const ALERT_TITLE_FAILED = '失败';
export const ADD_CAR_INFO = "请输入车牌号";
export const GO_TO_BIND_PHONE = "绑定手机号方便您使用停车功能, 请前往绑定";
export const GO_TO_BIND_CARNUM = "您尚无车牌号, 请先绑定车牌"
export const SELECT_CAR_NUM = "请选择车牌号!"
export const AFF_CAR_NUM = "请确认您所输入的泊位号是正确的"
export const SUCC_CAR_NUM = "请输入正确的车牌号"
//Stack Navigation name
export const STATUS_BAR_HEIGHT = 68;
export const STATUS_BAR_IOS_HEIGHT = 60;
export const NAVIGATION_BAR_HEIGHT = 60;
export const NAV_SPLASH = 'Splash';
export const NAV_LOGIN = 'Login';
export const NAV_HOME_PAGE = "Home";
export const NAV_SUPPORT_PAGE = "Support";
export const NAV_UPGRADES_PAGE = "Upgrades";
export const NAV_MANUAL_INPUT_PAGE = "ManualSearchInput";
export const NAV_ASSET_PAGE = "Asset";
export const NAV_ASSET_INFO_PAGE = "AssetInfo";
export const NAV_OPPORTUNITY_DETAIL_PAGE = "OpportunityDetail";
export const NAV_SELECT_DATABASE = "SelectDatabase";
export const NAV_MY_UPGRADE = "MyUpgrade";
export const NAV_CASE_STUDIES = "CaseStudies";
export const NAV_FIND_UPGRADE = "FindUpgrade"
export const NAV_DOCUMENTS = "Documents";
export const NAV_CONTACTS = "Contacts";
export const NAV_FILTER = "Filter";
export const NAV_CONTACT_DETAIL = "ContactDetail";
export const NAV_SERVICE_REPORT = "ServiceReport";
export const NAV_REPORT_DETAIL = "ReportDetail";
export const NAV_ADD_SERVICE_REPORT = "AddServiceReport";
export const NAV_SERVICE_REPORT_DETAIL = "ServiceReportDetail";
export const NAV_SPARE_PARTS = "SpareParts";
export const NAV_ORDER_HISTORY = "OrderHistory";
export const NAV_VALVE_SEARCH = "ValveSearch";
export const NAV_VALVE_SEARCH_DETAIL = "ValveSearchDetail";
export const NAV_PRODUCT_NUMBER = "ProductNumber";
export const NAV_SCAN_TAG = "ScanTag";
export const NAV_TAKE_PIC = "TakePic";
export const NAV_ASSET_CHECK = "AssetCheck";
export const NAV_VALVE_DOCTOR_REQUEST = "ValveDoctorRequest";
export const NAV_VALVE_DOCTOR_REQUEST_SUBMITTED = "ValveDoctorRequestSubmitted";
export const NAV_CHAT = "Chat";
export const NAV_IMAGE_SUBMITTED = "ImageSubmitted";
export const NAV_UTILITIES = "Utilities";
export const NAV_VALUE_INPUT = "ValueInput";
export const NAV_UPGRADE_PAC_DETAIL = "UpgradePackageDetail";

export const LIST_ONE_PAGE_LIMIT = 20;
export const LIST_INITIAL_SIZE = 20;
export const LIST_END_REACHED_THRESHOLD = 20;

export const PARK_CAR_PLATE_COLOR_CLASS = {
  0: {backgroundColor:'#06c', color:'#fff'}, //BLUE = 0
  1: {backgroundColor:'#fc0', color:'#000'}, //YELLOW = 1
  2: {backgroundColor:'black', color:'#fff'}, //BLACK = 2
  3: {backgroundColor:'white', color:'#000'}, //WHITE = 3
  4: {backgroundColor:'green', color:'#000'}, //GREEN = 4
};

export const VERSION_UPDATE_RANK = {
  NO_UPDATE : 0,
  APK_UPDATE : 1,
  HOT_UPDATE : 2,
  INCREMENTAL_UPDATE : 3
};

export const NETWORK_REQUEST_STATE = {
  DOING : 'doing',
  DONE : 'done',
  ERROR : 'error',
  NULL : 'null'
};

export const ERR_MSG_INFO = {
  "-2" : { errmsg:"RETRY TIMEOUT",  desc:"重试超时" },
  "-1" : { errmsg:"NETWORK OR SERVER ERROR",  desc:"网络或服务器错误" },
  4001: { errmsg:"ERR INVALID PARAM",  desc:"参数错误"},
  4002: { errmsg:"NEED LOGIN", desc:"未登陆",   },
  4101: { errmsg:"USER LOGIN INVALID USER",  desc:"用户被冻结",    },
  4102: { errmsg:"USER LOGIN ERROR USERNAME OR PASSWORD", desc:"用户名或密码错误", },
  4401: { errmsg:"ERROR VERIFY CODE",        desc:"验证码错误",    },
  4501: { errmsg:"ERROR USERNAME OR PASSWORD",   desc:"用户名或者密码错误",   },
  4502: { errmsg:"ERROR SEND VERIFY CODE",   desc:"发送验证码失败",    },
  4503: { errmsg:"ERROR MULTI USER FOUND",  desc:"重复用户",    },
  4601: { errmsg:"invalid appid",  desc:"appid错误",    },
  4701: { errmsg:"ERROR NO PHONE",   desc:"缺少手机号", },
  4801: { errmsg:"PHONENUMBER EXIST",  desc:"手机号码已存在",    },
  4901: { errmsg:"GEN PAYMENT ORDER ERROR",  desc:"支付失败",    },
  4902: { errmsg:"WALLET NOT ENOUGH MONEY",  desc:"钱包余额不足",    },
  4903: { errmsg:"WALLET DISABLED",  desc:"您正在申请余额退款，余额暂不可用", },
  5001: { errmsg:"NO BOOKABLE PARKS",  desc:"无可预约的车位",    },
  5003: { errmsg:"NO ORDER DETECTED",  desc:"创建订单错误, 该泊位车辆未离场", },
  5002: { errmsg:"NO BOOK CHANCE",  desc:"无法预约，取消预约次数达到上限",    },
  5004: { errmsg:"HAVE BOOKED",   desc:"您已有预约订单" },
  5005: { errmsg:"ERROR PARKNO",   desc:"无效的车位" },
  5006: { errmsg:"DUPLICATED PARKNO", desc:"重复的车位"},
  5209: { errmsg:"ERROR LUCKY MONEY AMOUNT", desc:"红包余额不足，请重新提交订单"},
  5102: { errmsg:"ERR_SPECIALPAYCAR_NUM_EXCEED", desc:"该停车场月卡配额不足!"},
  5210: { errmsg:"ERR WALLET NO ENOUGH LUCKYMONEY", desc:"红包余额不足，请重新提交订单"},
  5211: { errmsg:"PACKAGE NO DAY COUNT", desc:"红包已超出每日限制" },
  5212: { errmsg:"PACKAGE STATUS ERROR", desc:"红包礼包状态不正确" },
  5213: { errmsg:"NO LUCKY MONEY GET RULE", desc:"无红包获取规则" },
};

export const OPPORTUNITY_STATUS = {
  0: "Proposal",
  1: "Sales Lead",
  2: "Negotiation",
  3: "Qualification"
}

export const DATA_BASE = {
  'contact': 'contact',
  'asset': 'asset',
  'report': 'asset',
  'opportunity': 'opp',
  'values': 'complete_values',
  'plant_input': 'plant_input'
}

export const filterType = () => {
  return {
    contact: [
      {
        name: 'Department',
        key: 'dept',
        options: [],
        selectValue: ''
      },
      {
        name: 'Job Title',
        key: 'job_title',
        options: [],
        selectValue: ''
      },
      {
        name: 'Location',
        key: 'location',
        options: [],
        selectValue: ''
      }
    ],
    asset: [
      {
        name: 'Industry',
        key: 'industry',
        options: [],
        selectValue: ''
      },
      {
        name: 'Plant Name',
        key: 'plant_name',
        options: [],
        selectValue: ''
      },
      {
        name: 'Application',
        key: 'application',
        options: [],
        selectValue: ''
      },
      {
        name: 'Product Type',
        key: 'product_type',
        options: [],
        selectValue: ''
      },
      {
        name: 'Model',
        key: 'model',
        options: [],
        selectValue: ''
      }
    ],
    report: [
      {
        name: 'Industry',
        key: 'industry',
        options: [],
        selectValue: ''
      },
      {
        name: 'Plant Type',
        key: 'plant_type',
        options: [],
        selectValue: ''
      },
      {
        name: 'Plant Name',
        key: 'plant_name',
        options: [],
        selectValue: ''
      }
    ],
    opportunity: [
      {
        name: 'Industry',
        key: 'industry',
        options: [],
        selectValue: ''
      },
      {
        name: 'Plant Type',
        key: 'plant_type',
        options: [],
        selectValue: ''
      },
      {
        name: 'Application',
        key: 'application',
        options: [],
        selectValue: ''
      },
      {
        name: 'Product Type',
        key: 'product_type',
        options: [],
        selectValue: ''
      },
      {
        name: 'Upgrade Type',
        key: 'upgrade_type',
        options: [],
        selectValue: ''
      }
    ],
    values: [
      {
        name: 'Application',
        key: 'application',
        options: [],
        selectValue: ''
      },
      {
        name: 'Product Type',
        key: 'product_type',
        options: [],
        selectValue: ''
      },
    ]
  }
}

export const oppCreatType = () => {
  return [
    {
      name: 'Industry',
      key: 'industry',
      options: [],
      selectValue: ''
    },
    {
      name: 'Plant Type',
      key: 'plant_type',
      options: [],
      selectValue: ''
    },
    {
      name: 'Application',
      key: 'application',
      options: [],
      selectValue: ''
    },
    {
      name: 'Product Type',
      key: 'product_type',
      options: ['Control Valve', 'BTG', 'Turbine Bypass System', 'Attemporator'],
      selectValue: ''
    }
  ]
}
