/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */
import {
    Image,
    StyleSheet,
    Dimensions
} from 'react-native';

import * as CONST from './constants';

const { height, width } = Dimensions.get('window');

// ================================================
// LoadingModal config
// ================================================
export const LoadingModalStyles = StyleSheet.create({
    modal:{
        justifyContent: 'center',
        alignItems: 'center',
        width:120,
        height:120,
        backgroundColor:'#777',
        borderRadius:7,
    },
    text:{
        marginTop:15,
        color:'#fff',
    },
});

// ================================================
// PopupMapInfoModal config
// ================================================
export const PopupMapInfoModalStyles = StyleSheet.create({
    modal:{
        justifyContent: 'center',
        alignItems: 'center',
        //width:width,
        height:(height-CONST.STATUS_BAR_HEIGHT-CONST.NAVIGATION_BAR_HEIGHT)/2,
        backgroundColor:'#FFF',
        borderRadius:0,
        borderTopWidth:1,
        borderBottomWidth:1,
        borderColor:'#efefef',
    },
    text:{
        marginTop:15,
        color:'#fff',
    },
});

// ================================================
// NaviSelectModal config
// ================================================
export const NaviSelectModalStyles = StyleSheet.create({
    modal:{
        justifyContent: 'center',
        alignItems: 'center',
        width: 220,
        height:260,
        backgroundColor:'#FFF',
        borderRadius:5,
        borderTopWidth:1,
        borderBottomWidth:1,
        borderColor:'#efefef',
    },
    lable:{
        marginTop: 20,
        marginBottom: 10,
        marginLeft:10,
        marginRight:10,
        color:'#999',
        fontSize:15,
    },
    scrolllist:{
        width: 220,
        paddingLeft:35,
        paddingRight:35,
    },
    button:{
        marginTop:8,
        marginBottom:8,
        //paddingTop:10,
        //paddingBottom:10,
        //paddingLeft:15,
        //paddingRight:15,
        width: 150,
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth:1,
        borderRadius:5,
        borderColor:'#4880bb',
    },
    text:{
        fontSize:18,
        color:'#4880bb',
    },
});

// ================================================
// NewTextInput config
// ================================================
export const NewTextInputStyles = StyleSheet.create({
    default:{
        height : 40,
        paddingLeft : 20,
        paddingRight : 20,
        fontSize : 16,
        color : '#252525',
        letterSpacing : 40,
        backgroundColor : '#f2f2f2',
        borderRadius : 50
    },
});
