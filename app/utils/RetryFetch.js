'use strict';

import * as CONST from '../utils/constants';
import { sFetch } from './utils';
const DEFAULT_RETRY_NUM = 0;
const DEFAULT_RETRY_INTERVAL = 3000;
export default class RetryFetch {
  constructor(retryInterval, retryNum) {
    this._retryInterval = DEFAULT_RETRY_INTERVAL;
    this._retryMaxNum = DEFAULT_RETRY_NUM;
    this._retryNum = 0;
    this._cancelled = false;
    if(retryInterval) {
      this._retryInterval = retryInterval;
    }
    if(retryNum) {
      this._retryMaxNum = retryNum;
    }
  }

  start = (url, opt, rcon, scb, ecb) => {
    let errorCallback = (error) => {
      if(!this._cancelled) {
        ecb(error);
      }
    };
    let successCallback = (json) => {
      if(!this._cancelled) {
        if(rcon(json)) {
          scb(json);
        } else {
          console.log("retry");
          if(!this._retryMaxNum || this._retryNum < this._retryMaxNum){//_retryMaxNum为0表示不限次数
            this._timeoutId = setTimeout(()=>{
              this.start(url, opt, rcon, scb, ecb);
            }, this._retryInterval);
          } else {
            if(ecb) {
              ecb({errcode : -2});
            }
          }
        }
      }
    };
    this._retryNum++;
    sFetch(url, opt, successCallback, errorCallback);
  };

  stop = () => {
    this._cancelled = true;
    clearTimeout(this._timeoutId);
    if(!this._retryMaxNum) {
      this._retryMaxNum = 1;
    }
    this.retryNum = this._retryMaxNum;
  };
}
