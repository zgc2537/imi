/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */

import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import {persistStore, autoRehydrate} from 'redux-persist';
import {AsyncStorage} from 'react-native';
import rootReducer from '../reducers/index';

const middlewares = [];
const { logger } = require('redux-logger');

//const logger = store => next => action => {
//    if(typeof action === 'function') console.log('dispatching a function');
//    else console.log('dispatching',action);
//    let result = next(action);
//    console.log('next state', store.getState());
//    return result;
//}

middlewares.push(thunk);

/* global __DEV__  */
if (__DEV__) {
    middlewares.push(logger);
}

let createAppStore = applyMiddleware(...middlewares)(createStore);

export default function configureStore(onComplete:()=>void){
    const store = autoRehydrate()(createAppStore)(rootReducer);
    let opt = {
        storage: AsyncStorage,
        transform: [],
    };
    persistStore(store,opt,onComplete);
    return store;
}

