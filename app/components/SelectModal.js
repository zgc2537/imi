'use strict'

import React, {Component} from 'react';

import {
  View,
  Dimensions,
  StyleSheet,
  Text,
  FlatList,
  TouchableHighlight
} from 'react-native';
import SModal from '../components/SModal';
import SearchBar from '../components/SearchBar';

const {height, width} = Dimensions.get('window');

export default class SelectModal extends Component {
  state = {
    visible : false,
    list: []
  };

  componentDidMount() {
    const { data, visible } = this.props
      this.setState({
        list: data,
        visible: visible
    })
  }

  componentWillReceiveProps(nextProps) {
      if(this.state.visible !== nextProps.visible) {
        this.setState({visible: nextProps.visible})
      }
      if(this.state.list !== nextProps.list) {
        this.setState({list: nextProps.list})
      }
  }
  _onChange = (text) => {
    const { list } = this.props
    if(text) {
      const data = list.filter(item => item.toUpperCase().indexOf(text.toUpperCase()) != -1)
      this.setState({ list: data})
    }else {
      this.setState({ list: list })
    }
  }

  _renderItem = ({item, index}) => {
    const { onChange } = this.props
      return (
          <View>
              <TouchableHighlight
                underlayColor="rgba(229, 241, 255, 0.9)" 
                onPress={()=>{onChange && onChange(item)}}
                >
                <View style={styles.itemRow}>
                    <Text numberOfLines={1}>{item}</Text>
                </View>
              </TouchableHighlight>
              <View style={{height: index == (this.state.list.length - 1) ? 10 : 0}} />
          </View>
        
          
      )
  }

  _keyExtractor = (item, index) => index

  render() {
    const { placeholder } = this.props
    return (
        <SModal 
            visible={this.state.visible} 
            modalStyles={{alignItems: 'flex-start', justifyContent: 'flex-start'}} 
            canWindowCloseOnTouchOutside={true}
            onTouchOutside={()=> {this.props.onClose && this.props.onClose()}}>

            <View style={{width: width, backgroundColor: '#f2f5f6'}}>
                <SearchBar
                    placeholder={placeholder}
                    hasFilter={false}
                    hasClear={false}
                    onChange={this._onChange}
                />
                <FlatList
                    style={styles.scorllStyle}
                    data={this.state.list}
                    renderItem={this._renderItem}
                    keyExtractor={this._keyExtractor}
                    // ItemSeparatorComponent={() => ( <View style={{height: 10}} /> )}
                />
            
            </View>
        </SModal>
    );
  }

}

var styles = StyleSheet.create({
    scorllStyle: {
        maxHeight: height / 3 * 2,
    },
    itemRow: {
        marginHorizontal: 20,
        paddingVertical: 10,
    }
})
