/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */
import React, { PropTypes } from 'react';
import { Dimensions, StyleSheet, Image, View, ViewPropTypes, Text, TouchableOpacity,Platform } from 'react-native';
import * as CONSTS from '../utils/constants';
const { width } = Dimensions.get('window');
let btnHeight = 64;

const propTypes = {
  onPress: PropTypes.func,
  btnHalfFlag : PropTypes.bool,
  btnGreenFlag : PropTypes.bool,
  btnPayTipFlag : PropTypes.bool,
  disabled: PropTypes.bool,
  activeOpacity: PropTypes.number,
  viewStyle: ViewPropTypes.style,
  style: Text.propTypes.style,
  disabledStyle: ViewPropTypes.style,
  title: PropTypes.string,
  titleStyle: Text.propTypes.style,
  titleDisabledStyle: Text.propTypes.style,
  iconSrc: PropTypes.number,
  iconStyle: Text.propTypes.style,
  btnImageStyle : Text.propTypes.style
};

  _btnSrc = (type, disabled, isgreen) => {
    switch (type) {
      case CONSTS.BUTTON_STYLE.TYPE1:
        if(isgreen) {
          return require('../img/btn_button_04.png');
        } else {
          if(disabled) {
            return require('../img/btn_button_01.png');
          } else {
            return require('../img/btn_button_02.png');
          }
        }
      case CONSTS.BUTTON_STYLE.TYPE2:
        if(disabled) {
          return require('../img/button_pop_01.png');
        } else {
          return require('../img/button_pop_02.png');
        }
      case CONSTS.BUTTON_STYLE.TYPE3:
        if(disabled) {
          return require('../img/btn_half_01.png');
        } else {
          return require('../img/btn_half_02.png');
        }
      case CONSTS.BUTTON_STYLE.TYPE4:
        if(disabled) {
          return require('../img/btn_home_01.png');
        } else {
          return require('../img/btn_home_02.png');
        }
      case CONSTS.BUTTON_STYLE.TYPE5:
        if(disabled) {
          return require('../img/button_pop_half_01.png');
        } else {
          return require('../img/button_pop_half_02.png');
        }
      case CONSTS.BUTTON_STYLE.TYPE6:
        if(disabled) {
          return require('../img/btn_account_recharge.png');
        } else {
          return require('../img/btn_account_recharge2.png');
        }
      default:
        return require('../img/button_pop_02.png');
    }
  };

  _btnStyle = (type) => {
    switch (type) {
      case CONSTS.BUTTON_STYLE.TYPE1:
        return jsonStyles.btnImage1;
      case CONSTS.BUTTON_STYLE.TYPE2:
        return jsonStyles.btnImage2;
      case CONSTS.BUTTON_STYLE.TYPE3:
        return jsonStyles.btnImage3;
      case CONSTS.BUTTON_STYLE.TYPE4:
        return jsonStyles.btnImage4;
      case CONSTS.BUTTON_STYLE.TYPE5:
        return jsonStyles.btnImage5;
      case CONSTS.BUTTON_STYLE.TYPE6:
        return jsonStyles.btnImage6;
      default:
        return jsonStyles.btnImage2;
    }
  };

const Button = ({
  onPress,
  btnType,
  btnGreen,
  disabled,
  activeOpacity,
  style,
  disabledStyle,
  title,
  titleStyle,
  titleDisabledStyle,
  iconSrc,
  iconStyle,
  btnImageStyle
}) => {

  let icon;
  let btn;

  if(iconSrc){
    icon = <Image source={iconSrc} style={[styles.icon, iconStyle]} />;
  }
  btn = <Image source={this._btnSrc(btnType, disabled, btnGreen)} resizeMode="contain" style={this._btnStyle(btnType)} />;
  let newBtn;
  newBtn = (
    <TouchableOpacity
      style={[{height:btnHeight},style]}
      onPress={onPress}
      disabled={disabled}
      activeOpacity={activeOpacity}
    >
      <View style={styles.contentContainer}>
          {btn}
          <View style={styles.content}>
            {icon}
            <Text style={disabled?[styles.title, titleStyle, styles.disabledTitle, titleDisabledStyle]:[styles.title, titleStyle]}>
              {title}
            </Text>
          </View>
      </View>
    </TouchableOpacity>
  );
  return (
    <View>
      {newBtn}
    </View>

)};

const jsonStyles = {
  container:{
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: btnHeight
  },
  disabled:{
    backgroundColor: '#4980B6',
  },
  btnImage1: {
    width: 325,
    height: btnHeight
  },
  btnImage2: {
    width: 246,
    height: btnHeight
  },
  btnImage3: {
    width: 148,
    height: btnHeight
  },
  btnImage4: {
    width: 124,
    height: btnHeight
  },
  btnImage5: {
    width: 112,
    height: btnHeight
  },
  btnImage6: {
    width: 64,
    height: 44
  },
  contentContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 0,
    height: btnHeight,
    borderRadius: 50,
  },
  content: {
    position: 'absolute',
    flexDirection: 'row',
    alignItems: 'center'
  },
  disabledStyle : {
    backgroundColor: '#91b3d6',
  },
  icon: {
    width: 30,
    height: 30,
    marginRight: 12,
  },
  title: {
    letterSpacing: 1,
    color: '#fff',
    fontSize: 20,
    backgroundColor : 'transparent',
  },
  disabledTitle:{
    color: '#fff',
  },
};

Button.propTypes = propTypes;

Button.defaultProps = {
  onPress() {},
  btnHalfFlag : false,
  btnGreenFlag : false,
  btnPayTipFlag : false,
  disabled: false,
  activeOpacity: 0.8,
  style: jsonStyles.container,
  disabledStyle: jsonStyles.disabled,
  title: '',
  titleStyle: jsonStyles.title,
  titleDisabledStyle: jsonStyles.disabledTitle,
  iconSrc: 0,
  iconStyle: jsonStyles.icon,
  btnImageStyle : jsonStyles.btnImageStyle
};

const styles = StyleSheet.create(jsonStyles);

export default Button;
