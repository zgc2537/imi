/**
 *
 * Copyright 2015-present reading
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';
import { ActivityIndicator, Text, StyleSheet, View } from 'react-native';
import Modal from 'react-native-modalbox';

export default class LoadingModal extends React.Component {
  constructor(props){
      super(props);
  }

  render(){
    return (
      <Modal style={styles.modal} {...this.props}>
        <ActivityIndicator size="large" color="#fff" />
        <Text style={styles.modalText}>数据加载中...</Text>
      </Modal>
    )
  }
};

const styles = StyleSheet.create({
  modal:{
    justifyContent: 'center',
    alignItems: 'center',
    width:120,
    height:120,
    backgroundColor:'#777',
    borderRadius:7,
  },
  modalText:{
    marginTop:15,
    color:'#fff',
  },
});
