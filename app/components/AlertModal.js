'use strict';

import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  Modal,
  Navigator,
  TextInput,
  ScrollView,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
var { width, height, scale } = Dimensions.get('window');

export default class AlertModal extends Component {
  // 加载完成
  componentDidMount() {
    //
  }
  // view卸载
  componentWillUnmount(){
    //
  }
  // 绘制View
  render() {
    let subView;
    if(this.props.children){
      subView = (<View style={styles.subView}>{this.props.children}</View>)
    }else{
      subView = (
        <View style={styles.subView}>
            <Text style={styles.titleText}>
              {this.props.titileText ? this.props.titleText : '提示'}
            </Text>
            <Text style={styles.contentText}>
              {this.props.contentText ? this.props.contentText : '您确定要删除吗?'}
            </Text>
            <View style={styles.horizontalLine} />
            <View style={styles.buttonView}>
                <TouchableHighlight underlayColor='transparent'
                  style={styles.buttonStyle}
                  onPress={this.props.lBtn} >
                  <Text style={styles.buttonText}>
                    {this.props.rBtnText ? this.props.rBtnText : '确定' }
                  </Text>
                </TouchableHighlight>
                <View style={styles.verticalLine} />
                <TouchableHighlight underlayColor='transparent'
                  style={styles.buttonStyle}
                  onPress={this.props.rBtn}>
                  <Text style={styles.buttonText}>
                    {this.props.lBtnText ? this.props.lBtnText : '取消'}
                  </Text>
                </TouchableHighlight>
           </View>
        </View>
      )
    }
     return (
       <View style={styles.container}>
         <Modal animationType='fade'
                transparent={true}
                visible={this.props.visible}
                onShow={() => {console.log("111")}}
                onRequestClose={() => {}} >
           <View style={styles.modalStyle}>
             {subView}
           </View>
        </Modal>
       </View>
     )
  }

}
// Modal属性
// 1.animationType bool  控制是否带有动画效果
// 2.onRequestClose  Platform.OS==='android'? PropTypes.func.isRequired : PropTypes.func
// 3.onShow function方法
// 4.transparent bool  控制是否带有透明效果
// 5.visible  bool 控制是否显示

// css样式
var styles = StyleSheet.create({
  container:{
    backgroundColor: '#ECECF0',
  },
  // modal的样式
  modalStyle: {
    backgroundColor : 'rgba(0, 0, 0, 0.2)',
    alignItems: 'center',
    justifyContent:'center',
    flex: 1,
  },
  // modal上子View的样式
  subView:{
    width : width - 120,
    backgroundColor:'#fff',
    alignSelf: 'center',
    justifyContent:'center',
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor:'#ccc',
  },
  // 标题
  titleText:{
    marginTop:10,
    marginBottom:5,
    fontSize:16,
    fontWeight:'bold',
    textAlign:'center',
  },
  // 内容
  contentText:{
    margin:8,
    fontSize:14,
    textAlign:'center',
  },
  // 水平的分割线
  horizontalLine:{
    marginTop:5,
    height:0.5,
    backgroundColor:'#ccc',
  },
  // 按钮
  buttonView:{
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonStyle:{
    flex:1,
    height:44,
    alignItems: 'center',
    justifyContent:'center',
  },
  // 竖直的分割线
  verticalLine:{
    width:0.5,
    height:44,
    backgroundColor:'#ccc',
  },
  buttonText:{
    fontSize:16,
    color:'#3393F2',
    textAlign:'center',
  },
});
