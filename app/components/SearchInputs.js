/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */

// { handleSearch, handleClear, handleChangeText, disabled, maxLength, initValue } = this.props

import React , { Component } from 'react';
import {
    StyleSheet,
    Platform,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback
} from 'react-native';
import * as CONST from '../utils/constants';

const dismissKeyboard = require('dismissKeyboard');

export default class SearchInputs extends Component {

    constructor(props){
        super(props);
        this.state = {
          text: this.props.initValue?this.props.initValue:'',
        };
        this._onChangeSearchText = this._onChangeSearchText.bind(this);
        this._onPressSrcBtn = this._onPressSrcBtn.bind(this);
        this._onPressClearBtn = this._onPressClearBtn.bind(this);
    }

    _onChangeSearchText = (text) =>{
      const { needUpperCase } = this.props;

      if(Platform.OS == 'ios'){
      }else{
        needUpperCase && text != '' && ( text = text.toUpperCase() );
      }
      this.setState({text: text});
      this.props.onChangeText && this.props.onChangeText(text);
    };

    _onPressSrcBtn = () =>{
      dismissKeyboard();
      this.props.handleSearch && this.props.handleSearch(this.state.text);
    };

    _onPressClearBtn = () =>{
      this.setState({text:''});
      this.props.handleClear && this.props.handleClear();
    };

    render(){
        let hasClear;
        let clearIcon=null,searchIcon=null;

        if( this.state.text == ''){
          hasClear = false;
          searchIcon = (
            <TouchableOpacity
              style={[styles.searchIcon]}
              onPress={this._onPressSrcBtn}
              disabled={this.props.disabled?this.props.disabled:false}
            >
              <Image
                source={require('../img/ic_title_list_05.png')}
                style={styles.icon}
              />
            </TouchableOpacity>
          );
        }else{
          hasClear = true;
          clearIcon = (
            <TouchableOpacity
              style={[styles.clearIcon,this.props.clearIconStyle]}
              onPress={this._onPressClearBtn}
              disabled={this.props.disabled?this.props.disabled:false}
            >
              <Image
                source={require('../img/ic_title_list_06.png')}
                style={styles.icon}
              />
            </TouchableOpacity>
          );
        }

        return (
          <View style={styles.cotainer}>
            <TextInput style={ hasClear ? [styles.input,styles.clearInput,this.props.style] : [styles.input,this.props.style] }
              underlineColorAndroid='transparent' //设置下划线背景色透明 达到去掉下划线的效果
              returnKeyType={this.props.returnKeyType ? this.props.returnKeyType : "search"}
              clearButtonMode='never'
              placeholder={CONST.CARNO_INPUT_PLACEHOLDER3}
              onChangeText={this._onChangeSearchText}
              value={this.state.text}
              disabled={this.props.disabled?this.props.disabled:false}
              maxLength={this.props.maxLength?this.props.maxLength:null}
              autoCapitalize={this.props.needUpperCase?"characters":"none"}
              onSubmitEditing={this.props.onSubmitEditing}
              onFocus={this.props.onFocus}
              autoFocus={this.props.autoFocus}
            />
            {clearIcon}
            {searchIcon}
          </View>
        );
    }
}

//样式定义
const styles = StyleSheet.create({
  container:{
    position:'relative',
  },
  input:{
    paddingTop : 0,
    paddingBottom : 0,
    height : 34,
    paddingLeft: 42,
    paddingRight: 50,
    borderRadius: 17,
    fontSize : 16,
    color:'#999',
    backgroundColor : '#fff',
  },
  clearInput: {
    paddingLeft : 20,
  },
  searchIcon: {
    position:'absolute',
    width : 20,
    height : 20,
    left : 12,
    top : 7,
  },
  clearIcon: {
    position:'absolute',
    top: 7,
    right: 0,
    paddingRight: 12,
  },
  icon: {
    width: 20,
    height: 20,
  }
});
