/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */

// { handleSearch, handleClear, handleChangeText, disabled, maxLength, initValue } = this.props

import React , { Component } from 'react';
import {
    StyleSheet,
    Platform,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
} from 'react-native';
import * as CONST from '../utils/constants';

const dismissKeyboard = require('dismissKeyboard');

export default class SearchInput extends Component {

    constructor(props){
        super(props);
        this.state = {
          text: this.props.initValue?this.props.initValue:'',
        };
        this._onChangeSearchText = this._onChangeSearchText.bind(this);
        this._onPressSrcBtn = this._onPressSrcBtn.bind(this);
        this._onPressClearBtn = this._onPressClearBtn.bind(this);
    }

    _onChangeSearchText = (text) =>{
      const { needUpperCase } = this.props;
      if(Platform.OS == "android"){
          if(needUpperCase && text){
            this.setState({text: text.toUpperCase()})
          }
      }else{
        this.setState({text: text})
      }
      this.props.onChangeText && this.props.onChangeText(text);
    };

    _onPressSrcBtn = () =>{
      dismissKeyboard();
      const { needUpperCase } = this.props;
      if (needUpperCase) {
        let carno = this.state.text.toUpperCase();
        this.setState({
          text: carno
        }, () => {
          this.props.handleSearch && this.props.handleSearch(this.state.text);
        })
      } else {
        this.props.handleSearch && this.props.handleSearch(this.state.text);
      }
    };

    _onPressClearBtn = () =>{
      this.setState({text:''});
      this.props.handleClear && this.props.handleClear();
    };

    render(){
        let hasClear = false;
        let clearIcon;

        if(this.state.text && this.state.text != ''){
          hasClear = true;

          clearIcon = (
            <TouchableOpacity
              style={styles.clearIcon}
              onPress={this._onPressClearBtn}
              disabled={this.props.disabled?this.props.disabled:false}
            >
              <Image
                source={require('../img/ic_title_list_06.png')}
                style={styles.icon}
              />
            </TouchableOpacity>
          );
        }

        return (
          <View style={styles.cotainer}>
            <TextInput style={hasClear?[styles.input, styles.clearInput]:[styles.input]}
              underlineColorAndroid='transparent' //设置下划线背景色透明 达到去掉下划线的效果
              returnKeyType="search"
              clearButtonMode='never'
              placeholder={CONST.CARNO_INPUT_PLACEHOLDER2}
              onChangeText={this._onChangeSearchText}
              value={this.state.text}
              disabled={this.props.disabled?this.props.disabled:false}
              maxLength={this.props.maxLength?this.props.maxLength:null}
              autoCapitalize={this.props.needUpperCase?"characters":"none"}
              onSubmitEditing={this._onPressSrcBtn}
            />
            {clearIcon}
            <TouchableOpacity
              style={[styles.searchIcon]}
              onPress={this._onPressSrcBtn}
              disabled={this.props.disabled?this.props.disabled:false}
            >
              <Image
                source={require('../img/ic_title_list_05.png')}
                style={styles.icon}
              />
            </TouchableOpacity>
          </View>
        );
    }
}

//样式定义
const styles = StyleSheet.create({
  container:{
    position:'relative',
    height:40,
  },
  input:{
    height:40,
    paddingLeft:42,
    paddingRight:50,
    borderWidth:0,
    borderRadius:50,
    fontSize:16,
    color:'#333333',
    backgroundColor : '#fff',
  },
  clearInput: {
    paddingRight:90,
  },
  searchIcon: {
    position:'absolute',
    top: 10,
    left : 12,
    //right: 0,
    paddingRight : 10
  },
  clearIcon: {
    position:'absolute',
    top: 10,
    right: 0,
    paddingRight: 12,
  },
  icon: {
    width: 20,
    height: 20,
  }
});
