/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */
// import Drawer from 'react-native-drawer';
//import { DefaultRenderer, Actions } from 'react-native-router-flux';

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableHighlight,
  Dimensions,
} from 'react-native';

import * as CONST from '../utils/constants';

const {height,width} = Dimensions.get('window');

class IconList extends React.Component {
  state = {
  }

  _onPress = () => {
    this.props.onPress(this.props)
  }

  render() {
    return (
      // <View style={this.props.listStyle}>
      //   {this.props.contentView == "TextInput" ? (
      //     <View style={styles.listStyle}>
      //       <View style={[styles.listInStyle, this.props.viewStyle]}>
      //         <Image source={this.props.iconSrc} style={[styles.listIcon, this.props.iconStyle]} />
      //         <Text style={[styles.title, this.props.titleStyle]}>{this.props.title}</Text>
      //         <TextInput style={this.props.contentStyle ? this.props.contentStyle : styles.content}
      //           onChangeText={this.props.onChangeText}
      //           value={this.props.value}
      //           underlineColorAndroid='transparent'
      //           editable={this.props.disabled}
      //           placeholder={this.props.placeholder}
      //           placeholderTextColor="#999"/>
      //       </View>
      //     </View>
      //   ) : (
          <TouchableHighlight style={styles.listStyle} underlayColor={'#f8fafc'} onPress={this._onPress} disabled={this.props.disabled ? this.props.disabled : false}>
            <View style={[styles.listInStyle, this.props.viewStyle]}>
              <Image source={this.props.iconSrc} style={[styles.listIcon, this.props.iconStyle]} />
              <Text style={[styles.title, this.props.titleStyle]}>{this.props.title}</Text>
              <Text style={this.props.contentStyle ? this.props.contentStyle : styles.content}>{this.props.content}</Text>
              {this.props.hasRightIcon ? (
                <Image source={require('../img/ic_title_list_07.png')} style={styles.rightIcon} />
              ) : null}
            </View>
          </TouchableHighlight>
        // )}
      // </View>

    );
  }
}

const styles = StyleSheet.create({
  listStyle : {
    height : 48,
    paddingHorizontal : 24,
    backgroundColor : '#fff',
  },
  listInStyle : {
    height : 48,
    flexDirection : 'row',
    alignItems : 'center',
    borderBottomWidth : 1,
    borderColor : '#ddd'
  },
  listIcon : {
    width : 20,
    height : 20
  },
  rightIcon : {
    width : 14,
    height : 14,
    position : 'absolute',
    right : 0
  },
  title : {
    fontSize : 14,
    color : '#686868',
    marginLeft : 14,
    marginRight : 42
  },
  content : {
    paddingLeft : 0,
    fontSize : 14,
    color : '#999',
    width : width - 32 - 100
  }
});

export default IconList;
