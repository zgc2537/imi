'use stric';

import React from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import * as CONST from '../utils/constants';
import { selectCardMonthlyCarNum, selectCardMonthlyParkinglot } from '../actions/user';

const { width, height } = Dimensions.get('window');
const cardWidth = width - 20 * 2;
const cardBgScale = cardWidth / 320;

export default class MonthlyCard extends React.Component {
  onBuyBtnPress = () => {
    this.props.dispatch(selectCardMonthlyCarNum({
      plate_no: this.props.plateNo,
      plate_color: this.props.plateColor,
    }));
    this.props.dispatch(selectCardMonthlyParkinglot({
      ...this.props.parkinglots[0].parkinglot
    }));
    this.props.navigation.navigate(CONST.NAV_CARD_MONTHLY_BUY, {
      carType: this.props.carType,
      paidBackNaviKey: this.props.payBackRoute
    });
  };

  render() {
    return <View style={styles.cardContainer}>
      <Image source={this.props.overDate ? require('../img/bg_card2.png') : require('../img/bg_card1.png')} style={styles.cardBg} resizeMode="contain"/>
      <View style={styles.cardTopContentOccupy}>
        <View style={styles.cardContent}>
          <Text style={styles.plateNo}>{this.props.plateNo}</Text>
          <Text style={styles.validDays}>剩余{this.props.validDays}天</Text>
        </View>
        <TouchableOpacity onPress={this.onBuyBtnPress}>
          <View style={[styles.renewBtn, (this.props.overDate ? styles.overDateRenewBtn : null)]}>
            <Text style={[styles.renewTxt, (this.props.overDate ? styles.overDateRenewTxt : null)]}>{this.props.overDate ? '再次购买' : '续卡'}</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.cardBottomContentOccupy}>
        <Text style={[styles.validRouterTxt, (this.props.overDate ? styles.overDateValidRouterTxt : null)]}>{this.props.validRouter}</Text>
        <Text style={[styles.validRouterTxt, (this.props.overDate ? styles.overDateValidRouterTxt : null)]}>到期时间: {this.props.validDate}</Text>
      </View>
    </View>
  }
}

var styles = StyleSheet.create({
  cardContainer: {
    width: width,
    height: cardBgScale * 140,
    alignItems: 'center'
  },
  cardBg: {
    width: cardWidth,
    height: cardBgScale * 140
  },
  cardTopContentOccupy: {
    position: 'absolute',
    top: 0,
    left: 0,
    paddingTop: 32,
    paddingLeft: 40,
    paddingRight: 44,
    flexDirection: 'row',
    width: width,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  cardBottomContentOccupy: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    paddingLeft: 40,
    paddingBottom: cardBgScale * 10,
    width: cardWidth,
    height: cardBgScale * 34,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  plateNo: {
    color: "#fff",
    fontSize: 20,
    backgroundColor: "transparent"
  },
  validDays: {
    color: "#fff",
    fontSize: 16,
    marginTop: 16,
    backgroundColor: "transparent",
  },
  renewBtn: {
    width: 62,
    height: 28,
    borderRadius: 14,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center"
  },
  renewTxt: {
    color: "#4880bb",
    backgroundColor: "transparent",
    fontSize: 16
  },
  validRouterTxt: {
    color: "#7fb5e6",
    fontSize: 12,
    backgroundColor: "transparent",
  },
  overDateRenewBtn: {
    width: 87
  },
  overDateRenewTxt: {
    color: '#252525'
  },
  overDateValidRouterTxt: {
    color: '#e4e4e4'
  }
});
