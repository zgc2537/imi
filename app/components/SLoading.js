'use strict'

import React, {Component} from 'react';

import {
  View,
  Modal,
  StyleSheet
} from 'react-native';

import * as Progress from 'react-native-progress';
import SModal from './SModal';

export default class SLoading extends Component {
  constructor(props) {
    super(props);
    // this.state = {modalVisible : false};
  }

  // windowCloseOnTouchOutside() {
  //   if(this.props.canWindowCloseOnTouchOutside) {
  //     this.setVisible(false);
  //   }
  // }
  //
  // setVisible(show) {
  //   this.setState({modalVisible : show});
  //   console.log(show);
  // }

  render() {
    return (
      <Modal
        {...this.props}
        animationType={"fade"}
        transparent={true}
        visible={this.props.visible}
        onRequestClose ={() => {}}
        >
         <View style={[styles.modalBackgroundStyle, this.props.modalStyles]}>
            <View style={[styles.innerContainerTransparentStyle]}>
              <Progress.Circle color={'#0EBA54'} indeterminate={true}/>
            </View>
         </View>
      </Modal>
    );
  }

  // componentWillReceiveProps(nextProps) {
  //   console.log(nextProps.visible + "," + nextProps.info);
  //   if(this.props.visible != nextProps.visible){
  //     this.setVisible(nextProps.visible);
  //   }
  //
  // }
}

var styles = StyleSheet.create({
  modalBackgroundStyle : {
    backgroundColor : 'rgba(0, 0, 0, 0.5)',
    flex : 1,
    justifyContent : 'center',
    alignItems : 'center'
  },
  innerContainerTransparentStyle : {
    backgroundColor : 'rgba(0, 0, 0, 0.3)',
    borderRadius : 6,
    padding : 16
  }
});
