'use strict';

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions
} from 'react-native';

const { width, height } = Dimensions.get('window');

export default class EmptyComponent extends Component {
  render() {
    return <View style={[styles.container, this.props.style]}>
      <Text style={[styles.text, this.props.textStyle]}>{this.props.text ? this.props.text : "无数据"}</Text>
    </View>
  }
}

let styles = StyleSheet.create({
  container: {
    height: 45,
    width: width,
    justifyContent: "center",
    alignItems: 'center'
  },
  text: {
    fontSize: 14,
    color: '#999'
  }
});
