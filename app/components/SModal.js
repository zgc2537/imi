'use strict'

import React, {Component} from 'react';

import {
  ActivityIndicator,
  View,
  Modal,
  StyleSheet,
  Text,
  TouchableWithoutFeedback
} from 'react-native';

export default class SModal extends Component {
  state = {
    modalVisible : false,
  };

  setVisible(show) {
    this.setState({modalVisible : show});
  }

  windowCloseOnTouchOutside = () => {
    if(this.props.canWindowCloseOnTouchOutside) {
      this.setVisible(false);
      this.props.onTouchOutside && this.props.onTouchOutside()
    }
  }

  render() {
    return (
      <Modal
        {...this.props}
        animationType={"fade"}
        transparent={true}
        //visible={this.props.modalVisible}
        visible={this.state.modalVisible}
        onRequestClose ={() => {}}
        >
         <TouchableWithoutFeedback onPress={this.windowCloseOnTouchOutside} style={{flex:1}}>
           <View style={[styles.modalBackgroundStyle, this.props.modalStyles]}>
             <TouchableWithoutFeedback>
              {this.props.children}
            </TouchableWithoutFeedback>
           </View>
          </TouchableWithoutFeedback>
      </Modal>
    );
  }

  componentWillMount(props) {
    this.setVisible(this.props.visible);
  }

  componentWillReceiveProps(nextProps) {
    this.setVisible(nextProps.visible);
  }

}

var styles = StyleSheet.create({
  modalBackgroundStyle : {
    backgroundColor : 'rgba(0, 0, 0, 0.5)',
    flex : 1,
    justifyContent : 'center',
    alignItems : 'center'
  },
});
