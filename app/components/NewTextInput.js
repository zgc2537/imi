/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */
import React from 'react';
import { StyleSheet, View, TextInput } from 'react-native';
import * as CONST from '../utils/constants';

export default class NewTextInput extends React.Component {
  render() {
    return (
      <TextInput
        {...this.props}
        style={[styles.defaultInput,this.props.style]}
        underlineColorAndroid='transparent' //设置下划线背景色透明 达到去掉下划线的效果
        editable = {true}
      />
    );
  }
}

const styles = StyleSheet.create({
  defaultInput:{
    height:45,
    paddingLeft:20,
    paddingRight:20,
    fontSize: 20,
    letterSpacing: 40,
    backgroundColor: '#f2f2f2',
    borderRadius: 5
  },
});
