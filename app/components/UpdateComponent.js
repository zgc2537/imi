'use strict'

import React from 'react';

import {
  View,
  Text,
  Image,
  Modal,
  StyleSheet,
  TouchableOpacity,
  ToastAndroid,
  DeviceEventEmitter
} from 'react-native';

import Device from './device';
import { connect } from 'react-redux';
import * as CONSTS from '../utils/constants';
import * as Progress from 'react-native-progress';
import NewButton from './NewButton';
import { sFetch, compareVersion, getGetParams, getErrStr } from '../utils/utils';

export default class UpdateComponent extends React.Component {
  componentDidMount() {
    this.getLatestVersion();
  }

  componentWillUnmount() {
    this.updateListener && this.updateListener.remove();
  }

  wifiConfirm = () => {
    Device.wifiEnabled().then((enabled)=>{
      this.openUpdateModal(enabled);
    }).catch(()=>{
      this.openUpdateModal(true);
    });
  };

  _update = () => {
    this.setState({
      updating : true
    });
    this.updateProgressListener = DeviceEventEmitter.addListener("update-progress", (msg) => {//apk或者bundle文件下载进度监听
      this.setState({updateProgress : msg < 10 ? 0.1 : msg / 100});
    });
    this.updateResultListener = DeviceEventEmitter.addListener("update-event", (msg) => {//apk或者bundle文件下载结果监听
      this.updateProgressListener && this.updateProgressListener.remove();
      this.updateResultListener && this.updateResultListener.remove();
      if(msg) {
        if(this.state.updateRank == CONSTS.VERSION_UPDATE_RANK.HOT_UPDATE) {
          this.unZipBundleFileListener = DeviceEventEmitter.addListener("unzip-event", (msg) => {//bundle文件解压结果监听
            this.unZipBundleFileListener && this.unZipBundleFileListener.remove();
            if(!msg) {
              this.setState({
                updateDialogVisible : false
              });
            }
          });
          Device.hotUpdateUnZip();
        } else {
          //如果用户点了取消安装，回到app，确保更新Dialog关闭掉
          this.setState({
            updateDialogVisible : false
          });
        }
      } else {
        this.setState({
          updateDialogVisible : false
        });
      }
    });
    if(this.state.updateRank == CONSTS.VERSION_UPDATE_RANK.APK_UPDATE) {
      Device.apkUpdate(this.state.updateUrl);
    } else {
      Device.hotUpdate(this.state.updateUrl);
    }
  };

  getLatestVersion = () => {
    sFetch(CONSTS.GET_VERSION, getGetParams(), (json)=>{
      this.getLatestVersionText(CONSTS.VERSION_NAME, json.version_name, json.update_file);
    }, (error)=>{
      ToastAndroid.show(getErrStr(error.errcode), ToastAndroid.LONG);
    });
  }

  openUpdateModal = (wifiEnabled) => {
    this.setState({
      wifiEnabled: wifiEnabled,
      updateDialogVisible: true,
      updating: false
    });
  }

  closeUpdatemodal = () => {
    this.setState({
      updateDialogVisible: false
    });
  }

  getLatestVersionText = (o, n, file) => {
    let rank = compareVersion(o, n);

    if(rank != CONSTS.VERSION_UPDATE_RANK.NO_UPDATE) {
      this.wifiConfirm();
      this.setState({lVersionText : n, updateRank : rank});
      if(rank == CONSTS.VERSION_UPDATE_RANK.APK_UPDATE) {
        this.setState({updateUrl : (CONSTS.UPDATE_HOST_NAME + "apk" + "/" + n + "/" + file)});
      } else if(rank == CONSTS.VERSION_UPDATE_RANK.HOT_UPDATE) {
        this.setState({updateUrl : (CONSTS.UPDATE_HOST_NAME + "bundle" + "/" + n + "/" + file)});
      }
    } else {
      // this.setState({lVersionText : '您的软件是最新版本'});
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <UpdateAlert visible={this.state.updateDialogVisible} updating={this.state.updating}
          progressValue={this.state.updateProgress} wifiEnabled={this.state.wifiEnabled}
            lVersionText={this.state.lVersionText} updateBtnPress={this._update}
              closeBtnPress={this.closeUpdatemodal}/>
          {this.renderChildren()}
      </View>
    );
  }
}

class UpdateAlert extends React.Component {
    state = {
      modalVisible: false
    }

    componentWillReceiveProps(nextProps) {
      this.setState({
        modalVisible : !nextProps.visible ? false : true
      });
    }

    render() {
      return <Modal
        animationType={"fade"}
        transparent={true}
        //visible={this.props.modalVisible}
        visible={this.state.modalVisible}
        onRequestClose ={() => {}}
        >
         <View style={styles.modalBackgroundStyle}>
           { this.props.updating ? <View style={styles.updatingAlertContainer}>
             <Progress.Circle progress={this.props.progressValue}  color='#4880BB'
               unfilledColor='#E8F1FA' thickness={6} showsText={true} size={160}
                strokeCap="round" borderWidth={0} />
               <Text style={styles.updatingAlertTip}>正在下载,请稍候...</Text>
           </View> : <View style={styles.updateAlertContainer}>
             <Image style={styles.updateAlertTitleImg} source={require("../img/pic_update.png")}></Image>
             <Text style={styles.updatePrimaryTitle}>检测到V{this.props.lVersionText}版本更新...</Text>
              {!this.props.wifiEnabled ? <Text style={styles.updateSecondaryTitle}>(当前为非WIFI网络,下载将花费大量流量)</Text> : null}
             <NewButton title="立即升级" style={styles.updateBtn} onPress={this.props.updateBtnPress}/>
             <NewButton title="下次升级" onPress={this.props.closeBtnPress}/>
           </View> }
         </View>
      </Modal>;
    }
}

var styles = StyleSheet.create({
  container : {
    flex : 1
  },
  modalBackgroundStyle : {
    backgroundColor : 'rgba(0, 0, 0, 0.5)',
    flex : 1,
    justifyContent : 'center',
    alignItems : 'center'
  },
  updateAlertContainer: {
    width: 280,
    height: 404,
    borderRadius: 7,
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  updateAlertTitleImg: {
    width: 280,
    height: 192
  },
  updatePrimaryTitle: {
    marginTop: 10,
    fontSize: 14,
    color: '#686868'
  },
  updateSecondaryTitle: {
    marginTop: 10,
    fontSize: 14,
    color: '#686868'
  },
  updateBtn: {
    marginTop: 20
  },
  updatingAlertContainer: {
    width: 280,
    height: 288,
    borderRadius: 8,
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingTop: 46
  },
  updatingAlertTip: {
    fontSize: 14,
    color: '#686868',
    marginTop: 24
  }
});
