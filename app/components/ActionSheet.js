'use strict'

import React, {Component} from 'react';

import {
  View,
  Modal,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions
} from 'react-native';

const {height,width} = Dimensions.get('window');

export default class ActionSheet extends Component {
  state = {
    visible : false
  }

  componentWillMount(){
    this.setState({visible : this.props.visible})
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.visible != this.state.visible){
      this.setState({visible: nextProps.visible})
    }
  }

  render() {
    let actionSheets = this.props.items.BUTTONS.map((item,i)=>{
      return (
       <TouchableOpacity
           key={i}
           style={[styles.actionItem,this.props.itemStyle,i == this.props.items.BUTTONS.length - 1 ? {borderTopWidth:0.5,borderTopColor:'#ccc',marginTop:5} : null]}
           onPress={item.click}>
           <Text style={[styles.actionItemTitle,this.props.itemTitleStyle]}
           >{item.title}</Text>
       </TouchableOpacity>
      )
    })

    return (
      <Modal
        {...this.props}
        animationType={"fade"}
        transparent={true}
        visible={this.state.visible}
        onRequestClose ={() => {}}
        >
         <View style={styles.modalStyle}>
            <View style={styles.subView}>
              <View style={styles.itemContainer}>
                  {this.props.modalTitle ? (
                    <Text style={[styles.actionTitle,this.props.actionTitleStyle]}>
                      {this.props.modalTitle}
                    </Text>
                  ) : null}
                  {actionSheets}
              </View>

          </View>
        </View>
      </Modal>
    );
  }

}

var styles = StyleSheet.create({
  modalStyle:{
    justifyContent:'flex-end',
    alignItems:'center',
    flex:1,
    backgroundColor : 'rgba(0, 0, 0, 0.5)'
  },
  subView:{
      justifyContent:'flex-end',
      alignItems:'center',
      alignSelf:'stretch',
      width:width,
  },
  itemContainer:{
      marginLeft: 0,
      marginRight: 0,
      borderRadius: 0,
      justifyContent:'center',
      alignItems:'center',
      backgroundColor: '#ccc'
  },
  itemContainerBottom : {
    marginBottom : 0
  },
  actionItem:{
      width: width,
      height: 50,
      backgroundColor : '#fff',
      alignItems:'center',
      justifyContent:'center',
      borderBottomColor:'#cccccc',
      borderBottomWidth:0.5,
  },
  actionTitle:{
      fontSize:13,
      color:'#808080',
      textAlign:'center',
      paddingTop:10,
      paddingBottom:10,
      paddingLeft:15,
      paddingRight:15,
  },
  actionItemTitle:{
      fontSize: 18,
      color: '#252525',
      textAlign:'center',
  },
});
