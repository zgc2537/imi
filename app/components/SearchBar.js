/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */

// { handleSearch, handleClear, handleChangeText, disabled, maxLength, initValue } = this.props

import React, {Component} from 'react';
import {
  View,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  TextInput
} from 'react-native';
const {height, width} = Dimensions.get('window');

export default class SearchBar extends Component {
  state = {
    searchContent: ''
  }

  componentDidMount() {
    this.props.value && this.setState({searchContent: this.props.value})
  }

  _onChangeText = (text) => {
    this.setState({ searchContent: text })
    this.props.onChange && this.props.onChange(text)
  }

  _onSearch = () => {
    this.props.onSearch && this.props.onSearch(this.state.searchContent)
  }

  _onFilter = () => {
    this.props.onFilter()
  }

  render() {
    return (
        <View style={[styles.searchBox, this.props.style ? this.props.style : {}]}>
          <View style={[styles.inputBox, this.props.hasFilter ? {} : {flex: 1}]}>
            <TextInput
              style={styles.input}
              value={this.state.searchContent}
              underlineColorAndroid="transparent"
              placeholderTextColor="#8B999F"
              placeholder={this.props.placeholder ? this.props.placeholder : 'Manual Search'}
              onChangeText={this._onChangeText}>
            </TextInput>
            <TouchableOpacity style={styles.search} onPress={this._onSearch}>
              <Image style={styles.searchIcon} source={require('../img/ic_search.png')} />
            </TouchableOpacity>
          </View>
          {
            this.props.hasFilter ? (
              <TouchableOpacity style={styles.filter} onPress={this._onFilter}>
                <Image style={styles.filterIcon} source={require('../img/ic_search_filter.png')} />
              </TouchableOpacity>
            ) : (<View></View>)
          }

        </View>
    );
  }
}

const styles = StyleSheet.create({
  searchBox: {
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#f2f5f6'
  },
  inputBox: {
    width: width - 20 - 24 - 10,
  },
  input: {
    height: 40,
    backgroundColor: '#e2e4e5',
    borderRadius: 4,
    paddingLeft: 15,
    paddingRight: 40,
  },
  search: {
    position: 'absolute',
    right: 0,
    top: 0,
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  filter: {
    flexDirection: 'column',
    justifyContent: 'center'
  },
  searchIcon: {
    width: 18,
    height: 18
  },
  filterIcon: {
    width: 24,
    height: 24
  }
});
