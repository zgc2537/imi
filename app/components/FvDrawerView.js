
import React from 'react';
import {
  DrawerNavigator,
  DrawerItems,
  NavigationActions
} from 'react-navigation';
import { connect } from 'react-redux';
import {
  Image,
  Button,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  Platform,
  StatusBar,
  TouchableHighlight,
  NativeAppEventEmitter
} from 'react-native';

import MyHomeScreen from '../ParkApp';
import TabView from '../components/TabView';
import * as CONST from '../utils/constants';
import { sFetch, getPostParams } from '../utils/utils';
const { width, height } = Dimensions.get('window');
import { getUserMsg } from '../actions/user';
import Getui from 'react-native-getui';
let componentProps;

// const FvDrawerView = DrawerNavigator({
//   Home: {
//     screen: MyHomeScreen,
//     navigationOptions: {
//       drawerLabel: '缴费',
//       drawerIcon: (option) => (
//         <Image
//            source={require('../img/ic_list_profile_04.png')}
//            style={{width:24, height:24}}
//         />
//       ),
//     },
//     // navigationOptions: ({navigation},homeTitle) => DrawerOptions({navigation}),
//   },
//   MyCarNum: {
//     screen: MyCarNum,
//     navigationOptions : {
//       drawerLabel : '我的车牌',
//       drawerIcon: () => (
//          <Image
//            source={require('../img/ic_list_profile_01.png')}
//            style={{width:24, height:24}}
//          />
//        ),
//     },
//   },
//   MyWallet: {
//     //screen: MyWallet,
//     screen: MyWalletContainer,
//     navigationOptions : {
//       drawerLabel : '我的钱包',
//       drawerIcon: () => (
//          <Image
//            source={require('../img/ic_list_profile_02.png')}
//            style={{width:24, height:24}}
//          />
//        ),
//     },
//   },
//   PaidHistory : {
//     screen : PaidHistoryContainer,
//     navigationOptions : {
//       drawerLabel : '缴费记录',
//       drawerIcon : () => (
//         <Image
//              source={require('../img/ic_list_profile_03.png')}
//              style={{width:24, height:24}}
//            />
//       ),
//     },
//   },
//   MyUnpaid : {
//     screen : MyUnpaidContainer,
//     navigationOptions : {
//       drawerLabel : '欠费补缴',
//       drawerIcon: () => (
//         <Image
//           source={require('../img/ic_list_profile_04.png')}
//           style={{width:24, height:24}}
//         />
//       ),
//     },
//   },
//   Setting : {
//     screen : SettingContainer,
//     navigationOptions : {
//       drawerLabel : '设置',
//       drawerIcon: () => (
//         <Image
//            source={require('../img/ic_list_profile_05.png')}
//            style={{width:24, height:24}}
//          />
//       ),
//     },
//   },
// },{
//     drawerWidth: 236,
//     drawerPosition: 'left',
//     contentOptions: {
//       initialRouteName: 'MyWallet',
//       activeItemKey : 'Home',
//       labelStyle : {
//         fontSize : 15,
//         color : '#686868',
//         height : 20,
//         fontWeight : 'normal',
//       },
//       //activeTintColor: 'white',  // 选中文字颜色
//       activeBackgroundColor: 'transparent', // 选中背景颜色transparent
//       inactiveTintColor: '#686868',  // 未选中文字颜色
//       inactiveBackgroundColor: '#fff', // 未选中背景颜色#ff8500
//    },
//
//    contentComponent: props => {
//      const {isLoggedIn, userInfo, status} = componentProps;
//      const { navigate } = props.navigation;
//      let userNickname = "未登录";
//      let userImage = require('../img/ic_menulist_headpic.png');
//
//      if(isLoggedIn){
//        userInfo && userInfo.headimgurl && (userImage = {uri:userInfo.headimgurl});
//        if(userInfo && userInfo.nickname){
//          userNickname = userInfo.nickname;
//        }
//      }
//
//      return (
//        <ScrollView>
//          <View style={styles.container}>
//            <TouchableHighlight onPress={() => {
//              if(isLoggedIn) {
//                props.navigation.navigate(CONST.NAV_FV_ME);
//              } else {
//                props.navigation.navigate(CONST.NAV_LOGIN);
//              }
//            }}>
//             <View style={styles.userStyle}>
//                <Image source={userImage} style={styles.userIcon}></Image>
//                <Text style={styles.userNickname}>{userNickname}</Text>
//             </View>
//            </TouchableHighlight>
//             {/* <DrawerItems {...props} /> */}
//             <TabView navigation={props.navigation}></TabView>
//           </View>
//         </ScrollView>
//       )
//     },
// });
var receiveRemoteNotificationSub;
class Base extends React.Component {
  componentDidMount() {
    this.bindNotification();
    Getui.resetBadge();
  }

  componentWillUnmount() {
    this.unBindNotification();
  }

 bindNotification = () => {
   if(receiveRemoteNotificationSub) {
     return;
   }
   receiveRemoteNotificationSub = NativeAppEventEmitter.addListener(
     'receiveRemoteNotification',
      (notification) => {
        //消息类型分为 cmd 和 payload 透传消息，具体的消息体格式会有差异
        switch (notification.type) {
            case "cid":
                sFetch(CONST.POST_NOTIFICATION_CLINET_ID, getPostParams({"client_id" : notification.cid}), undefined, undefined, false);
                break;
            case "cmd":
                //
                break;
            case "payload":
                this.props.dispatch(getUserMsg(CONST.GET_USER_MSG, false, false));
                break;
            default:
        }
      }
    );
 };

 unBindNotification = () => {
   if(receiveRemoteNotificationSub) {
     receiveRemoteNotificationSub.remove();
     receiveRemoteNotificationSub = null;
   }
 };

  componentWillReceiveProps(newProps) {
    if(this.props.isLoggedIn != newProps.isLoggedIn) {
      if(newProps.isLoggedIn) {
        this.bindNotification();
      } else {
        this.unBindNotification();
      }
    }
  }

  render() {
    componentProps = this.props;
    // return <FvDrawerView />;
    return <View></View>;
  }
}

// const DrawerOptions = ({navigation},titleValue) =>{
//
//   var drawerLabel = titleValue;
//   var drawerIcon = (option) => (
//     <Image
//        source={require('../img/ic_list_profile_04.png')}
//        style={{width:24, height:24}}
//     />
//  );
//
//   return { drawerLabel,drawerIcon };
// };

const styles = StyleSheet.create({
  container : {
    backgroundColor : '#fff',
    width : 236,
    height : height,
  },
  userStyle : {
    backgroundColor : '#fff',
    flexDirection : 'column',
    justifyContent : 'center',
    alignItems : 'center',
    paddingTop : Platform.OS === 'ios' ? 20 : StatusBar.currentHeight,
    // paddingBottom : 30,
    width : 236,
    height : 156,
  },
  userIcon : {
    marginLeft : 20,
    marginRight : 12,
    width : 60,
    height : 60,
    borderRadius:30,
  },
  userNickname : {
    fontSize : 18,
    color : '#686868',
    marginTop : 10,
  }
});

function select(store){
  return{
    isLoggedIn: store.userInfo.isLoggedIn,
    userInfo: store.userInfo.userInfo,
  }
}

export default connect(select)(Base);
//export default FvDrawerView;
