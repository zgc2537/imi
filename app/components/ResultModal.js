/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */

// this.props = { isResultOk, okText, ngText }

import React from 'react';
import { StyleSheet, View, Image, Text, Modal } from 'react-native';
import * as CONST from '../utils/constants';
import NewButton from './NewButton';
import ToastUtil from '../utils/ToastUtil';

class ResultModal extends React.Component {
  constructor(props){
    super(props);

    this.onModalShow = this.onModalShow.bind(this);
    this.onModalRequestClose = this.onModalRequestClose.bind(this);
    this._onPressOkBtn = this._onPressOkBtn.bind(this);
    this._onPressNgBtn = this._onPressNgBtn.bind(this);
  }

  onModalShow = () =>{
    const { handleModalShow } = this.props;
    handleModalShow && handleModalShow();
  };

  onModalRequestClose = () =>{
    const { handleModalRequestClose } = this.props;
    handleModalRequestClose && handleModalRequestClose();
  }

  _onPressOkBtn = () =>{
    const { handleOkBtn } = this.props;
    handleOkBtn && handleOkBtn();
  };

  _onPressNgBtn = () =>{
    const { handleNgBtn } = this.props;
    handleNgBtn && handleNgBtn();
  };

  render() {
    const { isResultOk, okText, ngText } = this.props;
    let tipImage;
    let tip1Text;
    let footBtn;

    if(isResultOk){
      tip1Text = <Text style={styles.tip1Text}>{okText}</Text>;
      tipImage = <Image source={require('../img/ic_pop_ok.png')} style={styles.tipImage} />
      footBtn  = (
        <NewButton
          btnType={CONST.BUTTON_STYLE.TYPE2}
          style={styles.footBtn}
          title="确定"
          onPress={this._onPressOkBtn}
        />
      );
    }else{
      tip1Text = <Text style={styles.tip1Text}>{ngText}</Text>;
      tipImage = <Image source={require('../img/ic_pop_lose.png')} style={styles.tipImage} />
      footBtn  = (
        <NewButton
          btnType={CONST.BUTTON_STYLE.TYPE2}
          style={styles.footBtn}
          title="确定"
          onPress={this._onPressNgBtn}
        />
      );
    }

    return (
      <Modal
        {...this.props}
        animationType='fade'
        transparent={true}
        visible={this.props.visible}
        onShow={this.onModalShow}
        onRequestClose={this.onModalRequestClose}
      >
        <View style={styles.container}>
        <View style={styles.content}>
            <View style={styles.header}>
              <Text style={styles.headerText}>提示</Text>
            </View>
            <View style={styles.body}>
              {tipImage}
              {tip1Text}
            </View>
            <View style={styles.footer}>
              {footBtn}
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    position:'absolute',
    top:0,
    left:0,
    right:0,
    bottom:0,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    justifyContent: 'center',
  },
  content:{
    marginLeft:30,
    marginRight:30,
    backgroundColor: '#fff',
    borderRadius:5,
  },
  header:{
    padding:10,
    borderBottomWidth:1,
    borderColor:'#ddd',
    alignItems:'center',
  },
  headerText:{
    fontSize:20,
    color:'#999',
  },
  body:{
    padding:20,
    alignItems:'center',
  },
  tipImage:{
    width: 80,
    height: 80,
  },
  tip1Text:{
    marginTop:15,
    fontSize:20,
    color:'#252525',
  },
  tip2Text:{
    marginTop:10,
    fontSize:20,
    color:'#ff0000',
  },
  footer:{
    paddingTop:10,
    paddingBottom:20,
    paddingLeft:30,
    paddingRight:30,
  },
});

export default ResultModal;
