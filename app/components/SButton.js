'use strict'

import React, {Component} from 'react';

import {
  TouchableOpacity,
  Text,
  StyleSheet
} from 'react-native';

export default class SButton extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity style={[this.props.disabled? styles.customStyleDisabled : styles.customStyle, this.props.style]} onPress={()=>{this.props.onPress()}} disabled={this.props.disabled}>
        <Text style={[this.props.disabled? styles.btnTextDisabled : styles.btnText, this.props.textStyles]}>{this.props.children}</Text>
      </TouchableOpacity>
    );
  }
}

var styles = StyleSheet.create({
  customStyle : {
    width : 270,
    height : 50,
    backgroundColor : '#0091D8',
    flexDirection : 'row',
    justifyContent : 'center',
    alignItems : 'center',
    borderRadius : 4
  },
  customStyleDisabled : {
    width : 270,
    height : 50,
    backgroundColor : '#515a6b',
    flexDirection : 'row',
    justifyContent : 'center',
    alignItems : 'center',
    borderRadius : 6
  },
  btnText : {
    fontSize : 16,
    color : '#ffffff'
  },
  btnTextDisabled : {
    fontSize : 16,
    color : '#7b89a3'
  }
});
