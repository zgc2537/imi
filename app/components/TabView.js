import React from 'react';
import {PropTypes} from "react";
import {
  Image,
  StyleSheet,
  ScrollView,
  Text,
  View,
  TouchableOpacity,
  Dimensions
} from "react-native";
//import Button from 'react-native-button';
import * as CONST from "../utils/constants";
import { connect } from 'react-redux';
import ToastUtil from '../utils/ToastUtil';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container : {
    backgroundColor : '#fff',
    //backgroundColor : rgba(),
    width : 236,
    height : height - 156,
    justifyContent : 'space-between',
  },
  userStyle : {
    backgroundColor : '#fff',
    flexDirection : 'row',
    justifyContent : 'center',
    alignItems : 'center',
    width : 236,
    height : 126
  },
  userIcon : {
    marginLeft : 20,
    marginRight : 12,
    width : 60,
    height : 60
  },
  userNickname : {
    fontSize : 20,
    color : '#fff'
  },
  listContainer :{
    height : 45,
    width : 236,
    // borderColor : '#dadada',
    // borderBottomWidth : 1,
    flexDirection : 'row',
    alignItems : 'center'
  },
  listIcon : {
    marginLeft : 20,
    marginRight : 12,
    width : 25,
    height : 25
  },
  listText : {
    fontSize : 15,
    color : '#686868'
  },
  namePlateRegion : {
    flexDirection : 'row',
    justifyContent : 'center',
    alignItems : 'center',
    marginBottom : 20,
  },
  logStyle : {
    fontSize : 12,
    color : '#999999'
  }
});

//const TabView = (props, context) => {
class TabView extends React.Component {

    _gotoChildPage = (pageStack) => {
      if(this.props.isLoggedIn){
        this.props.navigation.navigate(pageStack);
      }else{
        ToastUtil.showShort(CONST.HAS_NO_LOGGIN_ERRSTR);
        this.props.navigation.navigate(CONST.NAV_LOGIN);
      }
    };

    //const drawer = context.drawer;
    render() {
    const navigation = this.props.navigation;
    return (
      <ScrollView>
      <View style={styles.container}>
        <View style={styles.funcRegion}>
          <TouchableOpacity onPress={()=>{ this._gotoChildPage(CONST.NAV_MY_CAR_NUM);}}>
          <View style={styles.listContainer}>
            <Image source={require('../img/ic_list_profile_01.png')} style={styles.listIcon}></Image>
            <Text style={styles.listText}>我的车牌</Text>
          </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{ this._gotoChildPage(CONST.NAV_MY_WALLET);}}>
          <View style={styles.listContainer}>
            <Image source={require('../img/ic_list_profile_02.png')} style={styles.listIcon}></Image>
            <Text style={styles.listText}>我的钱包</Text>
          </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{ this._gotoChildPage(CONST.NAV_PAID_HISTORY);}}>
          <View style={styles.listContainer}>
            <Image source={require('../img/ic_list_profile_03.png')} style={styles.listIcon}></Image>
            <Text style={styles.listText}>缴费记录</Text>
          </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{ this._gotoChildPage(CONST.NAV_MY_UNPAID);}}>
          <View style={styles.listContainer}>
            <Image source={require('../img/ic_list_profile_014.png')} style={styles.listIcon}></Image>
            <Text style={styles.listText}>欠费补缴</Text>
          </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{ this._gotoChildPage(CONST.NAV_CUSTOMER_SERVICE);}}>
          <View style={styles.listContainer}>
            <Image source={require('../img/ic_list_profile_015.png')} style={styles.listIcon}></Image>
            <Text style={styles.listText}>客服中心</Text>
          </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{ this._gotoChildPage(CONST.NAV_SET_OTHER);}}>
          {/* <TouchableOpacity onPress={()=>{ this.props.navigation.navigate(CONST.NAV_FV_ME);}}> */}
          <View style={styles.listContainer}>
            <Image source={require('../img/ic_list_profile_06.png')} style={styles.listIcon}></Image>
            <Text style={styles.listText}>设置</Text>
          </View>
          </TouchableOpacity>
        </View>
        <View style={styles.namePlateRegion}>
          <Text style={styles.logStyle}>无锡市智慧停车管理有限公司</Text>
        </View>
      </View>
      </ScrollView>
    );
      }
};

//export default TabView;

function select(store){
  return{
    isLoggedIn: store.userInfo.isLoggedIn,
    userInfo: store.userInfo.userInfo,
  }
}

export default connect(select)(TabView);
