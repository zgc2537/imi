import React from 'react';
import {
  ActivityIndicator,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

export default class LoadMoreFooter extends React.Component {
  static propTypes = {
    isErr: React.PropTypes.bool,
    isNoMore: React.PropTypes.bool,
    largeHeight: React.PropTypes.bool,
    onReload: React.PropTypes.func,
  }

  static defaultProps = {
    isErr: false,
    isNoMore: false,
    largeHeight: true,
    onReload : null,
  }

  render() {
    const { isErr, isNoMore, largeHeight, onReload } = this.props;
    const caption = isErr ? '-- 加载失败，点击此次重新加载 --' : (isNoMore ? '-- 没有更多数据了 --' : '努力加载数据中...');

    return (
      <View style={largeHeight?[styles.loadingbox,styles.largeHeight]:[styles.loadingbox]}>
        <TouchableOpacity style={styles.loadingItem} onPress={ ()=>{ isErr && onReload && onReload() } } activeOpacity={0}>
          { !isNoMore && !isErr && <ActivityIndicator />}
          <Text style={styles.caption}>{caption}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  loadingbox: {
    height: 40,
  },
  largeHeight:{
    height: 200,
  },
  loadingItem:{
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  caption: {
    fontSize: 14,
    marginLeft: 5,
    color: 'gray'
  }
})
