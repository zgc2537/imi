/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */

'use strict';

import { Alert, InteractionManager, } from 'react-native';

import * as TYPES from './types';
import * as CONST from '../utils/constants';
import * as TESTDATA from '../utils/testdata';
import {
  getGetParams,
  getPostParams,
  getPatchParams,
  sFetch,
  stringReplace,
  checkWxResponse,
  getErrStr,
  dateFormat,
  judgeHasUnread
} from '../utils/utils';
import ToastUtil from '../utils/ToastUtil';

// login
let logIn = (opt) => {
  return (dispatch) => {

    dispatch({'type': TYPES.LOGGED_DOING});

    // request(CONST.REQ_URL_CHECK_VERIFY_CODE,'POST',JSON.stringify({phone_number:opt.phoneNum,verify_code:opt.smsCode}),opt.modal).then((res)=>{
    sFetch(CONST.REQ_URL_LOGIN, getPostParams({username: opt.username, password: opt.password}),(res)=>{
      dispatch({'type': TYPES.LOGGED_IN, 'user': res.user});
    },(e)=>{
      dispatch({'type': TYPES.LOGGED_ERROR, 'fetchErr': e});
    });
  };
};

// skip login
let skipLogin = () => {
  return {
    'type': TYPES.LOGGED_IN,
    'userInfo': TESTDATA.skipUser,
  }
};

// logout
let logOut = () => {
  let reqUrl = CONST.REQ_URL_LOGOUT;
  return (dispatch) => {
    dispatch({'type': TYPES.LOGGED_OUT_DOING});
    // request(reqUrl,'GET',null,opt.modal).then((d) => {
    sFetch(reqUrl,getGetParams(),(d) => {
      dispatch({'type': TYPES.LOGGED_OUT});
    },(e) => {
      dispatch({'type': TYPES.LOGGED_OUT});
    }, false);
  }
};

let syncUserInfo = (opt) => {
  return (dispatch) => {
    dispatch({'type': TYPES.SYNC_USERINFO_DOING});

    sFetch(stringReplace(CONST.REQ_URL_GET_CUSTOMER_PROFILE_DETAIL, opt.id),getGetParams(),(d)=>{
      let userInfo = {};
      if(d){
          d.wallet && typeof(d.wallet.balance)!='undefined' && (userInfo.balance = d.wallet.balance);
          d.wallet_id && typeof(d.wallet.id)!='undefined' && (userInfo.wallet_id = d.wallet.id);
          d.wxuser && typeof(d.wxuser.headimgurl)!='undefined' && (userInfo.headimgurl = d.wxuser.headimgurl);
          d.wxuser && typeof(d.wxuser.nickname)!='undefined' && (userInfo.nickname = d.wxuser.nickname);
          d.current_car && typeof(d.current_car.plate_no)!='undefined' && (userInfo.plate_no = d.current_car.plate_no);
          d.phone && (userInfo.phone = d.phone);
          d.birthday && (userInfo.birthday = d.birthday);
          d.business && (userInfo.business = d.business);
          d.company && (userInfo.company = d.company);
          userInfo.gender = d.gender;
          d.job && (userInfo.job = d.job);
      }
      if(userInfo.headimgurl && userInfo.headimgurl.length > 0 && userInfo.headimgurl[0] == '/'){
        userInfo.headimgurl = CONST.HOST_NAME + userInfo.headimgurl;
      }
      dispatch({'type':TYPES.SYNC_USERINFO_DONE, userInfo:userInfo});
      // dispatch({'type':TYPES.SYNC_USERINFO_NULL});
    },(e)=>{
      dispatch({'type':TYPES.SYNC_USERINFO_ERROR, fetchErr:e});
    });

  }
}

export {
  logIn,
  skipLogin,
  logOut,
  syncUserInfo
};
