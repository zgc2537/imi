/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */

//地图某坐标点附近的停车场列表
export const FETCH_MARKER_LIST = "FETCH_MARKER_LIST";
export const RECEIVE_MARKER_LIST = "RECEIVE_MARKER_LIST";
export const CHANGE_CHARGE_STATE = "CHANGE_CHARGE_STATE";

//用户登录信息
export const LOGGED_IN  = 'LOGGED_IN';
export const LOGGED_OUT = 'LOGGED_OUT';
export const LOGGED_ERROR = 'LOGGED_ERROR';
export const LOGGED_DOING = 'LOGGED_DOING';
export const LOGGED_OUT_DOING = 'LOGGED_OUT_DOING';
export const SYNC_USERINFO_DOING = 'SYNC_USERINFO_DOING';
export const SYNC_USERINFO_DONE = 'SYNC_USERINFO_DONE';
export const SYNC_USERINFO_ERROR = 'SYNC_USERINFO_ERROR';
