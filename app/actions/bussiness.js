/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */
'use strict';

import { Alert } from 'react-native';
import * as TYPES from './types';
import * as CONST from '../utils/constants';
import ToastUtil from '../utils/ToastUtil';
import {
  getGetParams,
  getPostParams,
  getPatchParams,
  sFetch,
  stringReplace
} from '../utils/utils';


let reqRecharge = (
  isRefreshing,
  isLoading,
  offset,
  limit,  //如果仅仅设置 offset=0&limit=0 将一次性返回所有请求数据.
) => {

  let URL = CONST.REQ_URL_GET_RECHARGE + `?offset=${offset}&limit=${limit}&order_by=base_amount&valid=1`;

  return dispatch => {
    dispatch(fetchRecharge());

    sFetch(URL,getGetParams(),(json) => {
      if( json && json.meta && json.meta.total_count >= 0 && json.objects){
        //dispatch(receiveRecharge(d.objects));
        let rechargeList = [];
        for(var i = 0; i< json.objects.length; i++){
          rechargeList[i] = {
            id : i,
            base_amount : parseFloat(json.objects[i].base_amount),
            reward_amount : parseFloat(json.objects[i].reward_amount),
          }
        }
        dispatch(receiveRecharge(rechargeList));
      }else{
        dispatch(receiveRecharge([]));
      }
    },(e) => {
      dispatch(receiveRecharge([]));
    });
  }
}

let fetchRecharge = ()=> {
  return {
    type: TYPES.FETCH_RECHARGE_LIST,
  }
}

let receiveRecharge = (list)=> {
  return {
    type: TYPES.RECEIVE_RECHARGE_LIST,
    list: list
  }
}

let resetRecharge = () => {
  return {
    type: TYPES.RESET_RECHARGE_LIST
  }
}

export {
  reqRecharge,
  fetchRecharge,
  receiveRecharge,
  resetRecharge,
};
