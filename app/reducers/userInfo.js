/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */

//用户信息包括昵称(nickname)，头像(avatar)，余额(balance)，绑定车牌号(bindedCarno)，手机号(phoneNo)

import * as TYPES from '../actions/types';
import * as CONSTS from '../utils/constants';
const initialState = {
  isLoggedIn : false,
  user : {},
  loginStatus : CONSTS.NETWORK_REQUEST_STATE.NULL,
  loginError : null,
  syncUserInfoStatus: CONSTS.NETWORK_REQUEST_STATE.NULL,
  syncUserInfoError: null,
  logoutStatus : CONSTS.NETWORK_REQUEST_STATE.NULL
};

export default function userInfo(state=initialState, action){
  switch(action.type){
    case TYPES.LOGGED_DOING:
      return {
        ...state,
        loginStatus: CONSTS.NETWORK_REQUEST_STATE.DOING
      };
    case TYPES.LOGGED_IN:
      return {
        ...state,
        isLoggedIn: true,
        user: action.user,
        loginStatus: CONSTS.NETWORK_REQUEST_STATE.DONE
      };
    case TYPES.LOGGED_ERROR:
      return {
        ...state,
        isLoggedIn: false,
        user: {},
        loginStatus: CONSTS.NETWORK_REQUEST_STATE.ERROR,
        loginError: action.fetchErr ? action.fetchErr : null
      };
    case TYPES.LOGGED_OUT_DOING:
      return {
        ...state,
        logoutStatus: CONSTS.NETWORK_REQUEST_STATE.DOING
      };
    case TYPES.LOGGED_OUT:
      return initialState;
    case TYPES.SYNC_USERINFO_DOING:
      return {
        ...state,
        syncUserInfoStatus: CONSTS.NETWORK_REQUEST_STATE.DOING,
      };
    case TYPES.SYNC_USERINFO_DONE:
      return {
        ...state,
        user:{...state.user, ...action.user},
        syncUserInfoStatus: CONSTS.NETWORK_REQUEST_STATE.DONE,
      };
    case TYPES.SYNC_USERINFO_ERROR:
      return {
        ...state,
        syncUserInfoStatus: CONSTS.NETWORK_REQUEST_STATE.ERROR,
        syncUserInfoError: action.fetchErr ? action.fetchErr : null,
      };
    default:
      return state;
  }
}
