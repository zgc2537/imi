/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */

import { combineReducers } from 'redux';

import bussinessReducer    from './bussiness';
import userInfoReducer     from './userInfo';

const rootReducer = combineReducers({
    bussiness: bussinessReducer,
    userInfo: userInfoReducer
});

export default rootReducer;
