'use strict'

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  FlatList,
  TextInput,
  ScrollView
} from 'react-native';
import SLoading from '../components/SLoading';
import SelectModal from '../components/SelectModal';
import { connect } from 'react-redux'
import * as CONST from '../utils/constants';
import {
  getGetParams,
  getPostParams,
  sFetch,
  getErrStr,
  onlyAlert
} from '../utils/utils';

const {height, width} = Dimensions.get('window');

class ValueInput extends Component {

  static navigationOptions = {
    title: 'Select Options',
  };

  state = {
    loadingVisible: false,
    modalVisible: false,
    filters: [],
    country: '',
    plantName: '',
    location: '',
    maker: '',
    model: '',
    problem: '',
    selectRow: 0,
  }

  componentDidMount() {
    this.setState({
      filters: CONST.oppCreatType(),
      from: CONST.DATA_BASE['plant_input']
    })
  }

  _getFilterOptions = (params) => {
    return new Promise((resolve, reject) => {
      sFetch(CONST.REQ_URL_GET_DATA, getPostParams(params),(res)=>{
        resolve(res)
      },(e)=>{
        reject(e)
      });
    })
  }

  _createOpp = (params) => {
    return new Promise((resolve, reject) => {
      sFetch(CONST.REQ_URL_OPPORTUNITY_CREATE, getPostParams(params),(res)=>{
        resolve(res)
      },(e)=>{
        reject(e)
      });
    })
  }

  _jump = () => {
    const {
      navigation: {
        goBack
      }
    } = this.props

    const {
      filters,
      country,
      plantName,
      location,
      maker,
      model,
      problem
    } = this.state

    let err = false

    for (let index = 0; index < filters.length; index++) {
      const element = filters[index];
      if(!element.selectValue) {
        err = true
        onlyAlert(CONST.ALERT_TITLE_TIP, `Please Select ${element.name}`)
        break
      }
    }

    if(err) {
      return
    }

    if(!country) {
      onlyAlert(CONST.ALERT_TITLE_TIP, `Please Enter Country`)
      return
    }

    if(!plantName) {
      onlyAlert(CONST.ALERT_TITLE_TIP, `Please Enter Plant Name`)
      return
    }

    if(!location) {
      onlyAlert(CONST.ALERT_TITLE_TIP, `Please Enter Location`)
      return
    }

    if(!maker) {
      onlyAlert(CONST.ALERT_TITLE_TIP, `Please Enter Maker`)
      return
    }

    if(!model) {
      onlyAlert(CONST.ALERT_TITLE_TIP, `Please Enter Model`)
      return
    }

    if(!problem) {
      onlyAlert(CONST.ALERT_TITLE_TIP, `Please Enter Problem`)
      return
    }

    this.setState({loadingVisible: true})
    this._createOpp({
      industry: filters[0].selectValue,
      plant_type: filters[1].selectValue,
      application: filters[2].selectValue,
      product_type: filters[3].selectValue,
      country: country,
      location: location,
      maker: maker,
      model: model,
      plant_name: plantName,
      typical_problem: problem,
    }).then(res => {
      this.setState({loadingVisible: false})
      onlyAlert(CONST.ALERT_TITLE_TIP, `Create Success!`)
      goBack()
    }, err => {
      this.setState({loadingVisible: false})
      onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`)
    })

  }

  _openModal = (selectItem, selectRow) => {
    const { filters, from } = this.state

    if(
      (!selectItem.options.length && filters[selectRow - 1] && filters[selectRow - 1].selectValue) ||
      (!selectItem.options.length && selectRow == 0)
    ) {
      const filter_list = {}
      filters.filter(item => item.selectValue && item.key != 'product_type').map(item => {
        filter_list[item.key] = item.selectValue
      })

      // // filter_list级联
      // console.log({
      //   cls: from,
      //   param: selectItem.key,
      //   filter_list
      // })

      this.setState({loadingVisible: true})

      this._getFilterOptions({
        cls: from,
        param: selectItem.key,
        filter_list
      }).then(res => {
        // 此处给options赋值
        filters[selectRow].options = res.data
        this.setState({
          loadingVisible: false,
          filters: filters,
          selectRow: selectRow,
          modalVisible: true,
        })
      }, err => {
        this.setState({loadingVisible: false})
        onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
      })

    } else {

      this.setState({
        selectRow: selectRow,
        modalVisible: true,
      })

    }


  }

  _onSelected = (item) => {
    const { selectRow, filters } = this.state

    filters[selectRow].selectValue = item

    const tmp = filters.map((item, row) => {
      if(row > selectRow && item.key != 'product_type') {
        item.options = []
        item.selectValue = ''
      }
      return item
    })

    this.setState({
      filters: [...tmp],
      modalVisible: false
    })
  }

  _keyExtractor = (item, index) => index

  _renderItem = ({item, index}) => {
    return (
      <View style={[styles.filterContainer, (index == this.state.filters.length - 1) ? {borderBottomWidth: 0} : {}]}>
        <Text style={styles.filterTitle}>{item.name}</Text>
        <TouchableOpacity
          onPress={()=> {this._openModal(item, index)}}
          >
          <View style={styles.itemSelect}>
            <Text style={{maxWidth: width / 2, color: item.selectValue ? '#252525' : '#8B999F'}} numberOfLines={1}>{item.selectValue ? item.selectValue : `Select ${item.name}`}</Text>
            <Image style={{width: 25, height: 25}} source={require('../img/ic_list_nextpage.png')} />
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  render() {
    const { filters, selectRow } = this.state
    return (
      <ScrollView style={styles.container}>
        <FlatList
          data={this.state.filters}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          style={styles.filterList}
          // ItemSeparatorComponent={() => ( <View style={{height: 10}} /> )}
        />

        <View style={styles.inputContainer}>
          <Text style={styles.libTitle}>Add More</Text>
          <TextInput style={styles.input}
              underlineColorAndroid='transparent'
              editable={true}
              multiline={true}
              placeholder="Enter Country"
              placeholderTextColor="#8B999F"
              onChangeText={text => this.setState({country: text})} />
          <TextInput style={styles.input}
              underlineColorAndroid='transparent'
              editable={true}
              multiline={true}
              placeholder="Enter Plant Name"
              placeholderTextColor="#8B999F"
              onChangeText={text => this.setState({plantName: text})} />
          <TextInput style={styles.input}
              underlineColorAndroid='transparent'
              editable={true}
              multiline={true}
              placeholder="Enter Location"
              placeholderTextColor="#8B999F"
              onChangeText={text => this.setState({location: text})} />
          <TextInput style={styles.input}
              underlineColorAndroid='transparent'
              editable={true}
              multiline={true}
              placeholder="Enter Maker"
              placeholderTextColor="#8B999F"
              onChangeText={text => this.setState({maker: text})} />
          <TextInput style={styles.input}
              underlineColorAndroid='transparent'
              editable={true}
              multiline={true}
              placeholder="Enter Model"
              placeholderTextColor="#8B999F"
              onChangeText={text => this.setState({model: text})} />
          <TextInput style={[styles.input, {marginBottom: 100, height: 80, textAlignVertical: 'top'}]}
              underlineColorAndroid='transparent'
              editable={true}
              multiline={true}
              placeholder="Enter Problem Description"
              placeholderTextColor="#8B999F"
              onChangeText={text => this.setState({problem: text})} />
        </View>


        <View style={styles.btnGroup}>
          <TouchableOpacity style={styles.btn} onPress={this._jump}>
            <Text style={styles.btnText}>Submit</Text>
          </TouchableOpacity>
        </View>

        <SelectModal
          visible={this.state.modalVisible}
          onClose={()=> {this.setState({modalVisible: false})}}
          onChange={this._onSelected}
          list={filters.length ? filters[selectRow].options : []}
          placeholder={filters.length ? filters[selectRow].name : ''}
        />

        <SLoading visible={this.state.loadingVisible} modalStyles={{backgroundColor: 'transparent'}}/>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f2f5f6',
  },

  btnGroup: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: width,
    height: 50,
    flexDirection: 'row',
  },

  btn: {
    flex: 1,
    backgroundColor: '#0092DB',
    justifyContent: 'center',
    alignItems: 'center',
  },

  btnStyle: {
    backgroundColor: '#1FA3E6'
  },

  btnText: {
    fontSize: 16,
    color: '#fff'
  },

  filterList: {
    backgroundColor: '#fff',
    paddingTop: 10,
    paddingBottom: 5
  },

  filterTitle: {
    fontSize: 16,
    color: '#000',
    paddingVertical: 10,
  },

  libTitle: {
    paddingLeft: 30,
    fontSize: 16,
    color: '#496571',
    paddingVertical: 10,
  },

  itemSelect: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10
  },

  inputContainer: {
    backgroundColor: '#fff',
    marginTop: 6
  },

  input: {
    marginHorizontal: 30,
    paddingHorizontal: 15,
    paddingVertical: 7,
    borderColor: '#ddd',
    borderWidth: 1,
    marginBottom: 10
  },
  filterContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderColor: '#ddd',
    borderBottomWidth: 1,
    marginHorizontal: 30,
  }
});

function select(store) {
  return new Object();
}

export default connect(select)(ValueInput);
