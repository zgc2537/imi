'use strict'

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
} from 'react-native';

import { connect } from 'react-redux'
import SearchBar from '../components/SearchBar';
import * as CONST from '../utils/constants';
const {height, width} = Dimensions.get('window');

class ValveSearch extends Component {

  static navigationOptions = {
    title: 'Valve Search',
  };

  state = {
    value: {}
  }

  componentDidMount() {
    const {
      navigation: {
        state: { params }
      }
    } = this.props
    params && this.setState({ value: params.value })
  }

  render() {
    const { value } = this.state
    return (
      <View style={styles.container}>
        <View style={styles.listItemContainer}>
          <View style={styles.rowText}>
            <Text style={styles.title}>Manufacturer:</Text>
            <Text style={styles.value}>{value.maker}</Text>
          </View>
          <View style={styles.rowText}>
            <Text style={styles.title}>Product Type:</Text>
            <Text style={styles.value}>{value.product_type}</Text>
          </View>
          <View style={styles.rowText}>
            <Text style={styles.title}>Model:</Text>
            <Text style={styles.value}>{value.model}</Text>
          </View>
          <View style={styles.rowText}>
            <Text style={styles.title}>Industry:</Text>
            <Text style={styles.value}>{value.industry}</Text>
          </View>
          <View style={styles.rowText}>
            <Text style={styles.title}>Plant Type:</Text>
            <Text style={styles.value}>{value.plant_type}</Text>
          </View>
          <View style={styles.rowText}>
            <Text style={styles.title}>Application:</Text>
            <Text style={styles.value}>{value.application}</Text>
          </View>
          <View style={[styles.rowText, styles.rowTextMulti]}>
            <Text style={styles.title}>Typical Problem Example:</Text>
            <Text style={[styles.value, styles.rowTextMultiValue]}>{value.typical_problem}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f5f6',
  },
  listItemContainer: {
    width: width,
    paddingTop: 9,
    paddingBottom: 9,
    paddingLeft: 20,
    paddingRight: 15,
    backgroundColor: '#fff'
  },
  title: {
    fontSize: 16,
    color: '#496571'
  },
  value: {
    fontSize: 14,
    color: '#080808'
  },
  rowText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 7,
    paddingBottom: 7
  },
  rowTextMulti: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  rowTextMultiValue: {
    marginTop: 6,
    paddingLeft: 10
  }
});

function select(store) {
  return new Object();
}

export default connect(select)(ValveSearch);
