'use strict'

import React, {Component} from 'react';
import {
  Platform,
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  Dimensions,
  TouchableHighlight,
  TouchableOpacity,
  FlatList
} from 'react-native';

import { connect } from 'react-redux'
import SearchBar from '../components/SearchBar';
import * as CONST from '../utils/constants';
import {
  getGetParams,
  getPostParams,
  getPatchParams,
  sFetch,
  stringReplace,
  getErrStr,
  onlyAlert,
  encode
} from '../utils/utils';

const {height, width} = Dimensions.get('window');

class MyUpgrade extends Component {

  static navigationOptions = {
    title: 'My Upgrade List & Status',
  };

  state = {
    refreshing: false,
    next: null,
    list: []
  }

  componentDidMount() {
    this._onRefresh()
  }

  _fetch = (url) => {
    return new Promise((resolve, reject) => {
      sFetch(url, getGetParams(), (res)=>{
        resolve(res)
      },(e)=>{
        reject(e)
      });
    })

  }

  _getOpportunity = (url) => {
    const { list } = this.state
    this._fetch(url).then(res => {
      this.setState({
        refreshing: false,
        next: res.meta.next ? CONST.HOST_NAME + res.meta.next : null,
        list: [...list, ...res.objects]
      })
    }, err => {
      this.setState({refreshing: false})
      onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
    })
  }

  _onRefresh = (url = CONST.REQ_URL_OPPORTUNITY + "&contact_email=" + this.props.user.email) => {
    this.setState({refreshing: true, list: []}, ()=> {
      this._getOpportunity(url)
    })
  }

  _onEndReached = () => {
    const { next } = this.state
    next && this._getOpportunity(next)
  }

  _onFilter = () => {
    this.props.navigation.navigate(CONST.NAV_FILTER, {
      filter: 'opportunity',
      onChangeFilter: this._onChangeFilter
    });
  }

  _onSearch = (text) => {
    if(text) {
      this._onRefresh(`${CONST.REQ_URL_OPPORTUNITY}&contact_email=${this.props.user.email}&plant_name__icontains=${encode(text)}`)
    }else {
      this._onRefresh()
    }
  }

  _onChangeFilter = (filters) => {
    // 更新
    this._onRefresh(`${CONST.REQ_URL_OPPORTUNITY}&contact_email=${this.props.user.email}&${filters}`)
  }

  _keyExtractor = (item, index) => item.id

  _itemClick = (item) => {
    // console.log(item);
    this.props.navigation.navigate(CONST.NAV_OPPORTUNITY_DETAIL_PAGE, {
      from: 'My Upgrade List & Status',
      opportunity: item
    });
  }



  _renderItem = ({ item }) => {
    return (
      <TouchableHighlight
        underlayColor="rgba(229, 241, 255, 0.9)"
        style={{backgroundColor: 'rgba(255, 255, 255, 0.6)'}}
        onPress={()=>{
          this._itemClick(item)
        }}>
        <View style={styles.listItemContainer}>
          <View style={styles.listItem}>
            <View style={[styles.listItemRow, {marginTop: 0}]}>
              <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">{item.plant_name}</Text>
              {
                item.upgrade_type ? (
                  <Text style={[styles.type, item.upgrade_type == 'UGC' ? {} : styles.typeErr]}>{item.upgrade_type}</Text>
                ) : null
              }
            </View>
            <View style={styles.listItemRow}>
              <Image style={styles.icon} source={require('../img/ic_upgrade_application.png')} />
              <Text style={styles.rowText} numberOfLines={1}>{item.application}</Text>
            </View>
            <View style={styles.listItemRow}>
              <Image style={styles.icon} source={require('../img/ic_upgrade_maker.png')} />
              <Text style={styles.rowText} numberOfLines={1}>{item.model ? item.model : '-'}</Text>
            </View>
            <View style={styles.listItemRow}>
              <Image style={styles.icon} source={require('../img/ic_upgrade_status.png')} />
              <Text style={styles.rowText} numberOfLines={1}>{item.upgrade_status}</Text>
            </View>
          </View>
          <View>
            <Image style={styles.icon} source={require('../img/ic_list_nextpage.png')} />
          </View>
        </View>
      </TouchableHighlight>

    )
  }

  render() {
    return (
      <View style={styles.container}>
        <SearchBar
          placeholder={"Plant Name"}
          hasFilter={true}
          hasClear={false}
          onSearch={this._onSearch}
          onFilter={this._onFilter}
        />
        <View style={styles.listContainer}>
          <View style={styles.bgContainer}>
            <Image style={styles.bg} source={require('../img/bg_app.png')} />
          </View>
          <FlatList
            onRefresh={this._onRefresh}
            onEndReached={this._onEndReached}
            refreshing={this.state.refreshing}
            style={styles.listStyle}
            data={this.state.list}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            ItemSeparatorComponent={() => ( <View style={{height: 10}} /> )}
          />
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f7f9fa',
  },

  listContainer: {
    flex: 1
  },

  bgContainer: {
    flex: 1,
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: '#f7f9fa'
  },

  bg: {
    width: width,
    height: height - 72 - 60,
  },

  listStyle: {
    flex: 1,
  },

  listItemContainer: {
    width: width,
    height: 123,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 30,
  },

  listItem: {
    flexDirection: 'column',
    justifyContent: 'space-around'
  },

  listItemRow: {
    flexDirection: 'row',
    marginTop: 5,
    alignItems: 'center'
  },

  icon: {
    width: 22,
    height: 22
  },

  title: {
    width: width * 2 / 3,
    fontSize: 16,
    color: '#080808'
  },

  type: {
    fontSize: 14,
    backgroundColor: '#F5A623',
    color: '#fff',
    paddingHorizontal: 10,
    borderRadius: 15,
    marginLeft: 10
  },

  typeErr: {
    backgroundColor: '#099447'
  },

  rowText: {
    fontSize: 14,
    color: '#4a4a4a',
    marginLeft: 15,
  }
});

function select(store) {
  return {
      user: store.userInfo.user
  };
}

export default connect(select)(MyUpgrade);
