'use strict'

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableHighlight,
  TouchableOpacity,
  FlatList
} from 'react-native';

import { connect } from 'react-redux'
import SearchBar from '../components/SearchBar';
import * as CONST from '../utils/constants';
import {
  getGetParams,
  getPostParams,
  getPatchParams,
  sFetch,
  stringReplace,
  getErrStr,
  onlyAlert
} from '../utils/utils';

const {height, width} = Dimensions.get('window');
var FilePickerManager = require('NativeModules').FilePickerManager;

class AssetCheck extends Component {

  static navigationOptions = {
    title: 'Select Asset',
  };

  state = {
    refreshing: false,
    next: null,
    list: []
  }

  componentDidMount() {
    this._onRefresh()
  }

  _assetsFetch = (url) => {
    return new Promise((resolve, reject) => {
      sFetch(url, getGetParams(), (res)=>{
        resolve(res)
      },(e)=>{
        reject(e)
      });
    })

  }

  _getAssets = (url = CONST.REQ_URL_ASSET) => {
    const { list } = this.state
    this._assetsFetch(url).then(res => {
      this.setState({
        refreshing: false,
        next: res.meta.next ? CONST.HOST_NAME + res.meta.next : null,
        list: [...list, ...res.objects]
      })
    }, err => {
      this.setState({refreshing: false})
      onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
    })
  }

  _check = (item, index) => {
    const { list } = this.state
    list[index].isCheck = !list[index].isCheck
    this.setState({ list: list })
  }

  _keyExtractor = (item, index) => item.id

  _renderItem = ({ item, index }) => {
    const src = item.isCheck ? require('../img/ic_choose_up.png') : require('../img/ic_choose.png')
    return (
      <TouchableOpacity onPress={() => {this._check(item, index)}}>
        <View style={styles.listItemContainer}>
          <View style={{marginLeft: 15, marginRight: 40}}>
            <Image style={{height: 30, width: 30}} source={src} />
          </View>
          <View style={styles.listItem}>
          <View>
            <Text style={styles.title} numberOfLines={1}>{item.serial}</Text>
          </View>
          <View style={styles.listItemRow}>
            <Image style={styles.icon} source={require('../img/ic_upgrade_type.png')} />
            <Text style={styles.rowText} numberOfLines={1}>{item.application}</Text>
          </View>
          <View style={styles.listItemRow}>
            <Image style={styles.icon} source={require('../img/ic_upgrade_model.png')} />
            <Text style={styles.rowText} numberOfLines={1}>{item.model}</Text>
          </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  _onRefresh = () => {
    this.setState({refreshing: true, list: []}, ()=> {
      this._getAssets()
    })
  }

  _onEndReached = () => {
    const { next } = this.state
    next && this._getAssets(next)
  }

  _jump = () => {
    const { list } = this.state 
    const {
      navigation: {
        goBack,
        state: { params }
      }
    } = this.props
    const checkList = list.filter(item => item.isCheck === true)
    params && params.callback(checkList)
    goBack()
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.listContainer}>
          <FlatList
            onRefresh={this._onRefresh}
            onEndReached={this._onEndReached}
            refreshing={this.state.refreshing}
            style={styles.listStyle}
            data={this.state.list}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            ItemSeparatorComponent={() => ( <View style={{height: 10}} /> )}
          />
        </View>
        <TouchableOpacity style={styles.btn} onPress={this._jump}>
          <Text style={styles.btnText}>Submit</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f5f6',
  },

  btn: {
    width: width,
    height: 50,
    backgroundColor: '#1FA3E6',
    justifyContent: 'center',
    alignItems: 'center',
  },

  btnText: {
    fontSize: 16,
    color: '#fff'
  },

  listContainer: {
    flex: 1
  },

  listStyle: {
    flex: 1,
  },

  listItemContainer: {
    width: width,
    height: 123,
    flexDirection: 'row',
    // justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 10,
    backgroundColor: 'rgba(255, 255, 255, 1)'
  },

  listItem: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    width: width - 150
  },

  listItemRow: {
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center'
  },

  iconContainer: {
    borderLeftWidth: 1,
    borderLeftColor: '#D6DEE2',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 60
  },

  icon: {
    width: 24,
    height: 24
  },

  title: {
    fontSize: 18,
    color: '#080808'
  },

  rowText: {
    fontSize: 14,
    color: '#4a4a4a',
    marginLeft: 10,
  }
});

function select(store) {
  return new Object();
}

export default connect(select)(AssetCheck);
