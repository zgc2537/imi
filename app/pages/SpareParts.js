'use strict'

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  Dimensions,
  FlatList
} from 'react-native';

import { connect } from 'react-redux'
import * as CONST from '../utils/constants';
import {
  getPostParams,
  getGetParams,
  sFetch,
  stringReplace,
  getErrStr,
  onlyAlert
} from '../utils/utils';
const {height, width} = Dimensions.get('window');
import SButton from '../components/SButton';

class SpareParts extends Component {

  static navigationOptions = {
    title: 'Spare Parts Recommendation',
  };

  state = {
    refreshing: false,
    next: null,
    list: []
  }

  componentDidMount() {
    this._onRefresh()
  }

  _productFetch = (url) => {
    return new Promise((resolve, reject) => {
      sFetch(url, getGetParams(), (res)=>{
        resolve(res)
      },(e)=>{
        reject(e)
      });
    })

  }

  _getProduct = (url) => {
    const { list } = this.state
    this._productFetch(url).then(res => {
      this.setState({
        refreshing: false,
        next: res.meta.next ? CONST.HOST_NAME + res.meta.next : null,
        list: [...list, ...res.objects]
      })
    }, err => {
      this.setState({refreshing: false})
      onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
    })
  }

  _onRefresh = () => {
    const {
      navigation: {
        state: { params }
      }
    } = this.props
    params.asset && this.setState({refreshing: true, list: []}, ()=> {
      this._getProduct(stringReplace(CONST.REQ_URL_PRODUCT, params.asset.id))
    })
  }

  _onEndReached = () => {
    const { next } = this.state
    next && this._getProduct(next)
  }

  _submit = () => {
    const data = {
      type_name: 'SparePartsRecommentation'
    }

    sFetch(CONST.REQ_URL_BURIED, getPostParams(data), null, null, false);
  };

  _jump = (adress) => {
    this._submit();
    this.props.navigation.navigate(adress);
  };

  _keyExtractor = (item, index) => item.id

  _renderItem = ({ item, index }) => {
    const option = (
      <View style={styles.listItemContainer}>
        <View style={styles.listItem}>
          <View>
            <Text style={styles.title} numberOfLines={1}>{item.part_number}</Text>
          </View>
          <View style={styles.listItemRow}>
            <View style={styles.listItemRowItem}>
              <Text style={styles.rowTextLabel}>Part Name: </Text>
              <Text style={styles.rowText}>{item.part_name}</Text>
            </View>
            <View style={styles.listItemRowItem}>
              <Text style={styles.rowTextLabel}>Qty: </Text>
              <Text style={styles.rowText}>{item.quantity}</Text>
            </View>
          </View>
        </View>
      </View>
    )

    return (
      <View>
        {option}
        {
          this.state.list.length && (index == this.state.list.length - 1) ? (
            <SButton style={styles.requestBtn} onPress={() => {this._jump(CONST.NAV_VALVE_DOCTOR_REQUEST_SUBMITTED)}}>
              Request Quotation
            </SButton>
          ) : null
        }
      </View>
    )
  }

  render() {
    const {
      navigation: {
        state: { params }
      }
    } = this.props
    return (
      <View style={styles.container}>
        <View style={styles.idArea}>
          <Text style={styles.idText}>{params.asset.serial}</Text>
        </View>
        <View style={styles.listContainer}>
          <FlatList
            onRefresh={this._onRefresh}
            onEndReached={this._onEndReached}
            refreshing={this.state.refreshing}
            style={styles.listStyle}
            data={this.state.list}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            ItemSeparatorComponent={() => ( <View style={{height: 10}} /> )}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f5f6',
  },
  idArea: {
    height: 42,
    backgroundColor: '#133c6c',
    opacity: 0.8,
    justifyContent: 'center',
    alignItems: 'center'
  },
  idText: {
    fontSize: 18,
    color: '#fff'
  },
  listContainer: {
    flex: 1
  },
  listStyle: {
    flex: 1,
  },
  listItemContainer: {
    width: width,
    paddingTop: 12,
    paddingBottom: 18,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 30,
    backgroundColor: '#fff'
  },

  listItem: {
    flexDirection: 'column',
    justifyContent: 'space-around'
  },

  listItemRow: {
    width: width - 30 - 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 10
  },
  listItemRowItem: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  title: {
    fontSize: 16,
    color: '#080808'
  },
  rowTextLabel: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#4a4a4a',
  },
  rowText: {
    fontSize: 14,
    color: '#4a4a4a',
    marginLeft: 6,
  },
  requestBtn: {
    width: width - 40,
    marginHorizontal: 20,
    height: 50,
    marginTop: 20,
    marginBottom: 20,
  }
});

function select(store) {
  return new Object();
}

export default connect(select)(SpareParts);
