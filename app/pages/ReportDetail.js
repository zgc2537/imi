'use strict'

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableHighlight,
  TouchableOpacity,
  FlatList
} from 'react-native';

import { connect } from 'react-redux'
import SearchBar from '../components/SearchBar';
import * as CONST from '../utils/constants';
const {height, width} = Dimensions.get('window');

class ReportDetail extends Component {
  static navigationOptions = ({navigation}) => {
    let { report, isEdit } = navigation.state.params;
    return {
      title: isEdit ? 'Add Service Report' : report.name,
    }
  };

  state = {
    refreshing: false,
  }

  componentDidMount() {
    
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>111</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f7f9fa',
  },
});

function select(store) {
  return new Object();
}

export default connect(select)(ReportDetail);
