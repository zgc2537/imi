'use strict'

import React from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  ScrollView,
  StyleSheet,
  Dimensions
} from 'react-native';
import moment from 'moment';
import SLoading from '../components/SLoading';
import * as CONST from '../utils/constants';
import {
  getGetParams,
  sFetch,
  getErrStr,
  onlyAlert
} from '../utils/utils';

const {width} = Dimensions.get('window');


export default class ServiceReportDetail extends React.Component {
  static navigationOptions = {
    title: 'Service Report Detail',
  };

  state = {
    loadingVisible: false,
    asset: {},
    report: {}
  }

  componentDidMount() {
    const {
      navigation: {
        state: { params }
      }
    } = this.props
    params && this.setState({ asset: params.asset }, () => {
      this._getRepord(params.asset.id)
    })
  }

  _getRepord = (id) => {
    this.setState({loadingVisible: true})
    sFetch(`${CONST.REQ_URL_REPORT}?order_by=-id&asset__id=${id}`, getGetParams(), (res)=>{
      this.setState({
        loadingVisible: false,
        report: res.objects[0] ? res.objects[0] : {}
      })

    },(err)=>{
      this.setState({loadingVisible: false})
      onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
    });
  }

  render() {
    const { asset, report } = this.state

    return <ScrollView><View>
        <View style={styles.itemContainer}>
          <Text style={styles.title}>Asset Infomation</Text>
          <View style={styles.assetInfoRow}>
            <Text style={styles.assetInfoItem}>{asset.serial}</Text>
            <Text style={styles.assetInfoItem}>{asset.industry}</Text>
            <Text style={styles.assetInfoItem}>{asset.application}</Text>
            <Text style={styles.assetInfoItem}>{asset.model}</Text>
            <Text style={styles.assetInfoItem}>{asset.plant_name}</Text>
            <Text style={styles.assetInfoItem}>{asset.plant_type}</Text>
          </View>
        </View>
        <View style={[styles.itemContainer, styles.attachContainer]}>
          <Text style={styles.title}>Period</Text>
          <View style={styles.periodRow}>
            <Text style={styles.font4A16}>{moment(report.period_start).format('MM/DD/YYYY')}</Text>
            <Text style={styles.font4A16}>~</Text>
            <Text style={styles.font4A16}>{moment(report.period_end).format('MM/DD/YYYY')}</Text>
          </View>
        </View>
        <View style={[styles.itemContainer, styles.attachContainer]}>
          <Text style={styles.title}>Attach Report</Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={[styles.font4A16, {maxWidth: 150}]}>{report.service_file ? report.service_file.split('/').pop() : ''}</Text>
            {/* <Image style={styles.attachMore} source={require('../img/ic_upgrade_openmore.png')} /> */}
          </View>
        </View>
        <View style={styles.itemContainer}>
          <Text style={styles.title}>Problem Description</Text>
          <Text style={styles.font4A16}>{report.problem_description}</Text>
        </View>
        <View style={styles.itemContainer}>
          <Text style={styles.title}>Root Cause</Text>
          <Text style={styles.font4A16}>{report.root_cause}</Text>
        </View>
        <View style={styles.itemContainer}>
          <Text style={styles.title}>Action Taken</Text>
          <Text style={styles.font4A16}>{report.action_taken}</Text>
        </View>
        <View style={styles.itemContainer}>
          <Text style={styles.title}>Spare Part Detail</Text>
          <Text style={styles.font4A16}>{report.spare_detail ? report.spare_detail : ''}</Text>
        </View>

        {
          report.extra_assets && report.extra_assets.map((item, index) => {
            return (
              <View key={index} style={styles.otherAssetContainer}>
                <View style={{flexDirection: 'row'}}>
                  <Image source={require('../img/ic_asset_date.png')} style={styles.otherAssetIcon} />
                  <Text style={styles.font4A16}>{item.serial}</Text>
                </View>
                {/* <Image source={require('../img/ic_list_nextpage.png')} style={styles.otherAssetDel} /> */}
              </View>
            )
          })
        }
        <SLoading visible={this.state.loadingVisible} />
    </View>
</ScrollView>
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f2f5f6'
  },
  itemContainer: {
    paddingLeft: 30,
    paddingTop: 15,
    paddingBottom: 15,
    paddingRight: 15,
    marginBottom: 6,
    backgroundColor: '#fff'
  },
  title: {
    fontSize: 16,
    color: '#879AA0'
  },
  assetInfoRow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center'
  },
  assetInfoItem: {
    fontSize: 16,
    color:'#fff',
    backgroundColor: '#02B78F',
    borderRadius: 15,
    paddingHorizontal: 7,
    paddingVertical: 2,
    marginTop: 10,
    marginRight: 10
  },
  font4A16: {
    color: "#4A4A4A",
    fontSize: 16,
  },
  periodRow: {
    flexDirection: 'row'
  },
  attachContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  attachMore: {
    width: 20,
    height: 20,
    marginLeft: 9
  },
  contentInputFirst: {
    marginTop: 12
  },
  contentInput: {
    height: 87,
    borderWidth: 1,
    borderColor: "#E0E0E0",
    borderRadius: 2,
    marginTop: 6,
    textAlignVertical: 'top'
  },
  choose: {
    width: 24,
    height: 24,
    marginRight: 11
  },
  chooseContainer: {
    flexDirection: 'row'
  },
  sparePartInput: {
    marginTop: 20,
    borderWidth: 1,
    borderColor: "#E0E0E0",
    borderRadius: 2
  },
  otherAssetContainer: {
    marginBottom: 6,
    paddingLeft: 30,
    paddingTop: 15,
    paddingBottom: 15,
    paddingRight: 15,
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  otherAssetIcon: {
    width: 22,
    height: 22,
    marginRight: 18
  },
  otherAssetDel: {
    width: 24,
    height: 24
  }
});
