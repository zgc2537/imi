'use strict'

import React from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  ScrollView,
  StyleSheet,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import moment from 'moment';
import SLoading from '../components/SLoading';
import DatePicker from 'react-native-datepicker';
import * as CONST from '../utils/constants';
import {
  getPostParams,
  getFormDataPostParams,
  sFetch,
  getErrStr,
  onlyAlert
} from '../utils/utils';

const {width} = Dimensions.get('window');

var FilePickerManager = require('NativeModules').FilePickerManager;

export default class AddServiceReport extends React.Component {
  static navigationOptions = {
    title: 'Add Service Report',
  };
  state = {
    loadingVisible: false,
    asset: {},
    period_start: moment().format('YYYY/MM/DD'),
    period_end: moment().format('YYYY/MM/DD'),
    spare_part_needed: false,
    file: {},
    action_taken: '',
    extra_assets: [],
    problem_description: '',
    root_cause: '',
    spare_detail: '',
  };

  componentDidMount() {
    const {
      navigation: {
        state: { params }
      }
    } = this.props
    params && this.setState({ asset: params.asset })
  }

  needSparePartOnPress = () => {
    this.setState({spare_part_needed: !this.state.spare_part_needed});
  }

  selectFile = () => {
    FilePickerManager.showFilePicker(null, (response) => {
      if (!response.didCancel && !response.error) {
        console.log(response)
        this.setState({
          file: response
        });
      }
    });
  }

  _uploadFile = (id) => {
    if(!file.path) {
      this.setState({loadingVisible: false}, () => {
        onlyAlert(CONST.ALERT_TITLE_TIP, `Create Success!`);
        goBack();
      });
      return;
    }
    const { goBack } = this.props.navigation;
    let { file } = this.state
    let formData = new FormData();
    let fileData = { uri: 'file://' + file.path, type: "multipart/form-data", name: file.fileName };
    formData.append('service_id', id);
    formData.append('file', fileData);

    sFetch(CONST.REQ_URL_UPLOAD_FILE, getFormDataPostParams(formData), (res)=>{
      this.setState({loadingVisible: false}, () => {
        onlyAlert(CONST.ALERT_TITLE_TIP, `Create Success!`);
        goBack();
      });
    },(err)=>{
      console.log(err)
      this.setState({loadingVisible: false})
      onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
    });
  }

  _submit = () => {

    let { asset, file, period_start, period_end, problem_description, root_cause, action_taken, spare_part_needed, spare_detail, extra_assets } = this.state

    // if(!file.path) {
    //   onlyAlert(CONST.ALERT_TITLE_TIP, `Please Select File!`)
    //   return
    // }

    if(!problem_description) {
      onlyAlert(CONST.ALERT_TITLE_TIP, `Please Enter Problem Description!`)
      return
    }

    if(!root_cause) {
      onlyAlert(CONST.ALERT_TITLE_TIP, `Please Enter Root Cause!`)
      return
    }

    if(!action_taken) {
      onlyAlert(CONST.ALERT_TITLE_TIP, `Please Enter Action Taken!`)
      return
    }

    if(spare_part_needed && !spare_detail) {
      onlyAlert(CONST.ALERT_TITLE_TIP, `Please Enter Spare Detail!`)
      return
    }

    const data = {
      asset: asset.resource_uri,
      period_start: moment(period_start.replace(/\//g, '-')).startOf('day').format(),
      period_end: moment(period_end.replace(/\//g, '-')).endOf('day').format(),
      problem_description: problem_description,
      root_cause: root_cause,
      action_taken: action_taken,
      spare_part_needed: spare_part_needed,
      spare_detail: spare_detail,
      extra_assets: extra_assets.map(item => item.resource_uri)
    }

    this.setState({loadingVisible: true})
    sFetch(CONST.REQ_URL_REPORT, getPostParams(data), (res)=>{
      this._uploadFile(res.id)
    },(err)=>{
      this.setState({loadingVisible: false})
      onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
    });
  }

  _setAssetList = (list) => {
    list.length > 0 && this.setState({extra_assets: list})
  }

  _selectAsset = () => {
    this.props.navigation.navigate(CONST.NAV_ASSET_CHECK, {
      callback: this._setAssetList
    })
  }

  render() {
    const { asset, extra_assets } = this.state
    return <ScrollView><View>
        <View style={styles.itemContainer}>
          <Text style={styles.title}>Asset Infomation</Text>
          <View style={styles.assetInfoRow}>
            <Text style={styles.assetInfoItem}>{asset.serial}</Text>
            <Text style={styles.assetInfoItem}>{asset.industry}</Text>
            <Text style={styles.assetInfoItem}>{asset.application}</Text>
            <Text style={styles.assetInfoItem}>{asset.model}</Text>
            <Text style={styles.assetInfoItem}>{asset.plant_name}</Text>
            <Text style={styles.assetInfoItem}>{asset.plant_type}</Text>
          </View>
        </View>

        <View style={styles.itemContainer}>
          <Text style={styles.title}>Service Period - Adjust Dates</Text>
          <View style={styles.periodRow}>
          <DatePicker
            date={this.state.period_start}
            androidMode="spinner"
            mode="date"
            placeholder="from"
            format="YYYY/MM/DD"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            showIcon={false}
            customStyles={{
              dateText : [styles.time, styles.font4A16],
              placeholderText : [styles.time, styles.font4A16],
              dateInput: {
                borderWidth: 0,
                alignItems: 'center'
              }
            }}
            onDateChange={(date) => {this.setState({period_start: date})}}
          />
            <Text style={[styles.timeTo, styles.font4A16]}>To</Text>
            <DatePicker
              date={this.state.period_end}
              androidMode="spinner"
              mode="date"
              placeholder="to"
              format="YYYY/MM/DD"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              showIcon={false}
              customStyles={{
                dateText : [styles.time, styles.font4A16],
                placeholderText : [styles.time, styles.font4A16],
                dateInput: {
                  borderWidth: 0,
                  alignItems: 'center'
                }
              }}
              onDateChange={(date) => {this.setState({period_end: date})}}
            />

          </View>
        </View>

        <View style={[styles.itemContainer, styles.attachContainer]}>
          <Text style={styles.title}>Attach Report</Text>
          <TouchableOpacity onPress={this.selectFile}>
            <View style={{flexDirection: 'row'}}>
              <Text style={[styles.font4A16, {maxWidth: 150}]}>{this.state.file.fileName}</Text>
              <Image style={styles.attachMore} source={require('../img/ic_upgrade_openmore.png')} />
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.itemContainer}>
          <Text style={styles.title}>Other Description</Text>
          <TextInput style={[styles.contentInput, styles.contentInputFirst]}
            underlineColorAndroid='transparent' //设置下划线背景色透明 达到去掉下划线的效果
            editable={true}
            multiline={true}
            placeholder="Problem Description"
            placeholderTextColor="#8B999F"
            onChangeText={text => this.setState({problem_description: text})} />
          <TextInput style={styles.contentInput}
            underlineColorAndroid='transparent' //设置下划线背景色透明 达到去掉下划线的效果
            editable={true}
            multiline={true}
            placeholder="Root Cause"
            placeholderTextColor="#8B999F"
            onChangeText={text => this.setState({root_cause: text})} />
          <TextInput style={styles.contentInput}
            underlineColorAndroid='transparent' //设置下划线背景色透明 达到去掉下划线的效果
            editable={true}
            multiline={true}
            placeholder="Action Taken"
            placeholderTextColor="#8B999F"
            onChangeText={text => this.setState({action_taken: text})} />
        </View>

        <View style={styles.itemContainer}>
          <TouchableOpacity style={{flex: 1}} onPress={this.needSparePartOnPress}>
            <View style={styles.chooseContainer}>
              <Image source={this.state.spare_part_needed ? require('../img/ic_choose_up.png') : require('../img/ic_choose.png')} style={styles.choose} />
              <Text style={styles.font4A16}>Spare Part Need</Text>
            </View>
          </TouchableOpacity>
          <TextInput style={styles.sparePartInput}
            underlineColorAndroid='transparent' //设置下划线背景色透明 达到去掉下划线的效果
            editable={this.state.spare_part_needed}
            placeholder="Enter Spare Part Detail"
            placeholderTextColor="#8B999F"
            onChangeText={text => this.setState({spare_detail: text})} />
        </View>

        {
          extra_assets.map((item, index) => {
            return (
              <View key={index} style={styles.otherAssetContainer}>
                <View style={{flexDirection: 'row'}}>
                  <Image source={require('../img/ic_asset_date.png')} style={styles.otherAssetIcon} />
                  <Text style={styles.font4A16}>{item.serial}</Text>
                </View>
                <TouchableOpacity onPress={()=> {
                  let rows = [...extra_assets];
                  rows.splice(index, 1);
                  this.setState({ extra_assets: rows });
                }}>
                  <Image source={require('../img/ic_list_del.png')} style={styles.otherAssetDel} />
                </TouchableOpacity>
              </View>
            )
          })
        }

        <View style={styles.actionContainer}>
          <TouchableOpacity style={[styles.actionBtn, styles.actionLeftBtn]} onPress={this._submit}>
            <View>
              <Text style={styles.actionBtnText}>Submit</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.actionBtn, styles.actionRightBtn]} onPress={this._selectAsset}>
            <View >
              <Text style={styles.actionBtnText}>Add Assets</Text>
            </View>
          </TouchableOpacity>
        </View>
        <SLoading visible={this.state.loadingVisible} />
    </View>
</ScrollView>
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f2f5f6'
  },
  itemContainer: {
    paddingLeft: 30,
    paddingTop: 15,
    paddingBottom: 15,
    paddingRight: 15,
    marginBottom: 6,
    backgroundColor: '#fff'
  },
  title: {
    fontSize: 16,
    color: '#879AA0'
  },
  assetInfoRow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center'
  },
  assetInfoItem: {
    fontSize: 16,
    color:'#fff',
    backgroundColor: '#02B78F',
    borderRadius: 15,
    paddingHorizontal: 7,
    paddingVertical: 2,
    marginTop: 10,
    marginRight: 10
  },
  font4A16: {
    color: "#4A4A4A",
    fontSize: 16,
  },
  periodRow: {
    flexDirection: 'row',
    marginTop: 18,
    justifyContent: 'space-between',
  },
  time: {
    paddingTop: 4,
    paddingBottom: 4,
    paddingLeft: 14,
    paddingRight: 14,
    borderWidth: 1,
    borderColor: '#ABC7E8',
    borderRadius: 2,
    backgroundColor: '#E5F0F6',
    marginRight: 0
  },
  timeTo: {
    alignSelf: 'center'
  },
  attachContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  attachMore: {
    width: 20,
    height: 20,
    marginLeft: 9
  },
  contentInputFirst: {
    marginTop: 12
  },
  contentInput: {
    height: 87,
    borderWidth: 1,
    borderColor: "#E0E0E0",
    borderRadius: 2,
    marginTop: 6,
    textAlignVertical: 'top'
  },
  choose: {
    width: 24,
    height: 24,
    marginRight: 11
  },
  chooseContainer: {
    flexDirection: 'row'
  },
  sparePartInput: {
    marginTop: 20,
    borderWidth: 1,
    borderColor: "#E0E0E0",
    borderRadius: 2
  },
  otherAssetContainer: {
    marginBottom: 6,
    paddingLeft: 30,
    paddingTop: 15,
    paddingBottom: 15,
    paddingRight: 15,
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  otherAssetIcon: {
    width: 22,
    height: 22,
    marginRight: 18
  },
  otherAssetDel: {
    width: 24,
    height: 24
  },
  actionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  actionBtn: {
    width: width / 2,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  actionLeftBtn: {
    backgroundColor: '#0092DB'
  },
  actionRightBtn: {
    backgroundColor: '#2196F3'
  },
  actionBtnText: {
    fontSize: 16,
    color: '#fff'
  }
});
