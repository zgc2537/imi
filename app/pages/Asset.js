'use strict'

import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ImageBackground,
  Image
} from 'react-native';
import * as CONST from '../utils/constants';

const {width, height} = Dimensions.get('window');
import RNFetchBlob from 'rn-fetch-blob';
import FileViewer from 'react-native-file-viewer';
import * as Utils from '../utils/utils';
import SLoading from '../components/SLoading';
let dirs = RNFetchBlob.fs.dirs;

export default class Asset extends React.Component {
  static navigationOptions = {
    title: 'Asset Infomation',
  };

  state = {
     loadingVisible : false
  };

  _jump = (adress) => {
    const {
      navigation: {
        state: { params }
      }
    } = this.props
    params && this.props.navigation.navigate(adress, { ...params });
  }

  _openFile = (fileDir) => {
    FileViewer.open(fileDir).then(() => {
      this.setState({loadingVisible: false});
    }).catch((e) => {
      this.setState({loadingVisible: false});
      Utils.onlyAlert(CONST.ALERT_TITLE_TIP, 'Please install the pdf program');
    });
  };

  _openManual = () => {
    const {
      navigation: {
        state: { params }
      }
    } = this.props

    if(!(params.asset && params.asset.manual)) {
      Utils.onlyAlert(CONST.ALERT_TITLE_TIP, 'No such file')
      return
    }

    const fileName = params.asset.manual.split('/').pop();
    const path = `${dirs.DocumentDir}/imi/${fileName}`;
    const url = `${CONST.HOST_NAME}${params.asset.manual}`;
    
    this.setState({loadingVisible: true});

    RNFetchBlob.fs.exists(path)
    .then((exist) => {
      if(exist){
        this._openFile(path);
      } else {
        this._downloadFile(url, path);
      }
    }).catch(() => {
      this.setState({loadingVisible: false});
    });
  };

  _downloadFile = (url, path) => {
    RNFetchBlob
    .config({
      // response data will be saved to this path if it has access right.
      path : path
    })
    .fetch('GET', url, {
      //some headers ..
    })
    .then((res) => {
      this._openFile(path);
    }).catch((e) => {
      this.setState({loadingVisible: false});
      Utils.onlyAlert(CONST.ALERT_TITLE_TIP, 'File download failed');
    });
  }

  render() {
    const {
      navigation: {
        state: { params }
      }
    } = this.props
    return <ImageBackground style={{ flex: 1 }}
          source={require('../img/bg_app.png')}>
           <SLoading visible={this.state.loadingVisible} />
                <View style={styles.idArea}>
                  <Text style={styles.idText}>{params.asset.serial}</Text>
                </View>
              <TouchableOpacity style={{ flex: 1}} activeOpacity={0.9} onPress={()=>{this._jump(CONST.NAV_DOCUMENTS)}}>
                <View style={[styles.supportItem, styles.firstItem]}>
                  <Image style={styles.otherImage} source={require('../img/ic_asset_documents.png')} />
                  <View style={styles.descContainer}>
                    <Text style={styles.title}>Project Documents</Text>
                    <Text style={styles.description}>(Review TA Drawing / MDRB / ...)</Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flex: 1}} activeOpacity={0.9} onPress={this._openManual}>
                <View style={[styles.supportItem, styles.secondItem]}>
                  <Image style={styles.otherImage} source={require('../img/ic_upgrade_valve2.png')} />
                  <View style={styles.descContainer}>
                    <Text style={styles.title}>Product Manual</Text>
                    <Text style={styles.description}>Open Manual in PDF form</Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flex: 1}} activeOpacity={0.9} onPress={()=>{this._jump(CONST.NAV_SPARE_PARTS)}}>
                <View style={[styles.supportItem, styles.thirdItem]}>
                  <Image style={styles.otherImage} source={require('../img/ic_asset_manual.png')} />
                  <View style={styles.descContainer}>
                    <Text style={styles.title}>Spare Parts Recommendation</Text>
                    <Text style={styles.description}>Review the recommended Spare Parts for this Asset</Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flex: 1}} activeOpacity={0.9} onPress={()=>{this._jump(CONST.NAV_ORDER_HISTORY)}}>
                <View style={[styles.supportItem, styles.forthItem]}>
                  <Image style={styles.otherImage} source={require('../img/ic_asset_history.png')} />
                  <View style={styles.descContainer}>
                    <Text style={styles.title}>Order status & history</Text>
                    <Text style={styles.description}>Review the current order status and order history for this plant</Text>
                  </View>
                </View>
              </TouchableOpacity>
              {/* <TouchableOpacity style={{ flex: 1}} activeOpacity={0.9} onPress={()=>{}}>
                <View style={[styles.supportItem, styles.fifthItem]}>
                  <Image style={styles.otherImage} source={require('../img/ic_asset_moreinfo.png')} />
                  <View style={styles.descContainer}>
                    <Text style={styles.title}>More Information</Text>
                  </View>
                </View>
              </TouchableOpacity> */}
          </ImageBackground>;
  }
}

const styles = StyleSheet.create({
  idArea: {
    height: 42,
    backgroundColor: '#133c6c',
    opacity: 0.8,
    justifyContent: 'center',
    alignItems: 'center'
  },
  idText: {
    fontSize: 18,
    color: '#fff'
  },
  supportItem: {
    flex: 1,
    opacity: 0.8,
    // flexDirection: 'row',
    paddingLeft: 40,
    paddingRight: 40,
    justifyContent: 'center',
    alignItems: 'center'
  },
  firstItem: {
    backgroundColor: '#0091D8'
  },
  descContainer: {
    // flexGrow: 1,
    // width: 0
    alignItems: 'center'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
    color: '#fff',
    marginBottom: 4
  },
  description: {
    fontSize: 14,
    color: '#fff',
    flexWrap:'wrap',
    textAlign: 'center'
  },
  otherImage: {
    width: 24,
    height: 24,
    marginBottom: 12
  },
  secondItem: {
    backgroundColor: '#0272BE'
  },
  thirdItem: {
    backgroundColor: '#0261A2'
  },
  forthItem: {
    backgroundColor: '#024B7C'
  },
  fifthItem: {
    backgroundColor: '#082C56'
  }
});
