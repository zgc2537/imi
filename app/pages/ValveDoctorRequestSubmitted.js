'use strict'

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
} from 'react-native';

export default class ValveDoctorRequestSubmitted extends Component {

  static navigationOptions = {
    title: 'Request Submitted',
  };

  componentDidMount() {

  }

  _jump = (adress) => {
    this.props.navigation.navigate(adress);
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.contentContainer}>
          <Image style={styles.image} source={require('../img/pic_requestsubmitted.png')}/>
          <Text style={styles.txt1} >Request Submitted</Text>
          <Text style={[styles.txt2, styles.textStyle]} >Thank you!</Text>
          <Text style={[styles.txt3, styles.textStyle]} >We will get back to you with the requested quotation shortly.</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f5f6'
  },
  contentContainer: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 34,
    paddingBottom: 34,
    paddingLeft: 17,
    paddingRight: 17,
    alignItems: 'center'
  },
  image: {
    width: 164,
    height: 130
  },
  txt1: {
    fontSize: 22,
    fontWeight: 'bold',
    color: '#0091D8',
    marginTop: 32
  },
  txt2: {
    marginTop: 42
  },
  txt3: {
    marginTop: 12,
    paddingLeft: 20,
    paddingRight: 20
  },
  textStyle: {
    fontSize: 18,
    color: '#4a4a4a',
    fontWeight: 'bold',
    textAlign: 'center',
  }

});
