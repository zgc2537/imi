'use strict'

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  Dimensions,
  TextInput,
  TouchableOpacity
} from 'react-native';

import * as CONST from '../utils/constants';
const {height, width} = Dimensions.get('window');
import SButton from '../components/SButton';
import {
  getPostParams,
  sFetch,
} from '../utils/utils';

export default class ValveDoctorRequest extends Component {

  static navigationOptions = {
    title: 'Valve Doctor Request',
  };

  _jump = (adress) => {
    this.props.navigation.navigate(adress);
  };

  componentDidMount() {

  }

  _submit = () => {
    const data = {
      type_name: 'ValveDoctorRequest'
    }

    sFetch(CONST.REQ_URL_BURIED, getPostParams(data), null, null, false);
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.topContainer}>
          <Text style={styles.label}>Request Description</Text>
          <TextInput underlineColorAndroid='transparent'
          editable={true}
          placeholder="Enter Description here"
          placeholderTextColor="#8B999F"
          style={styles.descriptionInput}/>
        </View>
        <View style={styles.bottomContainer}>
          <Text style={styles.label}>Include Supporting Documents</Text>
          <View style={styles.actions}>
            <TouchableOpacity style={{flex: 1}}>
              <View style={styles.actionBtn}>
                <Image style={styles.actionImage} source={require('../img/ic_doc_audio.png')} />
                <Text style={styles.actionTxt}>Audio</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={{flex: 1}}>
              <View style={styles.actionBtn}>
                <Image style={styles.actionImage} source={require('../img/ic_doc_photo.png')} />
                <Text style={styles.actionTxt}>Photo</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={{flex: 1}}>
              <View style={styles.actionBtn}>
                <Image style={styles.actionImage} source={require('../img/ic_doc_video.png')} />
                <Text style={styles.actionTxt}>Video</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={{flex: 1}}>
              <View style={styles.actionBtn}>
                <Image style={styles.actionImage} source={require('../img/ic_doc_file.png')} />
                <Text style={styles.actionTxt}>Files</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity style={styles.btnContainer} onPress={() => {this._submit}}>
          <Text style={styles.submitBtn}>Submit</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f5f6',
  },
  topContainer: {
    backgroundColor: '#fff',
    paddingTop: 11,
    paddingBottom: 20,
    paddingLeft: 31,
    paddingRight: 31,
  },
  bottomContainer: {
    backgroundColor: '#fff',
    paddingTop: 19,
    paddingBottom: 26,
    paddingLeft: 31,
    paddingRight: 31,
    marginTop: 6
  },
  label: {
    color: '#879AA0',
    fontSize: 16
  },
  descriptionInput: {
    textAlignVertical: 'top',
    marginTop: 18,
    height: 111,
    borderWidth: 1,
    borderColor: '#e0e0e0',
    borderRadius: 2
  },
  actions: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 32
  },
  actionBtn: {
    alignItems: 'center'
  },
  actionImage: {
    width: 32,
    height: 32,
  },
  actionTxt: {
    fontSize: 16,
    color: '#879AA0',
    marginTop: 14
  },
  btnContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: width,
    height: 50
  },
  submitBtn: {
    flex: 1,
    backgroundColor: '#0091D8',
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
    textAlignVertical: 'center'
  }
});
