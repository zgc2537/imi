'use strict'

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableHighlight,
  FlatList
} from 'react-native';

import { connect } from 'react-redux'
import SearchBar from '../components/SearchBar';
import * as CONST from '../utils/constants';
import {
  getGetParams,
  sFetch,
  getErrStr,
  onlyAlert,
  encode
} from '../utils/utils';

const { width} = Dimensions.get('window');

class ValveSearchDetail extends Component {

  static navigationOptions = {
    title: 'Valve Search',
  }

  state = {
    refreshing: false,
    next: null,
    list: []
  }

  componentDidMount() {
    this._onRefresh()
  }

  _fetch = (url) => {
    return new Promise((resolve, reject) => {
      sFetch(url, getGetParams(), (res)=>{
        resolve(res)
      },(e)=>{
        reject(e)
      });
    })

  }

  _getValue = (url) => {
    const { list } = this.state
    this._fetch(url).then(res => {
      this.setState({
        refreshing: false,
        next: res.meta.next ? CONST.HOST_NAME + res.meta.next : null,
        list: [...list, ...res.objects]
      })
    }, err => {
      this.setState({refreshing: false})
      onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
    })
  }

  _onRefresh = (url = CONST.REQ_URL_VALUES) => {
    this.setState({refreshing: true, list: []}, ()=> {
      this._getValue(url)
    })
  }

  _onEndReached = () => {
    const { next } = this.state
    next && this._getValue(next)
  }

  _onFilter = () => {
    this.props.navigation.navigate(CONST.NAV_FILTER, {
      filter: 'values',
      onChangeFilter: this._onChangeFilter
    });
  }

  _onSearch = (text) => {
    if(text) {
      this._onRefresh(`${CONST.REQ_URL_VALUES}&model__icontains=${encode(text)}`)
    }else {
      this._onRefresh()
    }
  }

  _onChangeFilter = (filters) => {
    // 更新
    this._onRefresh(`${CONST.REQ_URL_VALUES}&${filters}`)
  }

  _keyExtractor = (item, index) => item.id

  _itemClick = (item) => {
    this.props.navigation.navigate(CONST.NAV_VALVE_SEARCH, {
      value: item
    });
  }

  _renderItem = ({ item }) => {
    return (
      <TouchableHighlight
        onPress={()=>{
          this._itemClick(item)
        }}>
        <View style={styles.listItemContainer}>
          <View>
            <View style={styles.rowText}>
              <Text style={styles.title}>Manufacturer:</Text>
              <Text style={styles.value} numberOfLines={1} ellipsizeMode={'tail'}>{item.maker}</Text>
            </View>
            <View style={styles.rowText}>
              <Text style={styles.title}>Product Type:</Text>
              <Text style={styles.value} numberOfLines={1} ellipsizeMode={'tail'}>{item.product_type}</Text>
            </View>
            <View style={styles.rowText}>
              <Text style={styles.title}>Model:</Text>
              <Text style={styles.value} numberOfLines={1} ellipsizeMode={'tail'}>{item.model}</Text>
            </View>
            <View style={styles.rowText}>
              <Text style={styles.title}>Application:</Text>
              <Text style={styles.value} numberOfLines={1} ellipsizeMode={'tail'}>{item.application}</Text>
            </View>
          </View>
          <Image style={styles.icon} source={require('../img/ic_list_nextpage.png')} />
        </View>
      </TouchableHighlight>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <SearchBar
          placeholder={"Model"}
          hasFilter={true}
          hasClear={false}
          onSearch={this._onSearch}
          onFilter={this._onFilter}
        />
        <View style={styles.listContainer}>
          <FlatList
            onRefresh={this._onRefresh}
            onEndReached={this._onEndReached}
            refreshing={this.state.refreshing}
            style={styles.listStyle}
            data={this.state.list}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            ItemSeparatorComponent={() => ( <View style={{height: 10}} /> )}
          />
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f5f6',
  },
  listContainer: {
    flex: 1
  },
  listStyle: {
    flex: 1,
  },
  listItemContainer: {
    width: width,
    paddingTop: 9,
    paddingBottom: 9,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 15,
    backgroundColor: '#fff'
  },

  listItemRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10
  },

  icon: {
    width: 22,
    height: 22
  },
  modelIcon: {
    marginLeft: 12
  },
  title: {
    fontSize: 16,
    color: '#496571'
  },
  value: {
    fontSize: 14,
    color: '#080808',
    width: width / 2,
    textAlign: 'right'
  },
  rowText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: width - 30 - 15 - 22 - 10,
    paddingTop: 7,
    paddingBottom: 7
  }
});

function select(store) {
  return new Object();
}

export default connect(select)(ValveSearchDetail);
