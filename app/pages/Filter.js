'use strict'

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import SLoading from '../components/SLoading';
import SelectModal from '../components/SelectModal';
import { connect } from 'react-redux'
import * as CONST from '../utils/constants';
import {
  getGetParams,
  getPostParams,
  sFetch,
  getErrStr,
  onlyAlert,
  encode
} from '../utils/utils';
const {height, width} = Dimensions.get('window');

class Filter extends Component {

  static navigationOptions = {
    title: 'Select Options',
  };

  state = {
    from: '',
    loadingVisible: false,
    modalVisible: false,
    filters: [],
    selectRow: 0,
  }

  componentDidMount() {
    this._resetFilter()
  }

  _getFilterOptions = (params) => {
    return new Promise((resolve, reject) => {
      sFetch(CONST.REQ_URL_GET_DATA, getPostParams(params),(res)=>{
        resolve(res)
      },(e)=>{
        reject(e)
      });
    })
  }

  _resetFilter = () => {
    const {
      navigation: {
        state: { params }
      }
    } = this.props

    const filters = CONST.filterType()
    this.setState({
      filters: filters[params.filter],
      from: params.filter
    })
  }

  _jump = () => {
    const {
      navigation: {
        goBack,
        state: { params }
      }
    } = this.props
    this._formatFilter() && params.onChangeFilter && params.onChangeFilter(this._formatFilter())
    goBack()
  }

  _formatFilter = () => {
    const { filters } = this.state
    const arg = filters.filter(item => item.selectValue).map((item) => {
      return `${item.key}=${encode(item.selectValue)}`
    }).join('&')
    return arg
  }

  //此处判断  自己领悟吧，看的你想死
  _openModal = (selectItem, selectRow) => {
    const { filters, from } = this.state

    if(!selectItem.options.length) {
      const filter_list = {}
      filters.filter(item => item.selectValue).map(item => {
        filter_list[item.key] = item.selectValue
      })

      // 筛选的时候没有必要级联， 此处增加了入口， 有需要可以加上
      console.log({
        cls: CONST.DATA_BASE[from],
        param: selectItem.key,
        filter_list
      })

      this.setState({loadingVisible: true})
      
      this._getFilterOptions({
        cls: CONST.DATA_BASE[from],
        param: selectItem.key
      }).then(res => {
        // 此处给options赋值
        filters[selectRow].options = res.data
        this.setState({
          loadingVisible: false,
          filters: filters,
          selectRow: selectRow,
          modalVisible: true,
        })
      }, err => {
        this.setState({loadingVisible: false})
        onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
      })

    } else {

      this.setState({
        selectRow: selectRow,
        modalVisible: true,
      })

    }


  }
 
  _onSelected = (item) => {
    const { selectRow, filters } = this.state
    filters[selectRow].selectValue = item
    // 此处可增加级联入口
    this.setState({
      filters: [...filters],
      modalVisible: false
    })
  }

  _keyExtractor = (item, index) => index

  _renderItem = ({item, index}) => {
    return (
      <View>
        <Text style={styles.libTitle}>{item.name}</Text>
        <TouchableOpacity
          onPress={()=> {this._openModal(item, index)}}
          >
          <View style={styles.itemSelect}>
            <Text style={{maxWidth: width / 2, color: item.selectValue ? '#252525' : '#8B999F'}} numberOfLines={1}>{item.selectValue ? item.selectValue : `Select ${item.name}`}</Text>
            <Image style={{width: 25, height: 25}} source={require('../img/ic_list_fold.png')} />
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  render() {
    const { filters, selectRow } = this.state
    return (
      <View style={styles.container}>
        <FlatList
          style={styles.scorllStyle}
          data={this.state.filters}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          // ItemSeparatorComponent={() => ( <View style={{height: 10}} /> )}
        />
        <View style={styles.btnGroup}>
          <TouchableOpacity style={[styles.btn, styles.btnStyle]} onPress={this._resetFilter}>
            <Text style={styles.btnText}>Reset</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btn} onPress={this._jump}>
            <Text style={styles.btnText}>OK</Text>
          </TouchableOpacity>
        </View>

        <SelectModal
          visible={this.state.modalVisible}
          onClose={()=> {this.setState({modalVisible: false})}}
          onChange={this._onSelected}
          list={filters.length ? filters[selectRow].options : []}
          placeholder={filters.length ? filters[selectRow].name : ''}
        />

        <SLoading visible={this.state.loadingVisible} modalStyles={{backgroundColor: 'transparent'}}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f5f6',
  },

  btnGroup: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: width,
    height: 50,
    flexDirection: 'row',
  },

  btn: {
    flex: 1,
    backgroundColor: '#0092DB',
    justifyContent: 'center',
    alignItems: 'center',
  },

  btnStyle: {
    backgroundColor: '#1FA3E6'
  },

  btnText: {
    fontSize: 16,
    color: '#fff'
  },

  scorllStyle: {
    marginBottom: 50,
    backgroundColor: 'rgba(255, 255, 255, 0.5)'
  },

  libTitle: {
    paddingLeft: 30,
    fontSize: 16,
    color: '#496571',
    paddingVertical: 10,
  },

  itemSelect: {
    marginHorizontal: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    paddingVertical: 7,
    borderColor: '#ddd',
    borderWidth: 1
  }
});

function select(store) {
  return new Object();
}

export default connect(select)(Filter);
