/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */

import React, { Component } from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TextInput,
  View,
  ScrollView,
  Image,
  Alert,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';

import { connect } from 'react-redux';
import SLoading from '../components/SLoading';
// import Modal from 'react-native-modalbox';

import * as CONST from '../utils/constants';
import { NewTextInputStyles } from '../utils/styles';
import SButton from '../components/SButton';
import * as Utils from '../utils/utils';
import ToastUtil from '../utils/ToastUtil';
import NavigationUtil from '../utils/NavigationUtil';
let opt;
const { width, height } = Dimensions.get('window');

class LoginScreen extends Component{
  state = {
     username: '',
     password: '',
     loadingVisible : false
  };

   componentDidMount(){

   }

   componentWillReceiveProps(nextProps){
     const that = this;
     if((nextProps.loginStatus != this.props.loginStatus) && nextProps.loginStatus == CONST.NETWORK_REQUEST_STATE.DOING ){
       this.setState({loadingVisible : true});
       return;
     }
     if((nextProps.loginStatus != this.props.loginStatus) && nextProps.loginStatus == CONST.NETWORK_REQUEST_STATE.DONE ){
       this.setState({loadingVisible : false});
       if((nextProps.isLoggedIn != this.props.isLoggedIn) && nextProps.isLoggedIn){
           this.toMain();
           return;
       }
     }

     if((nextProps.loginStatus != this.props.loginStatus) && nextProps.loginStatus == CONST.NETWORK_REQUEST_STATE.ERROR){
         let e = nextProps.loginError;
         let strErr = Utils.getErrStr(e);
         Utils.onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${strErr}!`,()=>{
           this.setState({loadingVisible : false});
         });
     }
   }

   toMain = () => {
      NavigationUtil.reset(this.props.navigation, CONST.NAV_HOME_PAGE);
   }

   handleLogin = () => {
     const { actions } = this.props;
     if(Utils.trim(this.state.username) == '') {
       Utils.onlyAlert(CONST.ALERT_TITLE_TIP, `Please Enter Email`);
       return;
     }
     if(Utils.trim(this.state.password) == '') {
       Utils.onlyAlert(CONST.ALERT_TITLE_TIP, `Please Enter Password`);
       return;
     }
     opt = {
       username: this.state.username,
       password: this.state.password
     }
     actions.logIn(opt);
   }

   _onChangeUsername = (text) => {
     this.setState({'username': text});
   }

   _onChangePassword = (text) => {
      this.setState({'password': text});
   }

   scrollViewTo(distance){
     this.refs.scroll.scrollTo({y:distance, animated : true});
 　}

   _pwOnFocus = () => {
     this.scrollViewTo(0);
   };

   render(){
       return (
         <View style={styles.container}>
           <KeyboardAvoidingView behavior='position' >
           {/* <ScrollView> */}
           <ScrollView ref='scroll' keyboardShouldPersistTaps='always'>
           <View>
             <Image source={require('../img/login_logo.png')} style={styles.logo}></Image>
             <Text style={styles.title}>Valve Doctor Solutions</Text>
               <TextInput
                 style={styles.input}
                 underlineColorAndroid='transparent' //设置下划线背景色透明 达到去掉下划线的效果
                 editable={true}
                 onChangeText={this._onChangeUsername}
                 placeholder="Email"
                 placeholderTextColor="#8B999F"
                 onFocus={this._pwOnFocus}
               />
               <TextInput
                 secureTextEntry={true}
                 style={styles.input}
                 underlineColorAndroid='transparent' //设置下划线背景色透明 达到去掉下划线的效果
                 editable={true}
                 textContentType='password'
                 placeholder="Password"
                 placeholderTextColor="#8B999F"
                 onChangeText={this._onChangePassword}
               />
               <SButton style={styles.loginBtn} onPress={this.handleLogin}>
                 Login
               </SButton>
             {/* <Modal style={styles.loadingModal} position={"center"} ref={"loadingModal"} backdropPressToClose={false} swipeToClose={false} backdropOpacity={0} isDisabled={false}>
               <ActivityIndicator size="large" color="#fff" />
               <Text style={styles.modalText}>用户登陆中...</Text>
             </Modal> */}
             <SLoading visible={this.state.loadingVisible} />
          </View>
          </ScrollView>
          </KeyboardAvoidingView>
          <View style={styles.actionContainer}>
            <TouchableOpacity style={{flex: 1}} onPress={() => {}}><Text style={styles.bottomText}>Register</Text></TouchableOpacity>
            <TouchableOpacity style={{flex: 1}} onPress={() => {}}><Text style={[styles.bottomText, {textAlign: 'right'}]}>Forgot password</Text></TouchableOpacity>
          </View>
      </View>
       );
   }
}

const styles = StyleSheet.create({
  container: {
    height:height,
    backgroundColor: '#F1F5F6'
  },
  logo: {
    width: 164,
    height: 109,
    marginTop: 76,
    alignSelf: 'center'
  },
  title: {
    fontSize: 24,
    color: '#575757',
    fontWeight: 'bold',
    marginTop: 16,
    marginBottom: 29,
    alignSelf: 'center'
  },
  loadingModal:{
    justifyContent: 'center',
    alignItems: 'center',
    width:120,
    height:120,
    backgroundColor:'#777',
    borderRadius:7,
  },
  modalText:{
    marginTop:15,
    color:'#fff',
  },
  input: {
    width: 320,
    height: 50,
    backgroundColor: '#fff',
    borderRadius: 4,
    marginTop: 10,
    alignSelf: 'center',
    paddingLeft: 10,
    fontSize: 14
  },
  smscodeInput:{
    flex: 1,
  },
  loginBtn:{
    width: 320,
    height: 50,
    alignSelf: 'center',
    marginTop: 20
  },
  bottomText: {
    fontSize: 14,
    color: '#8b999f'
  },
  actionContainer: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    paddingHorizontal: 30,
    paddingVertical: 11,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});

export default LoginScreen;
