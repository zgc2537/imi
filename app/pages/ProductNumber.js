'use strict'

import React from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import SButton from '../components/SButton';
import * as CONST from '../utils/constants';
import { onlyAlert } from '../utils/utils';

export default class ProductNumber extends React.Component {
  static navigationOptions = {
    title: 'Enter Product Number',
  };
  
  state = {
    value: ''
  }

  ok = () => {
    if(this.state.value) {
      this.props.navigation.navigate(CONST.NAV_SELECT_DATABASE, {
        defValue: this.state.value
      })
    }else {
      onlyAlert(CONST.ALERT_TITLE_TIP, `Please Enter Serial Number!`);
    }
  };

  render() {
    return <View style={styles.container}>
      <Image style={styles.logo} source={require('../img/login_logo.png')}/>
      <Text style={styles.tip}>Please enter the Serial Number</Text>
      <TextInput style={styles.input}
       underlineColorAndroid='transparent' //设置下划线背景色透明 达到去掉下划线的效果
       editable={true}
       onChangeText={(text)=> this.setState({value: text})}
       placeholder="Serial Number"
       placeholderTextColor="#8B999F"/>
       <SButton style={styles.okBtn} onPress={this.ok}>
         OK
       </SButton>
    </View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  logo: {
    marginTop: 23,
    alignSelf: 'center',
    width: 121,
    height: 101
  },
  tip: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#4a4a4a',
    alignSelf: 'center',
    marginTop: 9
  },

  input: {
    paddingLeft: 10,
    alignSelf: 'center',
    marginTop: 23,
    borderWidth: 2,
    borderColor: '#0091d8',
    borderRadius: 2,
    marginLeft: 30,
    marginRight:30,
    width:320,
    height: 50
  },

  okBtn: {
    alignSelf: 'center',
    marginTop: 29,
    width:320,
    height: 50
  }

});
