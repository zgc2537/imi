'use strict'

import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ImageBackground,
  Image,
  Linking
} from 'react-native';
import * as CONST from '../utils/constants';

const {width, height} = Dimensions.get('window');

export default class Support extends React.Component {
  static navigationOptions = {
    title: 'Support',
  };

  _jump = (adress) => {
    this.props.navigation.navigate(adress);
  };

  _linking = (str, type) => {
    Linking.openURL(`${type}:${str}`).catch(err => console.log(err));
  }

  render() {
    return <ImageBackground style={{ flex: 1 }}
          source={require('../img/bg_app.png')}>
              <TouchableOpacity style={{flex: 1}} activeOpacity={0.9} onPress= {() => this._jump(CONST.NAV_CHAT)} >
                <View style={[styles.supportItem, styles.chatItem]}>
                  <Image style={styles.chatImage} source={require('../img/ic_support_message.png')} />
                  <View style={styles.descContainer}>
                    <Text style={styles.title}>Chat</Text>
                    <Text style={styles.description}>Message one of our experts for advice</Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{flex: 1}} activeOpacity={0.9} onPress={()=>{this._linking('', 'tel')}}>
                <View style={[styles.supportItem, styles.callItem]}>
                  <Image style={styles.otherImage} source={require('../img/ic_support_call.png')} />
                  <View style={styles.descContainer}>
                    <Text style={styles.title}>Call</Text>
                    <Text style={styles.description} numberOfLines={2}>Call for Technical Consultation or Urgent Troubleshooting</Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{flex: 1}} activeOpacity={0.9} onPress= {() => this._jump(CONST.NAV_VALVE_DOCTOR_REQUEST)} >
                <View style={[styles.supportItem, styles.requestItem]}>
                  <Image style={styles.otherImage} source={require('../img/ic_support_request.png')} />
                  <View style={styles.descContainer}>
                    <Text style={styles.title}>Request Valve Doctor</Text>
                    <Text style={styles.description}>Submit your request with a problem description for our experts to review</Text>
                  </View>
                </View>
              </TouchableOpacity>
          </ImageBackground>;
  }
}

const styles = StyleSheet.create({
  supportItem: {
    flex: 1,
    opacity: 0.8,
    // flexDirection: 'row',
    paddingLeft: 40,
    paddingRight: 40,
    justifyContent: 'center',
    alignItems: 'center'
  },
  chatItem: {
    backgroundColor: '#0091D8'
  },
  chatImage: {
    width: 24,
    height: 22,
    marginBottom: 12
  },
  descContainer: {
    // flexGrow: 1,
    // width: 0
    alignItems: 'center'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff',
    marginBottom: 4
  },
  description: {
    fontSize: 14,
    color: '#fff',
    flexWrap:'wrap',
    textAlign: 'center'
  },
  otherImage: {
    width: 24,
    height: 24,
    marginBottom: 12
  },
  callItem: {
    backgroundColor: '#0272BE'
  },
  requestItem: {
    backgroundColor: '#0261A2'
  },
});
