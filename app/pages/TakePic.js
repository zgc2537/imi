'use strict';
import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Image,
  Dimensions
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import * as CONST from '../utils/constants';
const { width, height } = Dimensions.get('window');

export default class TakePic extends Component {
  static navigationOptions = {
    title: 'Take A Product Photo',
  };

  _jump = (adress) => {
    this.props.navigation.navigate(adress);
  };

  render() {
  return (
    <View style={styles.container}>
      <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style = {styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.auto}
          permissionDialogTitle={'Permission to use camera'}
          permissionDialogMessage={'We need your permission to use your camera phone'}
      />
      <View style={styles.model}>
        <TouchableOpacity style={{flex: 1}} onPress={() => this._jump(CONST.NAV_IMAGE_SUBMITTED)}>
          <View style={styles.picBtn}>
            <Image source={require('../img/ic_camera.png')} style={styles.picImg}/>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

takePicture = async function() {
  if (this.camera) {
    const options = { quality: 0.5, base64: true };
    const data = await this.camera.takePictureAsync(options)
    console.log(data.uri);
  }
};
}

const styles = StyleSheet.create({
container: {
  flex: 1,
  flexDirection: 'column',
  backgroundColor: 'black'
},
preview: {
  flex: 1,
  justifyContent: 'flex-end',
  alignItems: 'center'
},
model: {
  position: 'absolute',
  bottom: 0,
  left: 0,
  height: 100,
  width: width,
  alignItems: 'center'
},
picBtn: {
  width: 60,
  height: 60,
  borderRadius: 30,
  backgroundColor: '#fff',
  justifyContent: 'center',
  alignItems: 'center'
},
picImg: {
  width: 35,
  height: 35
}
});
