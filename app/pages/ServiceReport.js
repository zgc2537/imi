'use strict'

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  FlatList
} from 'react-native';

import { connect } from 'react-redux'
import SearchBar from '../components/SearchBar';
import * as CONST from '../utils/constants';
import {
  getGetParams,
  sFetch,
  getErrStr,
  onlyAlert,
  encode
} from '../utils/utils';

const {width} = Dimensions.get('window');

class ServiceReport extends Component {

  static navigationOptions = {
    title: 'Service Report',
  };

  state = {
    refreshing: false,
    next: null,
    list: []
  }

  componentDidMount() {
    this._onRefresh()
  }

  _assetsFetch = (url) => {
    return new Promise((resolve, reject) => {
      sFetch(url, getGetParams(), (res)=>{
        resolve(res)
      },(e)=>{
        reject(e)
      });
    })

  }

  _getAssets = (url) => {
    const { list } = this.state;
    if(this.props.user.sfdc_account) {
      if(url.indexOf("?") != -1) {
        url += "&sfdc_account=" + this.props.user.sfdc_account;
      } else {
        url += "?sfdc_account=" + this.props.user.sfdc_account;
      }
    }
    this._assetsFetch(url).then(res => {
      this.setState({
        refreshing: false,
        next: res.meta.next ? CONST.HOST_NAME + res.meta.next : null,
        list: [...list, ...res.objects]
      })
    }, err => {
      this.setState({refreshing: false})
      onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
    })
  }

  _onFilter = () => {
    this.props.navigation.navigate(CONST.NAV_FILTER, {
      filter: 'report',
      onChangeFilter: this._onChangeFilter
    });
  }

  _onSearch = (text) => {
    if(text) {
      this._onRefresh(`${CONST.REQ_URL_ASSET}?serial__icontains=${encode(text)}`)
    }else {
      this._onRefresh()
    }
  }

  _onChangeFilter = (filters) => {
    // 更新
    console.log(`${CONST.REQ_URL_ASSET}?${filters}`)
    this._onRefresh(`${CONST.REQ_URL_ASSET}?${filters}`)
  }

  _keyExtractor = (item, index) => item.id

  _jump = (item, isEdit = false) => {
    this.props.navigation.navigate(CONST.NAV_ADD_SERVICE_REPORT, {
      asset: item,
      isEdit: isEdit
    });
  }

  _jumpHistory = (item, isEdit = false) => {
    this.props.navigation.navigate(CONST.NAV_SERVICE_REPORT_DETAIL, {
      asset: item,
      isEdit: isEdit
    });
  }

  _renderItem = ({ item }) => {
    return (
      <View style={styles.listItemContainer}>
        <View style={styles.listItem}>
          <View>
            <Text style={styles.title} numberOfLines={1}>{item.serial}</Text>
          </View>
          <View style={styles.listItemRow}>
            <Image style={styles.icon} source={require('../img/ic_upgrade_type.png')} />
            <Text style={styles.rowText} numberOfLines={1}>{item.application}</Text>
          </View>
          <View style={styles.listItemRow}>
            <Image style={styles.icon} source={require('../img/ic_upgrade_model.png')} />
            <Text style={styles.rowText} numberOfLines={1}>{item.model}</Text>
          </View>
        </View>
        <TouchableOpacity style={styles.iconContainer} onPress={()=>{this._jump(item, true)}}>
          <Image style={styles.icon} source={require('../img/ic_list_addreport.png')} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconContainer} onPress={()=>{this._jumpHistory(item)}}>
          <Image style={styles.icon} source={require('../img/ic_list_reporthistory.png')} />
        </TouchableOpacity>
      </View>

    )
  }

  _onRefresh = (url = CONST.REQ_URL_ASSET) => {
    this.setState({refreshing: true, list: []}, ()=> {
      this._getAssets(url)
    })
  }

  _onEndReached = () => {
    const { next } = this.state
    next && this._getAssets(next)
  }

  render() {
    return (
      <View style={styles.container}>
        <SearchBar
          placeholder={"Serial"}
          hasFilter={true}
          hasClear={false}
          onSearch={this._onSearch}
          onFilter={this._onFilter}
        />
        <View style={styles.listContainer}>
          <FlatList
            onRefresh={this._onRefresh}
            onEndReached={this._onEndReached}
            refreshing={this.state.refreshing}
            style={styles.listStyle}
            data={this.state.list}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            ItemSeparatorComponent={() => ( <View style={{height: 10}} /> )}
          />
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f5f6',
  },

  listContainer: {
    flex: 1
  },

  listStyle: {
    flex: 1,
  },

  listItemContainer: {
    width: width,
    height: 123,
    flexDirection: 'row',
    // justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 10,
    backgroundColor: 'rgba(255, 255, 255, 1)'
  },

  listItem: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    width: width - 150
  },

  listItemRow: {
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center'
  },

  iconContainer: {
    borderLeftWidth: 1,
    borderLeftColor: '#D6DEE2',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 60
  },

  icon: {
    width: 24,
    height: 24
  },

  title: {
    fontSize: 18,
    color: '#080808'
  },

  rowText: {
    fontSize: 14,
    color: '#4a4a4a',
    marginLeft: 10,
  }
});

function select(store) {
  return {
    user: store.userInfo.user
  };
}

export default connect(select)(ServiceReport);
