'use strict'

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback
} from 'react-native';

import { connect } from 'react-redux'
import NavigationUtil from '../utils/NavigationUtil';
import * as CONST from '../utils/constants';
import SModal from '../components/SModal';
const {height, width} = Dimensions.get('window');
import { logOut } from '../actions/user';
import SLoading from '../components/SLoading';
class Home extends Component {

  static navigationOptions = {
    header: null
  };

  state = {
    loadingVisible: false,
    quitVisible: false,
    data: [
      {
        name: 'Remote Assist',
        list: [
          {
            name: 'Support',
            nav: CONST.NAV_SUPPORT_PAGE,
            active: false,
            source: require('../img/ic_home_support.png'),
            activeSource: require('../img/ic_home_support_up.png')
          }, {
            name: 'Contacts',
            nav: CONST.NAV_CONTACTS,
            active: false,
            source: require('../img/ic_home_contact.png'),
            activeSource: require('../img/ic_home_contact_up.png')
          }
        ]
      },
      {
        name: 'Asset Management',
        list: [
          {
            name: 'Product & Asset',
            nav: CONST.NAV_ASSET_INFO_PAGE,
            active: false,
            source: require('../img/ic_home_productinf.png'),
            activeSource: require('../img/ic_home_productinf_up.png')
          }, {
            name: 'Upgrades',
            nav: CONST.NAV_UPGRADES_PAGE,
            active: false,
            source: require('../img/ic_home_upgrades.png'),
            activeSource: require('../img/ic_home_upgrades_up.png')
          }
        ]
      },
      {
        list: [
          {
            name: 'Service Report',
            nav: CONST.NAV_SERVICE_REPORT,
            active: false,
            source: require('../img/ic_home_servicerp.png'),
            activeSource: require('../img/ic_home_servicerp_up.png')
          }, {
            name: 'Utilities',
            nav: CONST.NAV_UTILITIES,
            active: false,
            source: require('../img/ic_home_utilities.png'),
            activeSource: require('../img/ic_home_utilities_up.png')
          }
        ]
      }
    ]
  }

  componentDidMount() {
    console.log(this.props.user)
  }

  componentWillReceiveProps(nextProps){
    const that = this;
    if((nextProps.logoutStatus != this.props.logoutStatus) && nextProps.logoutStatus == CONST.NETWORK_REQUEST_STATE.DOING ){
      this.setState({loadingVisible : true});
      return;
    }
    if((nextProps.isLoggedIn != this.props.isLoggedIn) && !nextProps.isLoggedIn ){
      this.setState({loadingVisible : false});
      this.toLogin();
      return;
    }
  }

  toLogin = () =>  {
    NavigationUtil.reset(this.props.navigation, CONST.NAV_LOGIN);
  };

  _changeActive = (row, col, status) => {
    let temp = this.state.data;
    temp[row].list[col].active = status;
    this.setState({ data:  temp});
  }

  _onShowUnderlay = (row, col) => {
    this._changeActive(row, col, true)
  }

  _onHideUnderlay = (row, col) => {
    this._changeActive(row, col, false)
  }

  _jump = (row, col) => {

    this.timer = setTimeout(() => {
      this.timer && clearTimeout(this.timer);
      NavigationUtil.next(this.props.navigation, this.state.data[row].list[col].nav);
    });

  }

  renderGroup = (data, key) => {
    let group = [];
    let content = [];

    data.list.map((item, index)=> {
      content.push(<TouchableWithoutFeedback
      key={index}
      onPressIn={()=>{this._changeActive(key, index, true)}}
      onPress={()=>{this._jump(key, index)}}
      onPressOut={()=>{this._changeActive(key, index, false)}}
      >
        <View key={index} style={[styles.groupItem, !index ? {borderRightWidth:1, borderColor:'#ddd'} : {}]}>
          <View style={[styles.imgBox, item.active && item.activeSource ? {backgroundColor: '#0091D8'} : null]}>
            <Image style={styles.icon} source={item.active && item.activeSource ? item.activeSource : item.source} />
          </View>
          <View style={styles.itemTextBox}>
            <Text style={[styles.itemText, item.active ? {color: '#0091D8'} : null]}>{item.name}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>)
    })

    group.push(<View style={styles.group}>
      {content}
    </View>)

    return group
  }

  renderContent = ()=> {
    let tmp = [];
    this.state.data.map((item, index)=> {
      if(item.name) {
        tmp.push(<View key={index} style={styles.groupTitle}>
          <Text style={styles.groupTitleText}>{item.name}</Text>
        </View>)
      }
      tmp.push(this.renderGroup(item, index))
    })
    return tmp
  }

  _submit = () => {
    this.props.dispatch(logOut(this.props.imei));
  };

  render() {
    const { user } = this.props
    let avatar
    if (user.avatar) {
      avatar = {uri: `${CONST.HOST_NAME}/media/${user.avatar}`}
    } else {
      avatar = require('../img/ic_contacts_userpic.png')
    }
    return (
      <View style={styles.container}>
      <SModal visible={this.state.quitVisible} canWindowCloseOnTouchOutside={true}>
        <View style={styles.inputContainer}>
          <View style={styles.inputHeaderContainer}>
            <Text style={styles.inputHeader}>Tip</Text>
          </View>
          <View style={styles.inputContentContainer}>
            <Text style={styles.tipText}>Are you sure to log out?</Text>
          </View>
          <View style={styles.inputActionContainer}>
            <TouchableOpacity style={styles.inputBtn} onPress={()=> {
              this.setState({quitVisible: false})
            }}>
              <Text style={styles.inputBtnText}>NO</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.inputBtn} onPress={this._submit}>
              <Text style={styles.inputBtnText}>YES</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SModal>
        <View style={styles.header}>
          <View style={styles.bg}>
            <Text style={styles.title}>Valve Doctor Solutions</Text>
          </View>
          <TouchableOpacity onPress={() => {this.setState({quitVisible: true})}}>
            <Image style={styles.user} source={avatar} />
          </TouchableOpacity>
          <Image style={styles.logo} source={require('../img/logo.png')} />
        </View>
        <ScrollView style={styles.content}>
          {this.renderContent()}
          <View style={styles.margin}></View>
        </ScrollView>

        <SLoading visible={this.state.loadingVisible} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#212A35',
  },
  header: {
    paddingTop: 24,
    height: 88,
    width: width,
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 10,
    borderBottomWidth: 1,
    borderColor: '#ddd'
  },
  user: {
    height: 44,
    width: 44,
    borderRadius: 22,
    overflow: 'hidden'
  },
  logo: {
    height: 44,
    width: 64,
    marginTop: 5,
  },
  title: {
    fontSize: 20,
    color: '#000',
    fontWeight: 'bold',
  },
  bg: {
    position: 'absolute',
    top: 24,
    height: 64,
    width: width,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  content: {
    paddingHorizontal: 20,
  },
  groupTitle: {
    paddingTop: 15,
    paddingBottom: 10,
    alignItems: 'center'
  },
  groupTitleText: {
    color: '#fff',
    fontSize: 18,
  },
  group: {
    flexDirection: 'row',
  },
  groupItem: {
    flex:1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#ddd'
  },
  imgBox: {
    width: (width - 40) / 2,
    paddingVertical: 20,
    backgroundColor: '#F7F9FA',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: 62,
    height: 62
  },
  itemTextBox: {
    backgroundColor: '#fff',
    width: (width - 40) / 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 8,
  },
  itemText: {
    color: '#8B999F',
    fontSize: 14
  },
  margin: {
    height: 20
  },
  tipText: {
    fontSize: 16
  },
  inputContainer: {
    width:270,
    borderRadius: 8,
    backgroundColor: '#fff'
  },
  inputHeaderContainer: {
    borderBottomWidth: 1,
    borderColor: '#DDE1E2'
  },
  inputHeader: {
    fontSize: 18,
    color: '#000',
    marginTop: 11,
    marginBottom: 9,
    marginLeft: 26
  },
  inputContentContainer: {
    paddingTop: 17,
    paddingLeft: 24,
    paddingRight: 24,
    paddingBottom: 17
  },
  inputLabel: {
    marginTop: 10,
    fontSize: 16,
    color: '#496571'
  },
  inputTextInput: {
    marginTop: 12,
    height: 40,
    borderRadius: 2,
    borderWidth: 1,
    borderColor: '#B8C9D1'
  },
  inputActionContainer: {
    flexDirection: 'row',
    borderTopWidth: 1,
    borderColor: '#DDE1E2'
  },
  inputBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 135,
    height: 42
  },
  inputBtnText: {
    color: '#4E6975',
    fontSize: 16
  }
});


function select(store){
  return {
      logoutStatus: store.userInfo.logoutStatus,
      isLoggedIn: store.userInfo.isLoggedIn,
      user: store.userInfo.user
  }
}

export default connect(select)(Home);
