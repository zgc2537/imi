'use strict'

import React, {Component} from 'react';
import {
  Platform,
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  Dimensions,
  TouchableHighlight,
  TouchableOpacity,
  FlatList
} from 'react-native';
import moment from 'moment';
import { connect } from 'react-redux'
import SearchBar from '../components/SearchBar';
import SLoading from '../components/SLoading';
import * as CONST from '../utils/constants';
import {
  getGetParams,
  sFetch,
  stringReplace,
  getErrStr,
  onlyAlert,
  getFileSize
} from '../utils/utils'
const {height, width} = Dimensions.get('window');

class CaseStudies extends Component {

  static navigationOptions = {
    title: 'Upgrade Packages',
  };

  state = {
    loadingVisible: false,
    refreshing: false,
    next: null,
    list: []
  }

  componentDidMount() {
    this._onRefresh()
  }

  _assetsFetch = (url) => {
    return new Promise((resolve, reject) => {
      sFetch(url, getGetParams(), (res)=>{
        resolve(res)
      },(e)=>{
        reject(e)
      });
    })

  }

  _getAssets = (url) => {
    const { list } = this.state
    this._assetsFetch(url).then(res => {
      this.setState({
        refreshing: false,
        next: res.meta.next ? CONST.HOST_NAME + res.meta.next : null,
        list: [...list, ...res.objects]
      })
    }, err => {
      this.setState({refreshing: false})
      onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
    })
  }

  _onRefresh = () => {
    this.setState({refreshing: true, list: []}, ()=> {
      this._getAssets(CONST.REQ_URL_SUCCESS_CASE)
    })
  }

  _onEndReached = () => {
    const { next } = this.state
    next && this._getAssets(next)
  }

  _onSearch = (text) => {
    console.log(text);
  }

  _onFilter = () => {
    this.props.navigation.navigate(CONST.NAV_FILTER, {
      changeFilter: this._changeFilter
    });
  }

  _changeFilter = (filters) => {
    //更新
    console.log(filters);
  }

  _keyExtractor = (item, index) => item.id

  _itemClick = (item) => {
    this.props.navigation.navigate(CONST.NAV_UPGRADE_PAC_DETAIL, {
      detail: item
    });
  }


  _renderItem = ({ item }) => {
    const imgUrl = item.icon ? {uri: `${CONST.HOST_NAME}${item.icon}`} : require('../img/logo.png');

    return (
      <TouchableHighlight
        onPress={()=>{
          this._itemClick(item)
        }}>
        <View style={styles.listItemContainer}>
          <View style={styles.listItemRowLeft}>
            <Image style={styles.fileIcon} source={imgUrl} />
            <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">{item.application}</Text>
          </View>
          <Image style={styles.rightIcon} source={require('../img/ic_list_nextpage.png')}/>
            {/**<View style={styles.listItemRow}>
              <Text style={styles.rowText} numberOfLines={3}>{item.header}</Text>
            </View>**/}
        </View>
      </TouchableHighlight>

    )
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.listContainer}>
          <FlatList
            onRefresh={this._onRefresh}
            onEndReached={this._onEndReached}
            refreshing={this.state.refreshing}
            style={styles.listStyle}
            data={this.state.list}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            ItemSeparatorComponent={() => ( <View style={{height: 10}} /> )}
          />
        </View>
        <SLoading visible={this.state.loadingVisible} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f5f6',
  },

  listContainer: {
    flex: 1
  },

  listStyle: {
    flex: 1,
  },

  listItemContainer: {
    width: width,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#fff',
    paddingVertical: 10
  },

  listItemRowLeft: {
    flexDirection: 'row',
    alignItems: 'center'
  },

  fileIcon: {
    backgroundColor: '#ddd',
    width: 90,
    height: 63
  },

  title: {
    fontSize: 14,
    color: '#4a4a4a',
    width: 200,
    marginLeft: 20
  },
  rightIcon: {
    width: 24,
    height: 24
  },
  rowText: {
    fontSize: 14,
    color: '#4a4a4a',
    marginLeft: 20,
  }
});

function select(store) {
  return new Object();
}

export default connect(select)(CaseStudies);
