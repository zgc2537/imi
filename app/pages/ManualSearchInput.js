'use strict'

import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ImageBackground,
  Image
} from 'react-native';
import * as CONST from '../utils/constants';

const {width, height} = Dimensions.get('window');

export default class ManualSearchInput extends React.Component {
  static navigationOptions = {
    title: 'Manual Search & Input',
  };

  render() {
    return <ImageBackground style={{ flex: 1 }}
          source={require('../img/bg_app.png')}>
              <TouchableOpacity style={{flex: 1}} activeOpacity={0.8} onPress={() => this.props.navigation.navigate(CONST.NAV_VALVE_SEARCH_DETAIL)}>
                <View style={[styles.supportItem, styles.firstItem]}>
                  <View style={styles.supportSubContainer}>
                    {/* <Image style={styles.otherImage} source={require('../img/ic_upgrade_valve1.png')} /> */}
                    <View style={styles.descContainer}>
                      <Text style={styles.title}>Valve Search</Text>
                      <Text style={styles.description}>Review List of Valves to replace</Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{flex: 1}} activeOpacity={0.8} onPress={() => this.props.navigation.navigate(CONST.NAV_VALUE_INPUT)}>
                <View style={[styles.supportItem, styles.secondItem]}>
                  <View style={styles.supportSubContainer}>
                    {/* <Image style={styles.otherImage} source={require('../img/ic_upgrade_valve2.png')} /> */}
                    <View style={styles.descContainer}>
                      <Text style={styles.title}>Input valve Problems</Text>
                      <Text style={styles.description} numberOfLines={2}>Enter reoccurring valve problems for a plant and IMI will help to solve it</Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
          </ImageBackground>;
  }
}

const styles = StyleSheet.create({
  supportItem: {
    flex: 1,
    opacity: 0.8,
    paddingLeft: 40,
    paddingRight: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  supportSubContainer: {
    alignItems: 'center'
  },
  firstItem: {
    backgroundColor: '#0091d8'
  },
  descContainer: {
    alignItems: 'center'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff',
    marginBottom:18
  },
  description: {
    fontSize: 14,
    color: '#fff',
    flexWrap:'wrap',
    textAlign: 'center'
  },
  otherImage: {
    width: 64,
    height: 64
  },
  secondItem: {
    backgroundColor: '#0261a2'
  }
});
