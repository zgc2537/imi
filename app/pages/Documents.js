'use strict'

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableHighlight,
  TouchableOpacity,
  FlatList
} from 'react-native';
import moment from 'moment';
import RNFetchBlob from 'rn-fetch-blob';
import FileViewer from 'react-native-file-viewer';
import { connect } from 'react-redux'
import SLoading from '../components/SLoading';
import * as CONST from '../utils/constants';
import {
  getGetParams,
  getPostParams,
  getPatchParams,
  sFetch,
  stringReplace,
  getErrStr,
  onlyAlert,
  getFileSize
} from '../utils/utils'

const {height, width} = Dimensions.get('window');
const dirs = RNFetchBlob.fs.dirs;

class Documents extends Component {

  static navigationOptions = {
    title: 'Project Documents',
  };

  state = {
    loadingVisible: false,
    refreshing: false,
    next: null,
    list: []
  }

  componentDidMount() {
    this._onRefresh()
  }

  _assetsFetch = (url) => {
    return new Promise((resolve, reject) => {
      sFetch(url, getGetParams(), (res)=>{
        resolve(res)
      },(e)=>{
        reject(e)
      });
    })

  }

  _getAssets = (url) => {
    const { list } = this.state
    this._assetsFetch(url).then(res => {
      this.setState({
        refreshing: false,
        next: res.meta.next ? CONST.HOST_NAME + res.meta.next : null,
        list: [...list, ...res.objects]
      })
    }, err => {
      this.setState({refreshing: false})
      onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
    })
  }

  _openFile = (fileDir) => {
    FileViewer.open(fileDir).then(() => {
      this.setState({loadingVisible: false});
    }).catch((e) => {
      this.setState({loadingVisible: false});
      onlyAlert(CONST.ALERT_TITLE_TIP, 'Please install the pdf program');
    });
  };

  _openManual = (item) => {

    const fileName = item.file.split('/').pop();
    const path = `${dirs.DocumentDir}/imi/${fileName}`;
    const url = `${CONST.HOST_NAME}${item.file}`;

    this.setState({loadingVisible: true});

    RNFetchBlob.fs.exists(path)
    .then((exist) => {
      if(exist){
        this._openFile(path);
      } else {
        this._downloadFile(url, path);
      }
    }).catch(() => {
      this.setState({loadingVisible: false});
    });

  };

  _downloadFile = (url, path) => {
    RNFetchBlob
    .config({
      // response data will be saved to this path if it has access right.
      path : path
    })
    .fetch('GET', url, {
      //some headers ..
    })
    .then((res) => {
      this._openFile(path);
    }).catch((e) => {
      this.setState({loadingVisible: false});
      onlyAlert(CONST.ALERT_TITLE_TIP, 'File download failed');
    });
  }

  _keyExtractor = (item, index) => item.id

  _itemClick = (item) => {
    this._openManual(item)
  }


  _renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={()=>{
          this._itemClick(item)
        }}>
        <View style={styles.listItemContainer}>
          <View>
            <Image style={styles.icon} source={require('../img/ic_documents_pdf.png')} />
          </View>
          <View style={styles.listItem}>
            <View>
              <Text style={styles.title} numberOfLines={1}>{item.file_name}</Text>
            </View>
            <View style={styles.listItemRow}>
              <Text style={styles.rowText} numberOfLines={1}>{moment(item.date).format('MM/DD/YYYY')}</Text>
            </View>
            <View style={styles.listItemRow}>
              <Text style={styles.rowText} numberOfLines={1}>{getFileSize(item.size)}</Text>
            </View>
          </View>

        </View>
      </TouchableOpacity>

    )
  }

  _onRefresh = () => {
    const {
      navigation: {
        state: { params }
      }
    } = this.props
    params.asset && this.setState({refreshing: true, list: []}, ()=> {
      this._getAssets(stringReplace(CONST.REQ_URL_ASSET_FILE, params.asset.id))
    })
  }

  _onEndReached = () => {
    const { next } = this.state
    next && this._getAssets(next)
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.listContainer}>
          <FlatList
            onRefresh={this._onRefresh}
            onEndReached={this._onEndReached}
            refreshing={this.state.refreshing}
            style={styles.listStyle}
            data={this.state.list}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
          />
        </View>
        <SLoading visible={this.state.loadingVisible} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f7f9fa',
  },

  listContainer: {
    flex: 1
  },

  listStyle: {
    flex: 1,
  },

  listItemContainer: {
    width: width,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 15
  },

  listItem: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    marginLeft: 20,
  },

  listItemRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  icon: {
    width: 42,
    height: 42
  },

  title: {
    fontSize: 15,
    color: '#080808',
    fontWeight: 'bold'
  },

  rowText: {
    fontSize: 12,
    color: '#8B999F',
    fontWeight: 'bold'
  }
});

function select(store) {
  return new Object();
}

export default connect(select)(Documents);
