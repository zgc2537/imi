'use strict'

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Linking,
  ToastAndroid
} from 'react-native';

import { connect } from 'react-redux'


const {height, width} = Dimensions.get('window');

const LINK_TYPE = {
  'TEL': 'tel',
  'URL': 'http',
  'MAP': 'geo',
  'SMS': 'smsto',
  'MAIL': 'mailto'
}
class ContactDetail extends Component {

  static navigationOptions = ({navigation}) => {
    let { contact } = navigation.state.params;
    return {
      title : contact.name
    }
  };

  state = {
    contact: {}
  };

  componentDidMount() {
    let { contact } = this.props.navigation.state.params;
    contact && this.setState({contact: contact})
  }

  _linking = (str, type) => {
    Linking.openURL(`${type}:${str}`).catch(err => console.log(err));
  }

  render() {
    let { contact } = this.state;
    return (
      <ScrollView style={styles.container}>
        <View style={styles.head}>
          <Image style={styles.avatar} source={require('../img/ic_contacts_userpic.png')} />
          <Text style={styles.name}>{`${contact.first_name} ${contact.last_name}`}</Text>
          <View style={styles.infoBox}>
            <Text style={[styles.info, styles.infoSp]}>{contact.job_title}</Text>
            <Text style={styles.info}>{contact.dept}</Text>
          </View>
        </View>

        <View style={styles.item}>
          <View style={styles.itemInfoContainer}>
            <Text style={styles.title}>Cell Phone</Text>
            <Text style={styles.detail} numberOfLines={1}>{contact.cell_phone}</Text>
          </View>
          <TouchableOpacity style={styles.iconContainer} onPress={()=>{this._linking(contact.cell_phone, LINK_TYPE.TEL)}}>
            <Image style={styles.icon} source={require('../img/ic_contactlist_call.png')} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.iconContainer} onPress={()=>{this._linking(contact.cell_phone, LINK_TYPE.SMS)}}>
            <Image style={styles.icon} source={require('../img/ic_contactlist_message.png')} />
          </TouchableOpacity>
        </View>

        <View style={styles.item}>
          <View style={styles.itemInfoContainer}>
            <Text style={styles.title}>Phone Number</Text>
            <Text style={styles.detail} numberOfLines={1}>{contact.phonenumber ? contact.phonenumber : '--'}</Text>
          </View>
          <TouchableOpacity style={styles.iconContainer} onPress={()=>{this._linking(contact.phonenumber, LINK_TYPE.TEL)}}>
            <Image style={styles.icon} source={require('../img/ic_contactlist_call.png')} />
          </TouchableOpacity>
        </View>

        <View style={styles.item}>
          <View style={styles.itemInfoContainer}>
            <Text style={styles.title}>Email</Text>
            <Text style={styles.detail} numberOfLines={1}>{contact.email}</Text>
          </View>
          <TouchableOpacity style={styles.iconContainer} onPress={()=>{this._linking(contact.email, LINK_TYPE.MAIL)}}>
            <Image style={styles.icon} source={require('../img/ic_contactlist_email.png')} />
          </TouchableOpacity>
        </View>

        <View style={styles.item}>
          <View style={styles.itemInfoContainer}>
            <Text style={styles.title}>Location</Text>
            <Text style={styles.detail} numberOfLines={1}>{`${contact.location} , ${contact.country}`}</Text>
          </View>
        </View>

        <View style={{height: 30}}></View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f7f9fa',
  },
  head: {
    width: width,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
  },
  avatar: {
    width: 90,
    height: 90,
    borderRadius: 45,
  },
  name: {
    fontSize: 18,
    color: '#212a35',
    marginTop: 5,
  },
  infoBox: {
    width: width,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5
  },
  info: {
    width: width / 2,
    fontSize: 14,
    color: '#8B999F',
    paddingHorizontal: 10,
  },
  infoSp: {
    textAlign: 'right',
    borderRightWidth: 1,
    borderRightColor: '#8B999F'
  },
  item: {
    paddingRight: 10,
    marginTop: 5,
    backgroundColor: '#fff',
    width: width,
    height: 73,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  iconContainer: {
    width: 60,
    borderLeftWidth: 1,
    borderLeftColor: '#8B999F',
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    width: 42,
    height: 42
  },
  itemInfoContainer: {
    flex: 1,
    paddingHorizontal: 20
  },
  title: {
    fontSize: 14,
    color: '#8b999f'
  },
  detail: {
    marginTop: 5,
    fontSize: 18,
    color: '#212a35'
  }
});

function select(store) {
  return new Object();
}

export default connect(select)(ContactDetail);
