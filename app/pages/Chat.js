'use strict'

import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Dimensions,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native';

import * as CONST from '../utils/constants';
const {height, width} = Dimensions.get('window');

export default class Chat extends Component {

  static navigationOptions = {
    title: 'Chat',
    inputText: ''
  };

  chats = [];
  state = {
    chats: this.chats
  };

  _jump = (adress) => {
    this.props.navigation.navigate(adress);
  };

  componentDidMount() {

  }

  _moreChat = () => {
    let chatsView = [];
    this.state.chats.map((item, index) => {
      chatsView.push(<Text style={styles.chatSelf}>{item}</Text>);
    });
    return chatsView;
  };

  _addChat = () => {
    if(this.state.inputText != "") {
      this.chats.push(this.state.inputText);
      this.setState({chats: this.chats}, ()=> {
        this.setState({chats: this.chats});
      });
      this.setState({inputText: ""});
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.bgContainer}>
          <Image resizeMode={"center"} style={styles.bg} source={require('../img/example.png')} />
        </View>
        <ScrollView>
          <View style={styles.chatContainer}>
            <Text style={styles.time}>30 mins ago</Text>
            <Text style={styles.chatWith}>Hi, Can i ask you one question</Text>
            <Text style={styles.chatSelf}>OK, no problem</Text>
            {this._moreChat()}
          </View>
        </ScrollView>
        <View style={styles.chatActionContainer}>
          <TextInput underlineColorAndroid='transparent'
          editable={true}
          onChangeText={(text) => {this.setState({inputText: text})}}
          placeholder="Something..."
          value={this.state.inputText}
          placeholderTextColor="#8B999F"
          style={styles.chatInput}/>
          <TouchableOpacity style={{width: 67, height: 35}} onPress={this._addChat}>
            <Text style={styles.chatBtn}>Send</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f5f6',
  },
  bgContainer: {
    flex: 1,
    position: 'absolute',
    bottom: 0,
    left: 0,
    backgroundColor: '#f7f9fa',
    justifyContent: 'center',
    alignItems: 'center'
  },

  bg: {
    width: width - 40,
    marginLeft: 20,
  },
  chatContainer: {
    paddingTop: 9,
    paddingLeft: 9,
    paddingRight: 9
  },
  time: {
    color: '#8B999F',
    fontSize: 12,
    alignSelf: 'center'
  },
  chatWith: {
    backgroundColor: '#0091D8',
    paddingLeft: 21,
    paddingRight: 21,
    paddingTop: 9,
    paddingBottom: 9,
    marginTop: 16,
    color: '#fff',
    fontSize: 16,
    borderRadius: 8,
    maxWidth: width * 2 / 3,
    alignSelf: 'flex-start'
  },
  chatSelf: {
    backgroundColor: '#4B4F63',
    paddingLeft: 21,
    paddingRight: 21,
    paddingTop: 9,
    paddingBottom: 9,
    marginTop: 16,
    color: '#fff',
    fontSize: 16,
    borderRadius: 8,
    maxWidth: width * 2 / 3,
    alignSelf: 'flex-end'
  },
  chatActionContainer: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    height: 54,
    paddingLeft: 9,
    paddingRight: 9,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width: width
  },
  chatInput: {
    width: width - 67 - 9 - 9 - 9,
    height: 40,
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#F8F8F8',
    borderRadius: 2
  },
  chatBtn: {
    width: 67,
    height: 35,
    backgroundColor: '#fff',
    color: '#2196F3',
    fontSize: 16,
    textAlign: 'center',
    textAlignVertical: 'center'
  }
});
