'use strict'

import React, {Component} from 'react';
import {
  Platform,
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  Dimensions,
  TouchableHighlight,
  TouchableOpacity,
  FlatList
} from 'react-native';

import {
  getGetParams,
  getPostParams,
  getPatchParams,
  sFetch,
  stringReplace,
  getErrStr,
  onlyAlert,
  encode
} from '../utils/utils';

import { connect } from 'react-redux'
import SearchBar from '../components/SearchBar';
import * as CONST from '../utils/constants';

const {height, width} = Dimensions.get('window');

class Contacts extends Component {

  static navigationOptions = {
    title: 'Contacts',
  };

  state = {
    refreshing: false,
    group: [],
    loadingVisible: false
  }

  componentDidMount() {
    this._onRefresh()
  }

  _getContacts = (url) => {
    return new Promise((resolve, reject) => {
      sFetch(url, getGetParams({
        limit: 0
      }),(res)=>{
        resolve(res.objects)
      },(e)=>{
        reject(e)
      });
    })

  }

  _formatContacts = (contacts) => {
    let group = {};
    contacts.map((item, index) => {
      let title = this._trim(item.dept)
      !group[title] && (group[title] = [])
      group[title] && group[title].push(item)
    })
    let list = Object.keys(group).map((item, index)=> {
      return {
        title: item,
        list: group[item]
      }
    })
    return list
  }

  _trim = (str) => {
    return str.replace(/^\s+|\s+$/g,"")
  }

  _onSearch = (text) => {
    if(text) {
      this._onRefresh(`${CONST.REQ_URL_CONTACT}&first_name__icontains=${encode(text)}`)
    }
  }

  _keyExtractor = (item, index) => index

  _itemClick = (item) => {
    this.props.navigation.navigate(CONST.NAV_CONTACT_DETAIL, {
      contact: item
    });
  }


  _renderItem = ({ item, index }) => {
    let sub = item.list.map((subItem, key)=> {
      return (
        <TouchableOpacity key={subItem.id} style={styles.personContainer} onPress={()=>{this._itemClick(subItem)}}>
          <View style={[styles.personSpl, (item.list.length > 1 && key != (item.list.length - 1)) ? styles.setBorder : {}]}>
            <Image style={styles.img} source={require('../img/ic_contacts_userpic.png')} />
            <View style={styles.infoBox}>
              <Text style={styles.infoName}>{`${subItem.first_name} ${subItem.last_name}`}</Text>
              <Text style={styles.info}>{subItem.job_title}</Text>
            </View>
          </View>
        </TouchableOpacity>
      )
    })

    return (
      <View >
        <View>
          <Text style={styles.groupTitle}>{item.title}</Text>
        </View>
        {sub}
        <View style={{height: index == (this.state.group.length - 1) ? 20 : 0 }}></View>
      </View>

    )
  }

  _onRefresh = (url = CONST.REQ_URL_CONTACT) => {
    console.log(url)
    this.setState({refreshing: true}, ()=> {
      this._getContacts(url).then((res) => {
        let list = this._formatContacts(res)
        this.setState({refreshing: false, group: list})
      }, (err) => {
        this.setState({refreshing: false})
        let strErr = getErrStr(err);
        onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${strErr}!`);
      })
    })

  }

  _onEndReached = () => {
    console.log('请求');
  }

  _onChangeFilter = (filters) => {
    this._onRefresh(`${CONST.REQ_URL_CONTACT}&${filters}`)
  }

  _onFilter = () => {
    this.props.navigation.navigate(CONST.NAV_FILTER, {
      filter: 'contact',
      onChangeFilter: this._onChangeFilter
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <SearchBar
          placeholder={"First Name"}
          hasFilter={true}
          hasClear={false}
          onSearch={this._onSearch}
          onFilter={this._onFilter}
        />
        <FlatList
          onRefresh={this._onRefresh}
          onEndReached={this._onEndReached}
          refreshing={this.state.refreshing}
          data={this.state.group}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
        />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f7f9fa',
  },
  groupTitle: {
    paddingVertical: 10,
    fontSize: 14,
    color: '#8B999F',
    paddingLeft: 25,
  },
  personContainer: {
    paddingHorizontal: 20,
    backgroundColor: '#fff'
  },
  setBorder: {
    borderBottomWidth: 1,
    borderBottomColor: '#ddd',
  },
  personSpl: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10
  },
  img: {
    width: 48,
    height: 48,
    borderRadius: 48,
  },
  infoBox: {
    marginLeft: 15,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  infoName: {
    fontSize: 18,
    color: '#212A35'
  },
  info: {
    fontSize: 14,
    color: '#4a4a4a'
  }
});

function select(store) {
  return new Object();
}

export default connect(select)(Contacts);
