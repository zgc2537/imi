/**
 *
 * Created by zhouguocheng @ 2017, Inc.
 *
 */
import React from 'react';
import {
  View,
} from 'react-native';
import * as CONST from '../utils/constants';
import SplashScreen from 'react-native-splash-screen';
import NavigationUtil from '../utils/NavigationUtil';
class Splash extends React.Component {
  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    if(this.props.isLoggedIn) {
      NavigationUtil.reset(this.props.navigation, CONST.NAV_HOME_PAGE);
    } else {
      NavigationUtil.reset(this.props.navigation, CONST.NAV_LOGIN);
    }
  }

  render() {
    return (
      <View>
      </View>
    );
  }
}

export default Splash;
