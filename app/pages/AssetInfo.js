'use strict'

import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ImageBackground,
  Image
} from 'react-native';
import * as CONST from '../utils/constants';

const {width, height} = Dimensions.get('window');

export default class AssetInfo extends React.Component {
  static navigationOptions = {
    title: 'Product & Asset Info',
  };

  _jump = (adress) => {
    this.props.navigation.navigate(adress);
  }

  render() {
    return <ImageBackground style={{ flex: 1 }}
          source={require('../img/bg_app.png')}>
              <TouchableOpacity style={{ flex: 1}} activeOpacity={0.9} onPress={()=>{this._jump(CONST.NAV_SCAN_TAG)}}>
                <View style={[styles.supportItem, styles.firstItem]}>
                  <Image style={styles.otherImage} source={require('../img/ic_product_scan.png')} />
                  <View style={styles.descContainer}>
                    <Text style={styles.title}>Scan Tag Plate</Text>
                    <Text style={styles.description}>Photograph a Tag plate and we will search for the part number</Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flex: 1}} activeOpacity={0.9} onPress={()=>{this._jump(CONST.NAV_PRODUCT_NUMBER)}}>
                <View style={[styles.supportItem, styles.secondItem]}>
                  <Image style={styles.otherImage} source={require('../img/ic_product_number.png')} />
                  <View style={styles.descContainer}>
                    <Text style={styles.title}>Enter Product Number</Text>
                    <Text style={styles.description}>Search for a product by Serial Model Number</Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flex: 1}} activeOpacity={0.9} onPress={()=>{this._jump(CONST.NAV_TAKE_PIC)}}>
                <View style={[styles.supportItem, styles.thirdItem]}>
                  <Image style={styles.otherImage} source={require('../img/ic_product_camera.png')} />
                  <View style={styles.descContainer}>
                    <Text style={styles.title}>Take A Product Photo</Text>
                    <Text style={styles.description}>Photograph a product and our experts will identify it</Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flex: 1}} activeOpacity={0.9} onPress={()=>{this._jump(CONST.NAV_SELECT_DATABASE)}}>
                <View style={[styles.supportItem, styles.forthItem]}>
                  <Image style={styles.otherImage} source={require('../img/ic_product_database.png')} />
                  <View style={styles.descContainer}>
                    <Text style={styles.title}>Select From Database</Text>
                    <Text style={styles.description}>Select your product from your IMI asset list</Text>
                  </View>
                </View>
              </TouchableOpacity>
          </ImageBackground>;
  }
}

const styles = StyleSheet.create({
  supportItem: {
    flex: 1,
    opacity: 0.8,
    // flexDirection: 'row',
    paddingLeft: 40,
    paddingRight: 40,
    justifyContent: 'center',
    alignItems: 'center'
  },
  firstItem: {
    backgroundColor: '#0091D8'
  },
  descContainer: {
    // flexGrow: 1,
    // width: 0
    alignItems: 'center'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#fff',
    marginBottom: 4
  },
  description: {
    fontSize: 14,
    color: '#fff',
    flexWrap:'wrap',
    textAlign: 'center'
  },
  otherImage: {
    width: 24,
    height: 24,
    marginBottom: 12
  },
  secondItem: {
    backgroundColor: '#0272BE'
  },
  thirdItem: {
    backgroundColor: '#0261A2'
  },
  forthItem: {
    backgroundColor: '#024B7C'
  },
});
