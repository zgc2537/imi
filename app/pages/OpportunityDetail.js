'use strict'

import React from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Dimensions,
  Text,
  TouchableOpacity,
  ImageBackground,
  Image,
  Modal,
  TextInput
} from 'react-native';
import * as CONST from '../utils/constants';
import {
  getGetParams,
  getPostParams,
  getPatchParams,
  sFetch,
  stringReplace,
  getErrStr,
  onlyAlert
} from '../utils/utils';
import SModal from '../components/SModal';
import RNFetchBlob from 'rn-fetch-blob';
import FileViewer from 'react-native-file-viewer';
import SLoading from '../components/SLoading';
import {RadioGroup, RadioButton} from '../components/SRadioButton';

const dirs = RNFetchBlob.fs.dirs;
const {width, height} = Dimensions.get('window');
const INPUT_CONTENT_TYPE = {
  HAVE_PROBLEM: 0,
  PROBLEM_INFO: 1
};
const PROBLEM_TYPE = {
  NO_PROBLEM: 0,
  EXISTS: 1,
  UNKNOW: -1
}
const PROBLEM_TYPE_TIP = {
  '0': 'No problem',
  '1': 'Exists on site',
  '-1': 'Unkown'
}

export default class OpportunityDetail extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { from } = navigation.state.params;
    return {
      title: from,
    }
  }

  state = {
    loadingVisible: false,
    visible: false,
    opportunity: {},
    inputContent: INPUT_CONTENT_TYPE.HAVE_PROBLEM,
    selectedIndex: 0,
    problem: 0,
    maker: '',
    model: '',
    ty_problem: '',
    solution: ''
  };

  componentWillMount() {
    const {
      navigation: {
        state: { params }
      }
    } = this.props

    if(params) {
      this.setState({loadingVisible: true})
      this._getOpp(params.opportunity.id).then(res => {
        this.setState({loadingVisible: false})
        console.log(res)
        const opportunity = res.objects[0]
        this.setState({
          opportunity: opportunity,
          selectedIndex: Object.keys(PROBLEM_TYPE_TIP).indexOf(`${opportunity.problem}`),
          problem: opportunity.problem,
          maker: opportunity.maker,
          model: opportunity.model,
          ty_problem: opportunity.typical_problem,
          solution: opportunity.possible_solution,
        })
      }, err => {
        this.setState({loadingVisible: false})
        onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
      })

    }
  }

  renderInputContent = () => {
    return this.state.inputContent == INPUT_CONTENT_TYPE.HAVE_PROBLEM ? this.renderProblemContent() : this.renderProblemInfoContent();
  };

  problemSelect = (index, value) => {
    this.setState({
      selectedIndex: index,
    })
  };

  _submit = () => {
    const { selectedIndex } = this.state
    const { maker, model, ty_problem, solution } = this

    this.setState({
      visible: false,
      loadingVisible: true
    })

    if(this.state.inputContent == INPUT_CONTENT_TYPE.HAVE_PROBLEM) {

      this._patchOpp({
        problem: Object.keys(PROBLEM_TYPE_TIP)[selectedIndex]
      }).then(res => {
        this.setState({ problem: res.problem, loadingVisible: false })
        onlyAlert(CONST.ALERT_TITLE_TIP, `Successfully modified!`);
      }, err => {
        this.setState({ loadingVisible: false })
        onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
      })

    }else {

      let param = {}

      maker && (param['maker'] = maker)
      model && (param['model'] = model)
      ty_problem && (param['typical_problem'] = ty_problem)
      solution && (param['possible_solution'] = solution)

      if(!Object.keys(param).length) {
        this.setState({ loadingVisible: false })
        return
      }

      this._patchOpp(param).then(res => {

        this.setState({
          loadingVisible: false,
          maker: res.maker,
          model: res.model,
          ty_problem: res.typical_problem,
          solution: res.possible_solution,
        })

        onlyAlert(CONST.ALERT_TITLE_TIP, `Successfully modified!`);
      }, err => {
        this.setState({ loadingVisible: false })
        onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
      })

    }
  }

  _getOpp = (id) => {
    return new Promise((resolve, reject) => {
      sFetch(CONST.REQ_URL_OPPORTUNITY_CREATE + '?id=' + id, getGetParams(), (res)=>{
        resolve(res)
      },(e)=>{
        reject(e)
      });
    })
  }
  _patchOpp = (params) => {
    const { opportunity } = this.state
    return new Promise((resolve, reject) => {
      sFetch(CONST.REQ_URL_OPPORTUNITY_CREATE + opportunity.id + '/', getPatchParams(params), (res)=>{
        resolve(res)
      },(e)=>{
        reject(e)
      });
    })
  }

  renderProblemContent = () => {
    return <View style={styles.inputContentContainer}>
      <RadioGroup selectedIndex={this.state.selectedIndex} onSelect = {(index, value) => this.problemSelect(index, value)} style={styles.radioGroupStyle}>
              <RadioButton value={PROBLEM_TYPE.NO_PROBLEM}>
                <Text style={styles.radioButtonTextStyle}>No problem</Text>
              </RadioButton>
              <RadioButton value={PROBLEM_TYPE.EXISTS} >
                <Text style={styles.radioButtonTextStyle}>Exists on site</Text>
              </RadioButton>
              <RadioButton value={PROBLEM_TYPE.UNKNOW}>
                <Text style={styles.radioButtonTextStyle}>Unkown</Text>
              </RadioButton>
            </RadioGroup>
    </View>;
  };

  renderProblemInfoContent = () => {
    return (<View style={styles.inputContentContainer}>
      <Text style={styles.inputLabel}>Maker:</Text>
      <TextInput style={styles.inputTextInput} underlineColorAndroid="transparent"
        placeholder="Maker" placeholderTextColor="#9b9b9b" onChangeText={(text)=> {
          this.maker = text
        }} />
      <Text style={styles.inputLabel}>Valve Model:</Text>
      <TextInput style={styles.inputTextInput} underlineColorAndroid="transparent"
        placeholder="Valve Model" placeholderTextColor="#9b9b9b" onChangeText={(text)=> {
          this.model = text
        }} />
      <Text style={styles.inputLabel}>Problem:</Text>
      <TextInput style={styles.inputTextInput} underlineColorAndroid="transparent"
        placeholder="Problem" placeholderTextColor="#9b9b9b" onChangeText={(text)=> {
          this.ty_problem = text
        }} />
      <Text style={styles.inputLabel}>Possible Solution:</Text>
      <TextInput style={styles.inputTextInput} underlineColorAndroid="transparent"
        placeholder="Possible Solution" placeholderTextColor="#9b9b9b" onChangeText={(text)=> {
          this.solution = text
        }} />
    </View>);
  };



  _openFile = (fileDir) => {
    FileViewer.open(fileDir).then(() => {
      this.setState({loadingVisible: false});
    }).catch((e) => {
      this.setState({loadingVisible: false});
      onlyAlert(CONST.ALERT_TITLE_TIP, 'Please install the pdf program');
    });
  };

  _openManual = (item) => {
    if(!item.link_file) {
      onlyAlert(CONST.ALERT_TITLE_TIP, `File Error!`);
      return
    }

    const fileName = item.link_file.split('/').pop();
    const path = `${dirs.DocumentDir}/imi/${fileName}`;
    const url = CONST.HOST_NAME + item.link_file;

    this.setState({loadingVisible: true});
    RNFetchBlob.fs.exists(path)
    .then((exist) => {
      if(exist){
        this._openFile(path);
      } else {
        this._downloadFile(url, path);
      }
    }).catch(() => {
      this.setState({loadingVisible: false});
    });

  };

  _downloadFile = (url, path) => {
    RNFetchBlob
    .config({
      // response data will be saved to this path if it has access right.
      path : path
    })
    .fetch('GET', url, {
      //some headers ..
    })
    .then((res) => {
      this._openFile(path);
    }).catch((e) => {
      this.setState({loadingVisible: false});
      onlyAlert(CONST.ALERT_TITLE_TIP, 'File download failed');
    });
  }


  _renderFile = (item) => {

    const fileName = item.link_file.split('/').pop()

    return (
      <TouchableOpacity onPress={()=> {
        this._openManual(item)
      }}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={[styles.value, {maxWidth: 200}]}>Open Upgrade Packages</Text>
          <Image source={require('../img/ic_upgrade_openmore.png')} style={styles.more}/>
        </View>
      </TouchableOpacity>
    )
  }
  render() {
    const { opportunity, opportunity: {contact}, problem, maker, solution, ty_problem, model } = this.state;

    return <ScrollView>
      <SLoading visible={this.state.loadingVisible} />
      <SModal visible={this.state.visible} canWindowCloseOnTouchOutside={true}>
        <View style={styles.inputContainer}>
          <View style={styles.inputHeaderContainer}>
            <Text style={styles.inputHeader}>{this.state.inputContent == INPUT_CONTENT_TYPE.HAVE_PROBLEM ? "Problem" : "Problem Info"}</Text>
          </View>
           {this.renderInputContent()}

          <View style={styles.inputActionContainer}>
            <TouchableOpacity style={styles.inputBtn} onPress={()=> {
              this.setState({visible: false})
            }}>
              <Text style={styles.inputBtnText}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.inputBtn} onPress={this._submit}>
              <Text style={styles.inputBtnText}>OK</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SModal>

      <View style={styles.container}>

          <View style={styles.regionContainer}>
            <View style={styles.headContainer}>
              <Text style={styles.name}>{opportunity.plant_name}</Text>
              <Text style={styles.location} numberOfLines={1}>{opportunity.location ? `${opportunity.location}, ${opportunity.country}` : ''}</Text>
            </View>
            <View style={[styles.itemContainer, styles.borderTop]}>
              <Text style={styles.label}>Application:</Text>
              <Text style={styles.value} ellipsizeMode={'tail'} numberOfLines={1}>{opportunity.application}</Text>
            </View>
            <View style={[styles.itemContainer, styles.borderTop]}>
              <Text style={styles.label}>Upgrade Type:</Text>
              <Text style={opportunity.upgrade_type ? [styles.upgradeTypeValue, opportunity.upgrade_type == 'UGC' ? {} : styles.typeErr] : {}}>
                {opportunity.upgrade_type ? opportunity.upgrade_type : ''}
              </Text>
            </View>
            <View style={[styles.itemSsContainer, styles.borderTop]}>
              <Text style={styles.label}>Success Story:</Text>
              {
                opportunity.success_story && opportunity.success_story.link_file ? this._renderFile(opportunity.success_story) : (
                  <Text></Text>
                )
              }
            </View>
            <View style={[styles.itemContainer, styles.borderTop]}>
              <Text style={styles.label}>Problem:</Text>
              <View style={styles.problemValueContainer}>
                <Text style={styles.value}>{PROBLEM_TYPE_TIP[problem]}</Text>
                <TouchableOpacity style={styles.icon} onPress={() => {
                  this.setState({
                    visible: true,
                    inputContent: INPUT_CONTENT_TYPE.HAVE_PROBLEM
                  })
                }}>
                  <View>
                    <Image source={require('../img/ic_list_edit.png')} style={styles.more}/>
                  </View>
                </TouchableOpacity>

              </View>
            </View>
          </View>

          <View style={[styles.regionContainer,styles.bottomContainer]}>
              <Text style={styles.contactTitle}>Customer Contact</Text>
            <View style={[styles.itemContainer, styles.borderTop]}>
              <Text style={styles.label}>Name:</Text>
              <Text style={styles.value}>{opportunity.contact_name ? opportunity.contact_name : ''}</Text>
            </View>
            <View style={styles.itemContainer}>
              <Text style={styles.label}>Phone:</Text>
              <Text style={styles.value}>{opportunity.contact_phonenumber ? opportunity.contact_phonenumber : ''}</Text>
            </View>
            <View style={styles.itemContainer}>
              <Text style={styles.label}>Email:</Text>
              <Text style={styles.value}>{opportunity.contact_email ? opportunity.contact_email : ''}</Text>
            </View>
            <View style={styles.itemContainer}>
              <View style={[styles.halfBorder, styles.borderTop]}></View>
              <TouchableOpacity style={styles.icon} onPress={() => {
                this.setState({
                  visible: true,
                  inputContent: INPUT_CONTENT_TYPE.PROBLEM_INFO
                })
              }}>
                <View>
                  <Image source={require('../img/ic_list_edit.png')} style={styles.more}/>
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.itemContainer}>
              <Text style={styles.label}>Maker:</Text>
              <Text style={styles.value} ellipsizeMode={'tail'} numberOfLines={1}>{maker}</Text>
            </View>
            <View style={styles.itemContainer}>
              <Text style={styles.label}>Model:</Text>
              <Text style={styles.value} ellipsizeMode={'tail'} numberOfLines={1}>{model}</Text>
            </View>
            <View style={styles.itemContainer}>
              <Text style={styles.label}>Problem:</Text>
              <Text style={styles.value} ellipsizeMode={'tail'} numberOfLines={1}>{ty_problem}</Text>
            </View>
            <View style={styles.itemContainer}>
              <Text style={styles.label}>Possible Solution:</Text>
              <Text style={[styles.value, {width: width / 2}]} ellipsizeMode={'tail'} numberOfLines={1}>{solution}</Text>
            </View>
          </View>
        </View>
          </ScrollView>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: 20,
    backgroundColor: '#f2f5f6'
  },
  regionContainer: {
    backgroundColor: '#fff',
    opacity: 0.8,
    paddingLeft: 14,
    paddingRight: 14
  },
  headContainer: {
    paddingTop: 12,
    paddingBottom: 16
  },
  name: {
    fontSize: 20,
    color: '#080808',
    fontWeight: 'bold'
  },
  location: {
    maxWidth: width / 3 * 2,
    color: '#4a4a4a',
    fontSize: 16,
    textAlign: 'left'
  },
  itemContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  itemSsContainer: {
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },

  borderTop: {
    borderColor : '#dde1e2',
    borderTopWidth : 1
  },
  label: {
    color: '#496571',
    fontSize: 18,
    fontWeight: 'bold'
  },
  value: {
    maxWidth: width / 3 * 2,
    color: '#4a4a4a',
    fontSize: 16,
    textAlign: 'right'
  },
  upgradeTypeValue: {
    height: 20,
    paddingLeft: 12,
    paddingRight: 12,
    textAlignVertical: 'center',
    fontSize: 14,
    color: '#fff',
    backgroundColor: '#f5a623',
    borderRadius: 10
  },
  problemValueContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  more: {
    width: 20,
    height: 20
  },
  icon: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  bottomContainer: {
    marginTop: 12
  },
  contactTitle: {
    marginTop: 20,
    marginBottom:10,
    fontSize: 18,
    fontWeight: 'bold',
    color: '#080808'
  },
  halfBorder: {
    height: 22,
    marginTop: 23,
    width: width/2
  },
  inputContainer: {
    width:270,
    borderRadius: 8,
    backgroundColor: '#fff'
  },
  inputHeaderContainer: {
    borderBottomWidth: 1,
    borderColor: '#DDE1E2'
  },
  inputHeader: {
    fontSize: 18,
    color: '#000',
    marginTop: 11,
    marginBottom: 9,
    marginLeft: 26
  },
  inputContentContainer: {
    paddingLeft: 24,
    paddingRight: 24,
    paddingBottom: 17
  },
  inputLabel: {
    marginTop: 10,
    fontSize: 16,
    color: '#496571'
  },
  inputTextInput: {
    marginTop: 12,
    height: 40,
    borderRadius: 2,
    borderWidth: 1,
    borderColor: '#B8C9D1'
  },
  inputActionContainer: {
    flexDirection: 'row',
    borderTopWidth: 1,
    borderColor: '#DDE1E2'
  },
  inputBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 135,
    height: 42
  },
  inputBtnText: {
    color: '#4E6975',
    fontSize: 16
  },
  radioGroupStyle : {
    marginTop: 10
  },
  radioButtonTextStyle : {
    fontSize : 16,
    color : '#000'
  },
  typeErr: {
    backgroundColor: '#099447'
  },

});
