'use strict'

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableHighlight,
  FlatList
} from 'react-native';

import { connect } from 'react-redux'
import SearchBar from '../components/SearchBar';
import * as CONST from '../utils/constants';
import {
  getGetParams,
  getPostParams,
  getPatchParams,
  sFetch,
  stringReplace,
  getErrStr,
  onlyAlert,
  encode
} from '../utils/utils';

const {height, width} = Dimensions.get('window');

class SelectDatabase extends Component {

  static navigationOptions = {
    title: 'Select from Database',
  };

  state = {
    defValue: '',
    refreshing: false,
    next: null,
    list: []
  }

  componentWillMount() {
    const {
      navigation: {
        state: { params }
      }
    } = this.props

    if(params) {
      params.resetQr && params.resetQr()
      this.setState({defValue: params.defValue})
      this._onRefresh(`${CONST.REQ_URL_ASSET}?serial__icontains=${encode(params.defValue)}`)
    }else {
      this._onRefresh()
    }
  }

  _assetsFetch = (url) => {
    return new Promise((resolve, reject) => {
      sFetch(url, getGetParams(), (res)=>{
        resolve(res)
      },(e)=>{
        reject(e)
      });
    })

  }

  _getAssets = (url) => {
    const { list } = this.state
    if(this.props.user.sfdc_account) {
      if(url.indexOf("?") != -1) {
        url += "&sfdc_account=" + this.props.user.sfdc_account;
      } else {
        url += "?sfdc_account=" + this.props.user.sfdc_account;
      }
    }
    this._assetsFetch(url).then(res => {
      this.setState({
        refreshing: false,
        next: res.meta.next ? CONST.HOST_NAME + res.meta.next : null,
        list: [...list, ...res.objects]
      })
    }, err => {
      this.setState({refreshing: false})
      onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
    })
  }

  _onFilter = () => {
    this.props.navigation.navigate(CONST.NAV_FILTER, {
      filter: 'asset',
      onChangeFilter: this._onChangeFilter
    });
  }

  _onSearch = (text) => {
    if(text) {
      this._onRefresh(`${CONST.REQ_URL_ASSET}?serial__icontains=${encode(text)}`)
    }else {
      this._onRefresh()
    }
  }

  _onChangeFilter = (filters) => {
    // 更新
    console.log(`${CONST.REQ_URL_ASSET}?${filters}`)
    this._onRefresh(`${CONST.REQ_URL_ASSET}?${filters}`)
  }

  _keyExtractor = (item, index) => item.id

  _itemClick = (item) => {
    this.props.navigation.navigate(CONST.NAV_ASSET_PAGE, {
      asset: item,
    })
  }

  _renderItem = ({ item }) => {
    return (
      <TouchableHighlight
        onPress={()=>{
          this._itemClick(item)
        }}>
        <View style={styles.listItemContainer}>
          <View style={styles.listItem}>
            <View>
              <Text style={styles.title} numberOfLines={1}>{item.serial}</Text>
            </View>
            <View style={styles.listItemRow}>
              <Image style={styles.icon} source={require('../img/ic_upgrade_type.png')} />
              <Text style={[styles.rowText, {maxWidth: 170}]} numberOfLines={1} ellipsizeMode="tail">{item.application}</Text>
              <Image style={[styles.icon, styles.modelIcon]} source={require('../img/ic_upgrade_model.png')} />
              <Text style={styles.rowText} numberOfLines={1}>{item.model}</Text>
            </View>
          </View>
          <View>
            <Image style={styles.icon} source={require('../img/ic_list_nextpage.png')} />
          </View>
        </View>
      </TouchableHighlight>

    )
  }

  _onRefresh = (url = CONST.REQ_URL_ASSET) => {
    this.setState({refreshing: true, list: []}, ()=> {
      this._getAssets(url)
    })
  }

  _onEndReached = () => {
    const { next } = this.state
    next && this._getAssets(next)
  }

  render() {
    return (
      <View style={styles.container}>
        <SearchBar
          value={this.state.defValue}
          placeholder={"Serial"}
          hasFilter={true}
          hasClear={false}
          onSearch={this._onSearch}
          onFilter={this._onFilter}
        />
        <View style={styles.listContainer}>
          <FlatList
            onRefresh={this._onRefresh}
            onEndReached={this._onEndReached}
            refreshing={this.state.refreshing}
            style={styles.listStyle}
            data={this.state.list}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            ItemSeparatorComponent={() => ( <View style={{height: 10}} /> )}
          />
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f5f6',
  },
  listContainer: {
    flex: 1
  },
  listStyle: {
    flex: 1,
  },
  listItemContainer: {
    width: width,
    paddingTop: 12,
    paddingBottom: 18,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 30,
    backgroundColor: '#fff'
  },

  listItem: {
    flexDirection: 'column',
    justifyContent: 'space-around'
  },

  listItemRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10
  },

  icon: {
    width: 22,
    height: 22
  },
  modelIcon: {
    marginLeft: 12
  },
  title: {
    fontSize: 16,
    color: '#080808'
  },

  rowText: {
    fontSize: 14,
    color: '#4a4a4a',
    marginLeft: 6,
  }
});

function select(store) {
  return {
    user: store.userInfo.user
  };
}

export default connect(select)(SelectDatabase);
