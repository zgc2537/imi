'use strict'

import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ImageBackground,
  Image
} from 'react-native';
import { connect } from 'react-redux';
import * as CONST from '../utils/constants';
import NavigationUtil from '../utils/NavigationUtil';

const {width, height} = Dimensions.get('window');

class Upgrades extends React.Component {
  static navigationOptions = {
    title: 'Upgrades',
  };

  _jump = (adress) => {
    this.props.navigation.navigate(adress);
  }

  render() {
    return <ImageBackground style={{ flex: 1 }}
          source={require('../img/bg_app.png')}>
              <TouchableOpacity style={{ flex: 1}} activeOpacity={0.9} onPress={()=>{this._jump(CONST.NAV_FIND_UPGRADE)}}>
                <View style={[styles.supportItem, styles.firstItem]}>
                  <Image style={styles.otherImage} source={require('../img/ic_upgrade_1.png')} />
                  <View style={styles.descContainer}>
                    <Text style={styles.title}>Find Upgrade Opportunities</Text>
                    <Text style={styles.description}>Filter opportunities by application, plant type, account, ...</Text>
                  </View>
                </View>
              </TouchableOpacity>
              {this.props.user.sfdc_account ? null :
                <TouchableOpacity style={{ flex: 1}} activeOpacity={0.9} onPress={()=>{this._jump(CONST.NAV_MY_UPGRADE)}}>
                  <View style={[styles.supportItem, styles.secondItem]}>
                    <Image style={styles.otherImage} source={require('../img/ic_upgrade_2.png')} />
                    <View style={styles.descContainer}>
                      <Text style={styles.title}>My Upgrade List & Status</Text>
                      <Text style={styles.description} numberOfLines={2}>Review your upgrade cases and the latest status</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              }
              <TouchableOpacity style={{ flex: 1}} activeOpacity={0.9} onPress={()=>{this._jump(CONST.NAV_CASE_STUDIES)}}>
                <View style={[styles.supportItem, styles.thirdItem]}>
                  <Image style={styles.otherImage} source={require('../img/ic_upgrade_3.png')} />
                  <View style={styles.descContainer}>
                    <Text style={styles.title}>Upgrade Packages</Text>
                    <Text style={styles.description}>Browse through IMI's successful upgrade case studies</Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flex: 1}} activeOpacity={0.9} onPress={()=>{this._jump(CONST.NAV_MANUAL_INPUT_PAGE)}}>
                <View style={[styles.supportItem, styles.forthItem]}>
                  <Image style={styles.otherImage} source={require('../img/ic_upgrade_4.png')} />
                  <View style={styles.descContainer}>
                    <Text style={styles.title}>Manual Search & Input</Text>
                    <Text style={styles.description}>Review or input new upgrade opportunities</Text>
                  </View>
                </View>
              </TouchableOpacity>
          </ImageBackground>;
  }
}

const styles = StyleSheet.create({
  supportItem: {
    flex: 1,
    opacity: 0.8,
    // flexDirection: 'row',
    paddingLeft: 40,
    paddingRight: 40,
    justifyContent: 'center',
    alignItems: 'center'
  },
  firstItem: {
    backgroundColor: '#0091D8'
  },
  descContainer: {
    // flexGrow: 1,
    // width: 0
    alignItems: 'center'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#fff',
    marginBottom: 4
  },
  description: {
    fontSize: 14,
    color: '#fff',
    flexWrap:'wrap',
    textAlign: 'center'
  },
  otherImage: {
    width: 24,
    height: 24,
    marginBottom: 12
  },
  secondItem: {
    backgroundColor: '#0272BE'
  },
  thirdItem: {
    backgroundColor: '#0261A2'
  },
  forthItem: {
    backgroundColor: '#024B7C'
  },
});

function select(store) {
  return {
    user: store.userInfo.user
  };
}

export default connect(select)(Upgrades);
