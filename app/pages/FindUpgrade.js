'use strict'

import React, {Component} from 'react';
import {
  Platform,
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  Dimensions,
  TouchableHighlight,
  TouchableOpacity,
  FlatList
} from 'react-native';

import { connect } from 'react-redux';
import SearchBar from '../components/SearchBar';
import * as CONST from '../utils/constants';
import {
  getGetParams,
  getPostParams,
  getPatchParams,
  sFetch,
  stringReplace,
  getErrStr,
  onlyAlert,
  encode
} from '../utils/utils';

const {height, width} = Dimensions.get('window');

class FindUpgrade extends Component {

  static navigationOptions = {
    title: 'Find Upgrade Opportunities',
  };

  state = {
    refreshing: false,
    next: null,
    list: []
    // list: [
    //   {
    //     id: 1,
    //     name: 'TEPCO Hinoro',
    //     size: 'BFW Pump Recirculation',
    //     way: 'Closed Won',
    //     type: 1,
    //     status: 0
    //   },
    //   {
    //     id: 2,
    //     name: 'TEPCO Hinoro',
    //     size: 'BFW Pump Recirculation',
    //     way: 'Closed Won',
    //     type: 1,
    //     status: 1
    //   },
    //   {
    //     id: 3,
    //     name: 'TEPCO Hinoro',
    //     size: 'BFW Pump Recirculation',
    //     way: 'Closed Won',
    //     type: 0,
    //     status: 2
    //   },
    //   {
    //     id: 4,
    //     name: 'TEPCO Hinoro',
    //     size: 'BFW Pump Recirculation',
    //     way: 'Closed Won',
    //     type: 0,
    //     status: 3
    //   },
    //   {
    //     id: 5,
    //     name: 'TEPCO Hinoro',
    //     size: 'BFW Pump Recirculation',
    //     way: 'Closed Won',
    //     type: 0,
    //     status: 0
    //   },
    //   {
    //     id: 6,
    //     name: 'TEPCO Hinoro',
    //     size: 'BFW Pump Recirculation',
    //     way: 'Closed Won',
    //     type: 0,
    //     status: 1
    //   },
    //   {
    //     id: 7,
    //     name: 'TEPCO Hinoro',
    //     size: 'BFW Pump Recirculation',
    //     way: 'Closed Won',
    //     type: 0,
    //     status: 2
    //   },
    //   {
    //     id: 8,
    //     name: 'TEPCO Hinoro',
    //     size: 'BFW Pump Recirculation',
    //     way: 'Closed Won',
    //     type: 0,
    //     status: 3
    //   },
    // ]
  }

  componentDidMount() {
    this._onRefresh()
  }

  _fetch = (url) => {
    return new Promise((resolve, reject) => {
      sFetch(url, getGetParams(), (res)=>{
        resolve(res)
      },(e)=>{
        reject(e)
      });
    })

  }

  _getOpportunity = (url) => {
    const { list } = this.state
    if(this.props.user.sfdc_account) {
      if(url.indexOf("?") != -1) {
        url += "&sfdc_account=" + this.props.user.sfdc_account;
      } else {
        url += "?sfdc_account=" + this.props.user.sfdc_account;
      }
    }
    this._fetch(url).then(res => {
      this.setState({
        refreshing: false,
        next: res.meta.next ? CONST.HOST_NAME + res.meta.next : null,
        list: [...list, ...res.objects]
      })
    }, err => {
      this.setState({refreshing: false})
      onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
    })
  }

  _onRefresh = (url = CONST.REQ_URL_OPPORTUNITY) => {
    this.setState({refreshing: true, list: []}, ()=> {
      this._getOpportunity(url)
    })
  }

  _onEndReached = () => {
    const { next } = this.state
    next && this._getOpportunity(next)
  }

  _onFilter = () => {
    this.props.navigation.navigate(CONST.NAV_FILTER, {
      filter: 'opportunity',
      onChangeFilter: this._onChangeFilter
    });
  }

  _onSearch = (text) => {
    if(text) {
      this._onRefresh(`${CONST.REQ_URL_OPPORTUNITY}&plant_name__icontains=${encode(text)}`)
    }else {
      this._onRefresh()
    }
  }

  _onChangeFilter = (filters) => {
    // 更新
    this._onRefresh(`${CONST.REQ_URL_OPPORTUNITY}&${filters}`)
  }

  _keyExtractor = (item, index) => item.id

  _itemClick = (item) => {
    // console.log(item);
    this.props.navigation.navigate(CONST.NAV_OPPORTUNITY_DETAIL_PAGE, {
      from: 'Find Upgrade Opportunities',
      opportunity: item
    });
  }

  _renderItem = ({ item }) => {
    return (
      <TouchableHighlight
        onPress={()=>{
          this._itemClick(item)
        }}>
        <View style={styles.listItemContainer}>
          <View style={styles.listItem}>
            <View style={styles.listItemRow}>
              <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">{item.plant_name}</Text>
              {
                item.upgrade_type ? (
                  <Text style={[styles.type, item.upgrade_type == 'UGC' ? {} : styles.typeErr]}>{item.upgrade_type}</Text>
                ) : null
              }

            </View>
            <View style={styles.listItemRow}>
              <Image style={styles.icon} source={require('../img/ic_upgrade_application.png')} />
              <Text style={styles.rowText} numberOfLines={1}>{item.application}</Text>
            </View>
            <View style={styles.listItemRow}>
              <Image style={styles.icon} source={require('../img/ic_upgrade_maker.png')} />
              <Text style={styles.rowText} numberOfLines={1}>{item.model ? item.model : '-'}</Text>
            </View>
            {/* <View style={styles.listItemRow}>
              <Image style={styles.icon} source={require('../img/ic_upgrade_status.png')} />
              <Text style={styles.rowText} numberOfLines={1}>{CONST.OPPORTUNITY_STATUS[item.upgrade_status]}</Text>
            </View> */}
          </View>
          <View>
            <Image style={styles.icon} source={require('../img/ic_list_nextpage.png')} />
          </View>
        </View>
      </TouchableHighlight>

    )
  }

  render() {
    return (
      <View style={styles.container}>
        <SearchBar
          placeholder={"Plant Name"}
          hasFilter={true}
          hasClear={false}
          onSearch={this._onSearch}
          onFilter={this._onFilter}
        ></SearchBar>
        <View style={styles.listContainer}>
          <FlatList
            onRefresh={this._onRefresh}
            onEndReached={this._onEndReached}
            refreshing={this.state.refreshing}
            style={styles.listStyle}
            data={this.state.list}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            ItemSeparatorComponent={() => ( <View style={{height: 10}} /> )}
          />
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f5f6',
  },

  listContainer: {
    flex: 1
  },

  listStyle: {
    flex: 1,
  },

  listItemContainer: {
    width: width,
    height: 123,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 30,
    paddingBottom: 8,
    backgroundColor: '#fff'
  },

  listItem: {
    flexDirection: 'column',
    justifyContent: 'space-around'
  },

  listItemRow: {
    flexDirection: 'row',
    marginTop: 8,
    alignItems: 'center'
  },

  icon: {
    width: 22,
    height: 22
  },

  title: {
    width: width * 2 / 3,
    fontSize: 16,
    color: '#080808'
  },

  type: {
    fontSize: 14,
    backgroundColor: '#F5A623',
    color: '#fff',
    paddingHorizontal: 10,
    borderRadius: 15,
    marginLeft: 10
  },

  typeErr: {
    backgroundColor: '#099447'
  },

  rowText: {
    fontSize: 14,
    color: '#4a4a4a',
    marginLeft: 15,
  }
});

function select(store) {
  return {
    user: store.userInfo.user
  };
}

export default connect(select)(FindUpgrade);
