'use strict'

import React from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Dimensions,
  Text,
  TouchableOpacity,
  Image
} from 'react-native';
import * as CONST from '../utils/constants';

import RNFetchBlob from 'rn-fetch-blob';
import FileViewer from 'react-native-file-viewer';
import SLoading from '../components/SLoading';

import {
  getPostParams,
  sFetch,
  getErrStr,
  onlyAlert,
} from '../utils/utils';

const {width, height} = Dimensions.get('window');
const dirs = RNFetchBlob.fs.dirs;

export default class UpgradePackageDetail extends React.Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: navigation.state.params.detail.application
    };
  };

  state = {
    loadingVisible: false
  };

  _jump = (adress) => {
    this.props.navigation.navigate(adress);
  }

  _openFile = (fileDir) => {
    FileViewer.open(fileDir).then(() => {
      this.setState({loadingVisible: false});
    }).catch((e) => {
      this.setState({loadingVisible: false});
      onlyAlert(CONST.ALERT_TITLE_TIP, 'Please install the pdf program');
    });
  };

  _openManual = (item) => {
    if(!item.link_file) {
      onlyAlert(CONST.ALERT_TITLE_TIP, `File error!`);
      return
    }

    const fileName = item.link_file.split('/').pop();
    const path = `${dirs.DocumentDir}/imi/${fileName}`;
    const url = CONST.HOST_NAME + item.link_file;

    this.setState({loadingVisible: true});
    RNFetchBlob.fs.exists(path)
    .then((exist) => {
      if(exist){
        this._openFile(path);
      } else {
        this._downloadFile(url, path);
      }
    }).catch(() => {
      this.setState({loadingVisible: false});
    });

  };

  _downloadFile = (url, path) => {
    RNFetchBlob
    .config({
      // response data will be saved to this path if it has access right.
      path : path
    })
    .fetch('GET', url, {
      //some headers ..
    })
    .then((res) => {
      this._openFile(path);
    }).catch((e) => {
      this.setState({loadingVisible: false});
      onlyAlert(CONST.ALERT_TITLE_TIP, 'File download failed');
    });
  };

  _submit = () => {
    const data = {
      type_name: 'Enquire'
    }

    sFetch(CONST.REQ_URL_BURIED, getPostParams(data), null, null, false);
  };

  render() {
    let params = this.props.navigation.state.params.detail;
    const fileName = params.link_file ? params.link_file.split('/').pop() : '--';
    const imgUrl = params.diagram ? {uri: `${CONST.HOST_NAME}${params.diagram}`} : require('../img/logo.png');
    return <View style={styles.container}>
            <ScrollView>
              <View>
                <View style={styles.contentContainer}>
                  <Text style={styles.title}>{params.application}</Text>
                  <Image source={imgUrl}  style={styles.image}/>
                  <Text style={styles.problemTitle}>Typical Problems</Text>
                  <Text style={styles.problem}>{params.problem_description}</Text>
                </View>
                {params.link_file ? <TouchableOpacity style={{flex: 1}} onPress={() => {this._openManual(params);}}>
                  <View style={styles.fileContainer}>
                    <View style={styles.fileLeftContainer}>
                      <Image  style={styles.fileIcon} source={require('../img/ic_documents_pdf.png')}/>
                      <Text style={styles.fileName}>{fileName}</Text>
                    </View>
                    <Image style={styles.rightIcon} source={require('../img/ic_list_nextpage.png')}/>
                  </View>
                </TouchableOpacity> : null}
                <TouchableOpacity style={{flex: 1}} onPress={() => {this._submit();}}>
                  <Text style={styles.submitBtn}>Enquire</Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
            <SLoading visible={this.state.loadingVisible} />
          </View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f5f6'
  },
  contentContainer: {
    backgroundColor: '#fff',
    paddingHorizontal: 20,
    paddingVertical: 12
  },
  title: {
    fontSize: 16,
    color: '#080808',
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: 10,
    marginBottom: 20
  },
  image: {
    width: width - 20 * 2,
    height: (width - 20 * 2) * 2 / 3
  },
  problemTitle: {
    fontSize: 16,
    color: '#080808',
    fontWeight: 'bold',
    marginTop: 22,
    marginBottom: 9
  },
  problem: {
    fontSize: 16,
    color: '#080808'
  },
  fileContainer: {
    backgroundColor: '#fff',
    paddingHorizontal: 20,
    paddingVertical: 12,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 6
  },
  fileLeftContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  fileIcon: {
    width: 22,
    height: 23,
    marginRight: 20
  },
  fileName: {
    fontSize: 14,
    color: '#4a4a4a',
    width: 200
  },
  rightIcon: {
    width: 24,
    height: 24
  },
  submitBtn: {
    marginTop: 6,
    width: width,
    height: 50,
    backgroundColor: '#0091D8',
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
    textAlignVertical: 'center'
  }
});
