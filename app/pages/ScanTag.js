'use strict';
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import * as CONST from '../utils/constants';
const { width, height } = Dimensions.get('window');
import NavigationUtil from '../utils/NavigationUtil';

export default class ScanTag extends Component {
  static navigationOptions = {
    title: 'Scan Tag Plate',
  };

  state = {

  }

  render() {
  return (
    <View style={styles.container}>
      <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style = {styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.auto}
          permissionDialogTitle={'Permission to use camera'}
          permissionDialogMessage={'We need your permission to use your camera phone'}
          onBarCodeRead={({ data, rawData, type }) => {
            if(type == "QR_CODE") {
              if(this.qr == data) {
                return;
              }
              this.qr = data;
              this.props.navigation.navigate(CONST.NAV_SELECT_DATABASE, {
                defValue: data
              });
              this.camera.pausePreview();
            }
          }}
      />
      <View style={styles.model}>
        <View style={[styles.topContainer, styles.opacityBg]}></View>
        <View style={styles.middleContainer}>
          <View style={[styles.middleTwoView, styles.opacityBg]}></View>
          <View style={styles.middleView}></View>
          <View style={[styles.middleTwoView, styles.opacityBg]}></View>
        </View>
        <View style={[styles.bottomContainer, styles.opacityBg]}>
          <Text style={styles.tip}>Please align Tag Plate</Text>
        </View>
      </View>
    </View>
  );
}

takePicture = async function() {
  if (this.camera) {
    const options = { quality: 0.5, base64: true };
    const data = await this.camera.takePictureAsync(options)
    console.log(data.uri);
  }
};
}

const takePicViewWidth = width - 60 * 2;

const styles = StyleSheet.create({
container: {
  flex: 1,
  flexDirection: 'column',
  backgroundColor: 'black'
},
preview: {
  flex: 1,
  justifyContent: 'flex-end',
  alignItems: 'center'
},
model: {
  position: 'absolute',
  top: 0,
  left: 0,
  height: height - CONST.ACTION_BAR_HEIGHT,
  width: width
},
topContainer: {
  width: width,
  height: (height - CONST.ACTION_BAR_HEIGHT - takePicViewWidth) / 2
},
middleContainer: {
  width: width,
  height: takePicViewWidth,
  flexDirection: 'row'
},
middleTwoView: {
  width: 60,
  height: takePicViewWidth
},
middleView: {
  width: takePicViewWidth,
  height: takePicViewWidth,
  borderWidth: 2,
  borderColor: '#0091d8'
},
bottomContainer: {
  width: width,
  height: (height - CONST.ACTION_BAR_HEIGHT - (width - 60 * 2)) % 2 == 1 ? (height - CONST.ACTION_BAR_HEIGHT - (width - 60 * 2)) / 2 + 1 : (height - CONST.ACTION_BAR_HEIGHT - (width - 60 * 2)) / 2
},
opacityBg: {
  backgroundColor: '#000',
  opacity: 0.8
},
tip: {
  alignSelf: 'center',
  marginTop: 18,
  fontSize: 16,
  color: '#fff'
}
});
