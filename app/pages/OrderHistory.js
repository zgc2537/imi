'use strict'

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  Easing,
  Animated,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  FlatList
} from 'react-native';
import moment from 'moment';
import { connect } from 'react-redux'
import * as CONST from '../utils/constants';
import {
  getGetParams,
  sFetch,
  stringReplace,
  getErrStr,
  onlyAlert
} from '../utils/utils';
import SHideableView from '../components/SHideableView';

const { width } = Dimensions.get('window');
const ORDER_STATUS = {
  0: 'Close',
  1: 'Open'
}

class OrderHistory extends Component {

  static navigationOptions = {
    title: 'Order Status & History',
  };

  state = {
    refreshing: false,
    next: null,
    list: [],
    products: []
  }

  componentDidMount() {
    this._onRefresh()
  }

  _fetch = (url) => {
    return new Promise((resolve, reject) => {
      sFetch(url, getGetParams(), (res)=>{
        resolve(res)
      },(e)=>{
        reject(e)
      });
    })

  }
 
  _getAllfetch = (url1, url2) => {
    const { list } = this.state
    Promise.all([this._fetch(url1), this._fetch(url2)]).then((res) => {
      this.setState({
        refreshing: false,
        next: res[0].meta.next ? CONST.HOST_NAME + res[0].meta.next : null,
        list: [...list, ...res[0].objects],
        products: res[1].objects
      })
    }).catch((err) => {
      this.setState({refreshing: false})
      onlyAlert(CONST.ALERT_TITLE_TIP, `Request failed: ${getErrStr(err)}!`);
    })
  }

  _onRefresh = () => {
    const {
      navigation: {
        state: { params }
      }
    } = this.props
    params.asset && this.setState({refreshing: true, list: []}, ()=> {
      this._getAllfetch(stringReplace(CONST.REQ_URL_ASSET_ORDER, params.asset.id), stringReplace(CONST.REQ_URL_PRODUCT, params.asset.id))
    })
  }

  _onEndReached = () => {
    const { next } = this.state
    next && this._getAllfetch(next)
  }

  _keyExtractor = (item, index) => item.id;

  _renderItem = ({ item }) => {
    return <Order item ={item} products={this.state.products} />;
  };

  render() {
    const {
      navigation: {
        state: { params }
      }
    } = this.props
    return (
      <View style={styles.container}>
      <View style={styles.idArea}>
        <Text style={styles.idText}>{params.asset.serial}</Text>
      </View>
        <View style={styles.listContainer}>
          <FlatList
            onRefresh={this._onRefresh}
            onEndReached={this._onEndReached}
            refreshing={this.state.refreshing}
            style={styles.listStyle}
            data={this.state.list}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            ItemSeparatorComponent={() => ( <View style={{height: 10}} /> )}
          />
        </View>
      </View>
    );
  }
}

class Order extends Component {
  state = {
    childVisible : false
  };

  rotateValue = new Animated.Value(0);
  rotate = this.rotateValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: ['0deg', '90deg', '0deg']
    });
  styles = StyleSheet.create({
    iconTransform: {
      transform : [{rotate : this.rotate}]
    },
  });

  _keyExtractor = (item, index) => item.id;

  _rotate180 = () => {
    // if(!haveChildren) {
    //   return;
    // }
    if(!this.state.childVisible) {
      Animated.timing(
        this.rotateValue,
        {
          toValue: 0.5,
          duration: 30,
          easing: Easing.linear
        }
      ).start();
      this.setState({
        childVisible : true
      });
    } else {
      Animated.timing(
        this.rotateValue,
        {
          toValue: 1,
          duration: 300,
          easing: Easing.linear
        }
      ).start();
      this.rotateValue.setValue(0);
      this.setState({
        childVisible : false
      });
    }
  }

  _renderSparePartItem = ({ item }) => {
    return <View style={styles.sparePartItemContainer}>
            <Text style={styles.title} numberOfLines={1}>{item.part_number}</Text>
            <View style={styles.rightContainer}>
              <View style={styles.rightContainer}>
                <Image style={styles.icon} source={require('../img/ic_asset_qty.png')} />
                <Text style={styles.rowText} numberOfLines={1}>{item.quantity}</Text>
              </View>
              <View style={[styles.rightContainer, {width: width / 3}]}>
                <Image style={[styles.icon, styles.modelIcon]} source={require('../img/ic_asset_name.png')} />
                <Text style={styles.rowText} numberOfLines={1}>{item.part_name}</Text>
              </View>
            </View>
          </View>;
  }

  render() {
    return <View style={styles.listItem}>
      <TouchableWithoutFeedback onPress={() => {this._rotate180()}}>
        <View style={styles.listItemContainer}>
          <View style={styles.listItem}>
            <View>
              <Text style={styles.title} numberOfLines={1}>{moment(this.props.item.order_date).format('MM/DD/YYYY')}</Text>
            </View>
            <View style={styles.listItemRow}>
              <Image style={styles.icon} source={require('../img/ic_upgrade_status.png')} />
              <Text style={styles.rowText} numberOfLines={1}>{ORDER_STATUS[this.props.item.order_status]}</Text>
              <Image style={[styles.icon, styles.modelIcon]} source={require('../img/ic_asset_date.png')} />
              <Text style={styles.rowText} numberOfLines={1}>{this.props.item.sfdc_account}</Text>
            </View>
          </View>
          <Animated.Image style={[styles.icon, this.styles.iconTransform]} source={require('../img/ic_list_nextpage.png')} />
        </View>
      </TouchableWithoutFeedback>
      {this.state.childVisible ?
      <SHideableView noAnimation={false} removeWhenHidden={true}
        visible={this.state.childVisible}>
        <FlatList
          style={styles.listStyle}
          data={this.props.products}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderSparePartItem}
        />
      </SHideableView> : null}
    </View>;
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f5f6',
  },
  idArea: {
    height: 42,
    backgroundColor: '#133c6c',
    opacity: 0.8,
    justifyContent: 'center',
    alignItems: 'center'
  },
  idText: {
    fontSize: 18,
    color: '#fff'
  },
  listContainer: {
    flex: 1
  },
  listStyle: {
    flex: 1,
  },
  listItemContainer: {
    width: width,
    paddingTop: 12,
    paddingBottom: 18,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 30,
    backgroundColor: '#fff'
  },

  sparePartItemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingTop: 6,
    paddingBottom: 4,
    paddingHorizontal: 46,
    marginTop: 1
  },

  rightContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },

  listItem: {
    justifyContent: 'space-around'
  },

  listItemRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10
  },

  icon: {
    width: 22,
    height: 22
  },
  modelIcon: {
    marginLeft: 12
  },
  title: {
    fontSize: 16,
    color: '#080808'
  },

  rowText: {
    fontSize: 14,
    color: '#4a4a4a',
    marginLeft: 6,
  }
});

function select(store) {
  return new Object();
}

export default connect(select)(OrderHistory);
