'use strict'

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
} from 'react-native';

export default class Utilities extends Component {

  static navigationOptions = {
    title: 'Utilities',
  };

  componentDidMount() {

  }

  _jump = (adress) => {
    this.props.navigation.navigate(adress);
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.contentContainer}>
          <Image style={styles.image} source={require('../img/pic_under_construction.png')}/>
          <Text style={styles.txt1} >Under Construction</Text>
          <Text style={[styles.txt2, styles.textStyle]} >In the future you can access:</Text>
          <Text style={[styles.txt3, styles.textStyle]} >IMI Critical Engineering Product Information / Catalogues Agent/Distributor Contact Videos(Promotional/Training/...) Company News</Text>
          <Text style={[styles.txt3, styles.textStyle]} >And much more exciting content and future developments.</Text>
          <Text style={[styles.txt4, styles.textStyle]} >Stay tuned!</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f5f6'
  },
  contentContainer: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 34,
    paddingBottom: 34,
    paddingLeft: 17,
    paddingRight: 17,
    alignItems: 'center'
  },
  image: {
    width: 164,
    height: 130
  },
  txt1: {
    fontSize: 22,
    fontWeight: 'bold',
    color: '#F5A623',
    marginTop: 32
  },
  txt2: {
    marginTop: 42,
    alignSelf: 'flex-start',
    paddingLeft: 20,
    paddingRight: 20
  },
  txt3: {
    marginTop: 12,
    paddingLeft: 20,
    paddingRight: 20
  },
  txt4: {
    alignSelf: 'flex-start',
    paddingLeft: 20,
    paddingRight: 20
  },
  textStyle: {
    fontSize: 18,
    color: '#4a4a4a',
    textAlign: 'left',
    fontWeight: 'bold'
  }
});
